<?php

namespace App\Http\Controllers\students;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\models\Classes;
use App\models\Groupes;
use App\models\Sections;
use App\models\Sessions;

class StudentSetting extends Controller
{
    
    public function classes()
    {
        $classes = Classes::all();
        return view('admin/studentsetting/class',compact('classes')); //ddfkjldj
    } 

    public function sessions()
    {
        $sessions = Sessions::all();
        return view('admin/studentsetting/session',compact('sessions'));
    }

     public function groupes()
    {
        $groupes = Groupes::all();
        return view('admin/studentsetting/group',compact('groupes'));
    }   
    
    public function sections()
    {
        $sections = Sections::all();
        return view('admin/studentsetting/section',compact('sections'));
    } 





}
