<?php

namespace App\Http\Controllers\students;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\models\Classes;
use App\models\Groupes;
use App\models\Sections;
use App\models\Sessions;
use App\models\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , $allDataGet = null)
    {

        $classes = Classes::all();
        $groupes = Groupes::all();
        $sessions = Sessions::all();
        $sections = Sections::all();

       
        if($allDataGet == 'all'){
              $student = Student::all();
                return view('/admin/students/view',compact('student','classes','groupes','sessions','sections'));
        }
        else{

                if($request->classes_id != null){
                    $classes_id = $request->classes_id;
                }else{
                    $classes_id = null; 
                }

                if($request->sessions_id != null){
                    $sessions_id = $request->sessions_id;
                }else{
                    $sessions_id = null; 
                }

                if($request->groupes_id != null){
                    $groupes_id = $request->groupes_id;
                }else{
                    $groupes_id = null; 
                }

                if($request->sections_id != null){
                    $sections_id = $request->sections_id;
                }else{
                    $sections_id = null; 
                }


                        $data['classes_search'] = $classes_id;
                        $data['sessions_search'] = $sessions_id;
                  $student = Student::where(['classes_id' => $classes_id, 'sessions_id' => $sessions_id])->get();


            if($groupes_id != null && $sections_id != null ){
                  $student  =  $student->where('groupes_id' ,'=', $groupes_id)->where('sections_id', '=', $sections_id);
            }
            else{
                    $student ;
            }

            if($groupes_id != null){

                $data['groupes_search'] = $groupes_id;
                $student =  $student->where('groupes_id', '=' ,$groupes_id );
            }
            if($sections_id != null){
                $data['sections_search'] = $sections_id;
                $student =  $student->where('sections_id', '=', $sections_id );
            }


                return view('/admin/students/view',compact('student','classes','groupes','sessions','sections'),$data);
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
