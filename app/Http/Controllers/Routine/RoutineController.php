<?php

namespace App\Http\Controllers\Routine;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Classes;
use App\models\Groupes;
use App\models\Sessions;
use App\models\Student;
use App\models\Subject;
use App\models\Sections;
use App\models\Routine;
use App\models\Teachers;
use App\models\Time;
use App\models\Day;
use Session;
use Auth;
class RoutineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['classes']    = Classes::all();
        $data['sessions']   = Sessions::all();
        $data['groups']     = Groupes::all();
        $data['students']   = Student::all();
        $data['times']      = Time::all();
        $data['sections']   = Sections::all();
        $data['days']       = Day::all();
        $data['teachers']   = Teachers::all();

        if($request->class_id > 0 ){
           
            $class_id = $request->class_id ;
            $data['className'] =   Classes::where('id',$class_id)->first()->name; 
            $data['IdClass'] =  $request->class_id;
        }
        else{
            $class_id = 0 ;
        }

        if($request->session_id > 0 ){
            $session_id = $request->session_id ;
            $data['sessionName'] =   Sessions::where('id',$session_id)->first()->name; 
            $data['IdSession'] = $request->session_id;
        }
        else{
            $session_id = 0 ;
        }

        if($request->section_id > 0 ){
            $section_id = $request->section_id ;
            $data['SectionName'] =   Sections::where('id',$section_id)->first()->name; 
            $data['IdSection'] = $request->section_id;
        }
        else{
            $section_id = 0 ;
        }

            if($class_id > 0 && $session_id > 0 &&  $section_id > 0)   
             {
                $data['subject']    = Subject::where('classes_id',$class_id)->get();
                
                $data['routine']    = Routine::where('class_id', $class_id)->where('session_id',$session_id)->where('section_id',$section_id)->get();

                //---------------------------
                $data['classTime']    = Routine::select()->where('class_id', $class_id)->where('session_id',$session_id)->where('section_id',$section_id)->orderBy('id',"asc")->get(); 

                $data['subjects']    = Routine::select()->where('class_id', $class_id)->where('session_id',$session_id)->where('section_id',$section_id)->get();
                //---------------------------
             
             }
             else{
                $data['routine'] = [];
             }

        return view('admin.routine.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
            $time_id    = $request->time_id;
            $endtime_id    = $request->endtime_id;
            $day_id     = $request->day_id;
            $subject_id = $request->subject_id;
            $teacher_id = $request->teacher_id;
            $section_id = $request->section_id;
            $session_id = $request->session_id;
            $class_id   = $request->class_id;

           
            //first check, this teacher already exist at this time in this day...
            //second check, any subject is already exist at this time in this day in this section in this  class and this section...
           $checkTeacherAllreadyExist =  Routine::where('teacher_id',$teacher_id)->where('time_id',$time_id)->where('day_id',$day_id)->where('session_id',$session_id)->count();

           if($checkTeacherAllreadyExist == 0)
           {
            
                $checkTimeIsExist =   Routine::where('time_id',$time_id)->where('section_id',$section_id)->where('class_id',$class_id)->where('session_id',$session_id)->where('day_id',$day_id)->count(); 
                if($checkTimeIsExist == 0)
                {
                         //-----------------------------------------------
                        $subjectIsAlreadyExistInTime =  Routine::where('subject_id',$subject_id)->where('section_id',$section_id)->where('class_id',$class_id)->where('session_id',$session_id)->where('day_id',$day_id)->count(); 
                                                //->where('time_id',$time_id) where('subject_id',$subject_id)->
                            //this subject is not assigned in this day, in this section, in this class 
                        if( $subjectIsAlreadyExistInTime == 0)
                        {
                             $routine =    new Routine(); 

                             $routine->user_id = Auth::user()->id;
                             $routine->day_id = $day_id;
                             $routine->subject_id = $subject_id;
                             $routine->teacher_id = $teacher_id;
                             $routine->section_id = $section_id;
                             $routine->session_id = $session_id;
                             $routine->class_id = $class_id;
                             $routine->time_id = $time_id;
                             $routine->endtime_id = $endtime_id;
                             $routine->status = 1;
                             $save =  $routine->save();

                             if($save){
                                 Session::flash('success','Added Successfully');
                                return redirect()->back();
                             }
                             else{
                                 Session::flash('error','Not Added');
                                 return redirect()->back();
                             }

                        }  //end if ,, nai...
                        else{
                             Session::flash('error','Subject is already exist in this day, in this class in this section You may update only');
                          return redirect()->back();
                        }
                //-----------------------------------------------   

                }
                else{
                     Session::flash('error','This Time is already exist in this day, in this class in this section You may update only');
                    return redirect()->back();  
                }
           }
           else{
             Session::flash('error','This Teacher is already exists in the same time in the day');
               return redirect()->back();
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
