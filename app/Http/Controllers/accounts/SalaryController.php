<?php

namespace App\Http\Controllers\accounts;

use App\models\Salary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;


class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        $data['salaryStatus'] = Salary::where('status',1)->get();

        if($request->startDate == "" && $request->endDate == "" && $request->input("year") == "" && $request->input("month") == "")
        {
            $EndDate =  date('Y-m-d');
            $start = date('Y-m-d');
            $end = date("Y-m-d",strtotime($EndDate."+1 day"));
            $data['salary'] = $data['salaryStatus']->whereBetween('created_at',[$start,$end]);
            $data['totalAmout'] = $data['salary']->sum('amount');
           return view('admin.salary.view',$data);
        }
        elseif($request->startDate != "" && $request->endDate != "" && $request->input("year") == "" && $request->input("month") == "")
        {
            $startDate = $request->startDate;
            $endDate = $request->endDate;
            $data['startDate'] = $startDate;
            $data['EndDate'] =  $endDate;

            $EndDate =  $request->endDate;
            $start = date("Y-m-d",strtotime($request->input('startDate')));
            $end = date("Y-m-d",strtotime($EndDate."+1 day"));
            $data['salary'] = $data['salaryStatus']->whereBetween('created_at',[$start,$end]);
            $data['totalAmout'] = $data['salary']->sum('amount');
           return view('admin.salary.view',$data);
        }
        //------------------------------------------------
        if($request->startDate == "" && $request->endDate == "" && $request->input("year") != "" && $request->input("month") != "")
        {
            $year = $request->input("year");
            $month = $request->input("month");
            $thismonths =  $year."-".$month;
            $thismonth = date('Y-m',strtotime($thismonths));
            $data['yearData'] = $year;
            $data['monthDataFull'] = date('F',strtotime($thismonths));
            $data['monthData'] = $month;
            Session::put('yearmonth',$thismonth);

            $data['monthYearSearch'] = 1;

            $data['salaryStatus'] = Salary::where('status',0)->get();

            $data['salary'] =   Salary::where('date','like',"$thismonth%")->get();
            $data['totalAmout'] = $data['salary']->sum('amount');
         return view('admin.salary.view',$data);    
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if($request->get("month") == "" && $request->get("year") == "")
        {
            $monthCurrent = date('Y-m');

            $SearchSalaryCount =  Salary::where('date','like',"$monthCurrent%")->count();
            if($SearchSalaryCount > 0)
            {
                $SearchSalary =  Salary::where('date','like',"$monthCurrent%")->get();
        
                foreach( $SearchSalary as $searchId){
                    $teacher_id[] = $searchId->teacher_id; 
                }
                $data['employee'] = Teacher::where('status',1)
                                    ->WhereNotIn('id',$teacher_id)
                                    ->get();
            }
            else{
                $data['employee'] = Teacher::where('status',1)
                                    ->get();
            }

            $thismonth = null;
            Session::put('yearmonth', $thismonth);
                
            return view('admin.salary.add',$data);    
        }
        else{
            $year = $request->input("year");
            $month = $request->input("month");
            $thismonths =  $year."-".$month;
            
            $thismonth = date('Y-m',strtotime($thismonths));
            $data['yearData'] = $year;
            $data['monthDataFull'] = date('F',strtotime($thismonths));
            $data['monthData'] = $month;
            Session::put('yearmonth',$thismonth);

            $searchSalaryCount = Salary::select('teacher_id')->where('date','like',"$thismonth%")->count();
            if($searchSalaryCount > 0)
            {
                $SearchSalary =  Salary::where('date','like',"$thismonth%")->get();
    
                foreach( $SearchSalary as $searchId)
                {
                    $teacher_id[] = $searchId->teacher_id; 
                }
                $data['employee'] = Teacher::where('status',1)
                                    ->WhereNotIn('id',$teacher_id)
                                    ->get();

                return view('admin.salary.add',$data);
            }
            $data['employee'] = Teacher::where('status',1)->get();
           // $data['advancedSalaryOfThisMonth'] = Salary_advance::where('date','like',"$thismonth%")->get();
            return view('admin.salary.add',$data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        if($request->yearmonth == "")
        {
            $notification = array(
                'messege' => 'Please Select Month & Year First!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }


        $validator =  $this->validate($request, [
            'amount' => 'required|min:1|max:10',
            'teacher_id' => 'required',
                ], 
                [
            'amount.required' => 'Amount field is required',
            'amount.min' => 'You may use Minimum 1 character',
            'amount.max' => 'You may use Maximum 10 character',

            'teacher_id.required' => 'Employee Name field is required',
          
             ]   
            );

             $salary = new Salary(); 
             $salary->user_id = Auth::user()->id;
             $salary->teacher_id = $request->teacher_id;
             $salary->date = $request->yearmonth."-01";
             $salary->amount = $request->amount;
             $salary->status = 1;
            $save =  $salary->save();

            $thismonth = null;
            Session::put('yearmonth', $thismonth);

       if($save){
            $notification = array(
                'messege' => 'Salary is Paid  Successfully!',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege' => 'Salary is Not Paid!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
