<?php

namespace App\Http\Controllers\accounts;

use App\models\Expense;
use App\models\Daily_expense;
use App\models\Expense_perpose;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
    {
         $data['expenses'] = Daily_expense::all();
        return view('admin.accounts.expense.view',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     


    $date = date('Y-m-d');
        $data['expensePurposes'] = Expense_perpose::all();
    $data['todaysCost'] = Daily_expense::where('created_at','like',"$date%")->get();
        $data['todaysExpense'] = $data['todaysCost']->sum('amount');
        return view('admin.accounts.expense.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  $this->validate($request, [
            'expense_purpose_id' => 'required',
            'title' => 'required|max:50|min:2',
            'amount' => 'required|max:10|min:1',
            'date' => 'required',
            'description' =>'nullable|max:150'
                ], 
                [
            'expense_purpose_id.required' => 'Daily Expense field is required',
            'payment_method.required' => 'Payment Method field is required',
            'title.required' => 'Title field is required',
            'title.min' => 'You may use Minimum 2 character',
            'title.max' => 'You may use Maximum 50 characte',
            'date.required' => 'Date field is required',

            'amount.required' => 'Amount field is required',
            'amount.min' => 'You may use Minimum 1 character',
            'amount.max' => 'You may use Maximum 10 characte',

            'description.max' => 'You may use Maximum 150 character',
             ]   
            );
                
            $dailyExpense = new Daily_expense(); 
            $dailyExpense->user_id = Auth::user()->id;
            $dailyExpense->expense_purpose_id = $request->expense_purpose_id;
            $dailyExpense->title = $request->title;
            $dailyExpense->amount = $request->amount;
            $dailyExpense->date = $request->date;
            $dailyExpense->status = 1;
            $dailyExpense->description = $request->description;
            $save = $dailyExpense->save();


            if($save){
                $notification = array(
                    'messege' => 'New Expense is Added Successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('expenseindex')->with($notification);
            }else{
                $notification = array(
                    'messege' => 'New Expense is Not Added!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['expensePurposes'] = Expense_perpose::all();
        $data['totalExpense']  = Daily_expense::findOrFail($id);
        return view('admin.accounts.expense.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['expensePurposes'] = Expense_perpose::all();
        $data['totalExpense']  = Daily_expense::findOrFail($id);
        return view('admin.accounts.expense.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator =  $this->validate($request, [
            'expense_purpose_id' => 'required',
            'title' => 'required|max:50|min:2',
            'amount' => 'required|max:10|min:1',
            'date' => 'required',
            'description' =>'nullable|max:150'
                ], 
                [
            'expense_purpose_id.required' => 'Daily Expense field is required',
            'payment_method.required' => 'Payment Method field is required',
            'title.required' => 'Title field is required',
            'title.min' => 'You may use Minimum 2 character',
            'title.max' => 'You may use Maximum 50 characte',
            'date.required' => 'Date field is required',

            'amount.required' => 'Amount field is required',
            'amount.min' => 'You may use Minimum 1 character',
            'amount.max' => 'You may use Maximum 10 characte',

            'description.max' => 'You may use Maximum 150 character',
             ]   
            );
                
            $dailyExpense =  Daily_expense::findOrFail($id); 
            $dailyExpense->user_id = Auth::user()->id;
            $dailyExpense->expense_purpose_id = $request->expense_purpose_id;
            $dailyExpense->title = $request->title;
            $dailyExpense->amount = $request->amount;
            $dailyExpense->date = $request->date;
            $dailyExpense->status = 1;
            $dailyExpense->description = $request->description;
            $save = $dailyExpense->save();


            if($save){
                $notification = array(
                    'messege' => ' Expense is Updated Successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('expenseindex')->with($notification);
            }else{
                $notification = array(
                    'messege' => ' Expense is Not Updated!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['totalExpense']  = Daily_expense::findOrFail($id);
         $data['totalExpense']->delete();
        return redirect()->route('expenseindex')->with("success","This Expense is Deleted Successfully"); 
    }


    public function expenseReport(Request $request)
    {
        if($request->startDate != null){
            $startDate = $request->startDate;
            $startDate = date('Y-m-d', strtotime($startDate));
             $data['startDate'] = $request->startDate;
        }
        else{
            $startDate = date("Y-m-d");
        }
        if($request->endDate != null){
            $endDate = $request->endDate;
            $endDate = date('Y-m-d', strtotime($endDate."+0 days"));
            $data['endDate'] =  $request->endDate;
        }
        else{
              // $endDate = date("Y-m-d");
                $endDate = date("Y-m-d",strtotime(date("Y-m-d")."+0 day"));
        }
        /*if($request->startDate != null && $request->endDate != null)
        {
            return  $endDate . "----".$startDate;
        }*/

        $data['expenses'] = Daily_expense::where('status',1)->whereBetween('date',[$startDate,$endDate])->orderBy('date',"ASC")->get();
        $data['total_amount'] = $data['expenses']->sum('amount');
       return view('admin.accounts.expense.report',$data);
    }
}
