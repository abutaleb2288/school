<?php

namespace App\Http\Controllers\accounts;

use App\models\Expense_perpose;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class Expense_perposeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['expensePurpose'] = Expense_perpose::all();
          return view('admin.accounts.expense.purpose.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.accounts.expense.purpose.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  $this->validate($request, [
            'name' => 'required|max:50|min:2|unique:expense_perposes,name',
            'description' =>'nullable|max:150'
                ], 
                [
            'name.required' => 'Name field is required',
            'name.min' => 'You may use Minimum 2 character',
            'name.max' => 'You may use Maximum 50 characte',

            'description.max' => 'You may use Maximum 150 character',
             ]   
            );

            $expensePurpose = new Expense_perpose();
            $expensePurpose->user_id = Auth::user()->id; 
            $expensePurpose->name = $request->name;
            $expensePurpose->description = $request->description;
            $save = $expensePurpose->save();

            if($save){
                $notification = array(
                    'messege' => 'New Expense Purpose is Added Successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('expensePurposeIndex')->with($notification);
            }else{
                $notification = array(
                    'messege' => 'New Expense Purpose is Not Added!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['expensePurpose'] = Expense_perpose::findOrFail($id);
         return view('admin.accounts.expense.purpose.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $validator =  $this->validate($request, [
            'name' => 'required|max:50|min:2|unique:expense_perposes,name,'.$id,
            'description' =>'nullable|max:150'
                ], 
                [
            'name.required' => 'Name field is required',
            'name.min' => 'You may use Minimum 2 character',
            'name.max' => 'You may use Maximum 50 characte',

            'description.max' => 'You may use Maximum 150 character',
             ]   
            );

            $expensePurpose =  Expense_perpose::findOrFail($id);
            $expensePurpose->user_id = Auth::user()->id;
            $expensePurpose->name = $request->name;
            $expensePurpose->description = $request->description;
            $save = $expensePurpose->save();

            if($save){
                $notification = array(
                    'messege' => ' Expense Purpose is Updated Successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('expensePurposeIndex')->with($notification);
            }else{
                $notification = array(
                    'messege' => ' Expense Purpose is Not Updated!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
