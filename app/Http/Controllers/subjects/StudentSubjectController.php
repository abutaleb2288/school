<?php

namespace App\Http\Controllers\subjects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\models\Classes;
use App\models\Groupes;
use App\models\Sections;
use App\models\Sessions;
use App\models\Student;

use App\models\Student_Subject;

use Session;



class StudentSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , $allDataGet = null)
    {

        
        $classes = Classes::all();
        $groupes = Groupes::all();
        $sessions = Sessions::all();
        $sections = Sections::all(); 

       
        if($allDataGet == 'all'){
              $student = Student::all();
                return view('/admin/students/view',compact('student','classes','groupes','sessions','sections'));
        }
        else{

                if($request->classes_id != null){
                    $classes_id = $request->classes_id;
                }else{
                    $classes_id = null; 
                }

                if($request->sessions_id != null){
                    $sessions_id = $request->sessions_id;
                }else{
                    $sessions_id = null; 
                }

                if($request->groupes_id != null){
                    $groupes_id = $request->groupes_id;
                }else{
                    $groupes_id = null; 
                }

                if($request->sections_id != null){
                    $sections_id = $request->sections_id;
                }else{
                    $sections_id = null; 
                }


                        $data['classes_search'] = $classes_id;
                        $data['sessions_search'] = $sessions_id;
                  $student = Student::where(['classes_id' => $classes_id, 'sessions_id' => $sessions_id])->get();


            if($groupes_id != null && $sections_id != null ){
                  $student  =  $student->where('groupes_id' ,'=', $groupes_id)->where('sections_id', '=', $sections_id);
            }
            else{
                    $student ;
            }

            if($groupes_id != null){

                $data['groupes_search'] = $groupes_id;
                $student =  $student->where('groupes_id', '=' ,$groupes_id );
            }
            if($sections_id != null){
                $data['sections_search'] = $sections_id;
                $student =  $student->where('sections_id', '=', $sections_id );
            }


                return view('/admin/subjects/students/view',compact('student','classes','groupes','sessions','sections'),$data);
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $student = Student::find($id);
        return view('/admin/subjects/students/add',compact('student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();

         $classes_id  = $request->classes_id;
         $groupes_id  = $request->groupes_id;
         $session_id  = $request->sessiones_id;
         $student_id  = $request->student_id;
         

         $compulary_subject[] = $request->compulary_subject;

        if($request->classes_id >=1 && $request->classes_id <=10 ){



        if($input['compulary_subject'] != ''){
            foreach ($input['compulary_subject'] as $key => $value) {
                $StudentSub_dist = new Student_Subject;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->session_id      = $session_id;
                $StudentSub_dist->subject_id      = $input['compulary_subject'][$key];
                $StudentSub_dist->subject_type    = 1;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();
            }
        }


        Session::flash('success','Subject Add Successfully');

        return redirect('/admin/subjects/students');
        }
        /*class  play to eight subject add ====================== ======================*/



        elseif($request->classes_id == 11 || $request->classes_id == 12){

        if($request->groupes_id==1) {  /*nine and ten science*/

        if($input['compulary_subject'] != '') {

          foreach ($input['compulary_subject'] as $key => $value) {
                $StudentSub_dist = new Student_Subject;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->session_id      = $session_id;
                $StudentSub_dist->subject_id      = $input['compulary_subject'][$key];
                $StudentSub_dist->subject_type    = 1;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();

               
           }

       }  

       if($input['compulary_subjectgroup'] != '') {

          foreach ($input['compulary_subjectgroup'] as $key => $value) {
                $StudentSub_dist = new Student_Subject;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->session_id      = $session_id;
                $StudentSub_dist->subject_id      = $input['compulary_subjectgroup'][$key];
                $StudentSub_dist->subject_type    = 1;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();
         }

    }
   if($input['subjectgroup3rd'] != '') {

          foreach ($input['subjectgroup3rd'] as $key => $value) {
                $StudentSub_dist = new Student_Subject;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->session_id      = $session_id;
                $StudentSub_dist->subject_id      = $input['subjectgroup3rd'][$key];
                $StudentSub_dist->subject_type    = 1;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();
         }

    } 


    if($input['optional4thsubject'] != '') {

          foreach ($input['optional4thsubject'] as $key => $value) {
                $StudentSub_dist = new Student_Subject;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->session_id      = $session_id;
                $StudentSub_dist->subject_id      = $input['optional4thsubject'][$key];
                $StudentSub_dist->subject_type    = 0;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();
          }

       }


         Session::flash('success','Subject Add Successfully');

         return redirect('/admin/subjects/students');

         }  /* ============end science */

        elseif($request->groupes_id==2){

             if($input['compulary_subject'] != '') {

                foreach ($input['compulary_subject'] as $key => $value) {
                        $StudentSub_dist = new Student_Subject;
                        $StudentSub_dist->student_id      = $student_id;
                        $StudentSub_dist->classes_id      = $classes_id;
                        $StudentSub_dist->session_id      = $session_id;
                        $StudentSub_dist->subject_id      = $input['compulary_subject'][$key];
                        $StudentSub_dist->subject_type    = 1;
                        $StudentSub_dist->status          = 1;
                        $StudentSub_dist->save();

                       
                   }

               }    

             if($input['compulary_subjectgroup'] != '') {

                foreach ($input['compulary_subjectgroup'] as $key => $value) {
                        $StudentSub_dist = new Student_Subject;
                        $StudentSub_dist->student_id      = $student_id;
                        $StudentSub_dist->classes_id      = $classes_id;
                        $StudentSub_dist->session_id      = $session_id;
                        $StudentSub_dist->subject_id      = $input['compulary_subjectgroup'][$key];
                        $StudentSub_dist->subject_type    = 1;
                        $StudentSub_dist->status          = 1;
                        $StudentSub_dist->save();

                       
                   }

               }  
               
              if($input['optional4thsubject'] != '') {

                foreach ($input['optional4thsubject'] as $key => $value) {
                        $StudentSub_dist = new Student_Subject;
                        $StudentSub_dist->student_id      = $student_id;
                        $StudentSub_dist->classes_id      = $classes_id;
                        $StudentSub_dist->session_id      = $session_id;
                        $StudentSub_dist->subject_id      = $input['optional4thsubject'][$key];
                        $StudentSub_dist->subject_type    = 0;
                        $StudentSub_dist->status          = 1;
                        $StudentSub_dist->save();

                       
                   }

               }  






         Session::flash('success','Subject Add Successfully');

         return redirect('/admin/subjects/students');

        }
 
        

}



    elseif($request->groupes_id==2) /* nine and ten science hummanites*/
 
    {

    }



 
 


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        $student_info = Student_Subject::where('student_id',$id)->get();

          
        return view('/admin/subjects/students/show',compact('student','student_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
