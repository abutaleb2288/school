<?php

namespace App\Http\Controllers\subjects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Subject_Teacher;

use App\models\Classes;
use App\models\Groupes;
use App\models\Sections;
use App\models\Sessions;
use App\models\Teachers;
use App\models\Subject;

class SubjectTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['subjects'] = Subject_Teacher::all();
         return view('admin.subjects.teachers.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $data['classes'] = Classes::all();
        $data['groupes'] = Groupes::all();
        $data['sessions'] = Sessions::all();
        $data['sections'] = Sections::all();
        $data["subjects"] = Subject::all();

        $data["teachers"] = Teachers::all();

        return view('admin.subjects.teachers.add',$data);
    }

     public function searchSubjectByClassId(Request $request)
    {
        $classes_id =  $request->classId;

        $allSubjects =  Subject::where('classes_id',$classes_id)->get();

        $allSubject = "";

            foreach($allSubjects as $allsub){
                $allSubject .= '<option value="'.$allsub->id.'">'.$allsub->name.'</option>';
            }
            return response()->json($allSubject);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $this->validate($request,[
            'classes_id'    => 'required',
            'subject_id'  => 'required',
            'teacher_id' => 'required',
            'sessions_id' => 'required',
            'sections_id' => 'required',
         ]);
        
          $class_id =  $request->classes_id;
          $subject_id =  $request->subject_id;
          $teacher_id =  $request->teacher_id;
          $session_id =  $request->sessions_id;
          $section_id =  $request->sections_id;

         $allreadyDistributed =  Subject_Teacher::where(['class_id' => $class_id, 'subject_id' => $subject_id,'session_id' => $session_id,'section_id' => $section_id])->count();
         if($allreadyDistributed > 0)
         {
            return redirect()->back()->with("error","This subject is already distributed for this session of this section ! You may update only");
         }
         else{
            $subDist = new Subject_Teacher();
                $subDist->class_id = $class_id;
                $subDist->subject_id = $subject_id;
                $subDist->teacher_id = $teacher_id;
                $subDist->session_id = $session_id;
                $subDist->section_id = $section_id;
              $save =   $subDist->save();

                if($save){
                    return redirect()->route('teacherdistributedsubject')->with("success","This subject is distributed successfully!");
                }else{
                   return redirect()->back()->with("error","This subject is already distributed"); 
                }

         }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['classes'] = Classes::all();
        $data['groupes'] = Groupes::all();
        $data['sessions'] = Sessions::all();
        $data['sections'] = Sections::all();
        $data["subjects"] = Subject::all();

        $data["teachers"] = Teachers::all();
        
        $data['subjectsDistribute'] = Subject_Teacher::findOrFail($id);

        $data['subjects'] = Subject::where('classes_id',$data['subjectsDistribute']->class_id)->get();

        return view('admin.subjects.teachers.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'classes_id'    => 'required',
            'subject_id'  => 'required',
            'teacher_id' => 'required',
            'sessions_id' => 'required',
            'sections_id' => 'required',
         ]);
        
          $class_id =  $request->classes_id;
          $subject_id =  $request->subject_id;
          $teacher_id =  $request->teacher_id;
          $session_id =  $request->sessions_id;
          $section_id =  $request->sections_id;

            $subDist =  Subject_Teacher::findOrFail($id);
                $subDist->class_id = $class_id;
                $subDist->subject_id = $subject_id;
                $subDist->teacher_id = $teacher_id;
                $subDist->session_id = $session_id;
                $subDist->section_id = $section_id;
                 $save =   $subDist->save();

                if($save){
                    return redirect()->route('teacherdistributedsubject')->with("success","This subject distribution is updated successfully!");
                }else{
                   return redirect()->back()->with("error","This subject distribution is not updated"); 
                }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
