<?php

namespace App\Http\Controllers\subjects;

use App\models\Subject;
use App\models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
       $classes = Classes::all();
       $subjects = Subject::where('classes_id',$request->class_id)->get();

       return view('admin/subjects/subjects/view',compact('subjects','classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = Classes::all();
        return view('admin/subjects/subjects/add',compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            
            'classname' => 'required',
            'name' => 'required',
            'full_mark' => 'required',
            'passmark' => 'required',
            'wfm' => 'required',
            'wpm' => 'required',
            'subject_type' => 'required'
            
        ]);

        $subject = New Subject();
        $subject->classes_id = $request->classname;
        $subject->name = $request->name;
        $subject->code = $request->code;
        $subject->category = $request->category;
        $subject->full_mark = $request->full_mark;
        $subject->passmark = $request->passmark;
        $subject->wfm = $request->wfm;
        $subject->wpm = $request->wpm;
        $subject->mcqfm = $request->mcqfm;
        $subject->mcqpm = $request->mcqpm;
        $subject->pfm = $request->pfm;
        $subject->ppm = $request->ppm;
        $subject->subject_type = $request->subject_type;
        $subject->status = 1;
         
        $subject->save();


        Session::flash('success','New subject are add successfully');
        return redirect('/admin/subjects');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classes = Classes::all();
        $subject = Subject::find($id);
        return view('admin/subjects/subjects/edit',compact('subject','classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
         $this->validate($request,[
            
            'classname' => 'required',
            'name' => 'required',
            'full_mark' => 'required',
            'passmark' => 'required',
            'wfm' => 'required',
            'wpm' => 'required',
            'subject_type' => 'required'
            
        ]);

        $subject = Subject::find($id);
        $subject->classes_id = $request->classname;
        $subject->name = $request->name;
        $subject->code = $request->code;
        $subject->category = $request->category;
        $subject->full_mark = $request->full_mark;
        $subject->passmark = $request->passmark;
        $subject->wfm = $request->wfm;
        $subject->wpm = $request->wpm;
        $subject->mcqfm = $request->mcqfm;
        $subject->mcqpm = $request->mcqpm;
        $subject->pfm = $request->pfm;
        $subject->ppm = $request->ppm;
        $subject->subject_type = $request->subject_type;
        $subject->status = 1;
         
        $subject->save();


        Session::flash('success',' subject are update successfully');
        return redirect('/admin/subjects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
