<?php

namespace App\Http\Controllers\teachers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Teachers;
use Session;
class TeachersController extends Controller
{
    public function index()
    {
        $teachers = Teachers::all();
        return view('/admin/teachers/view',compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         return view('/admin/teachers/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     

         $teacher = New Teachers;
         $teacher->teacher_id = $request->teacher_id;
         $teacher->name = $request->name;
         $teacher->mobile = $request->mobile;
         $teacher->email = $request->email;
         $teacher->password = $request->password;
         $teacher->designation = $request->designation;
         $teacher->nid = $request->tnid;
         $teacher->bloodgroup = $request->bloodgroup;
         $teacher->gender = $request->gender;
         $teacher->religion = $request->religion;
         $teacher->father = $request->father;
         $teacher->mother = $request->mother;
         $teacher->education = $request->education;
         $teacher->salary = $request->salary;
         $teacher->joindate = $request->joinging;
         $teacher->address = $request->address;
         $teacher->type = $request->type;
         $teacher->status = 1;

         $teacher->save();

         Session::flash('success','Teacher Add Successfully');
         return redirect('/admin/teachers');
        
 


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function show($teachers)
    {
        $teachers= Teachers::find($teachers);
        return view('/admin/teachers/show',compact('teachers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function edit($teachers)
    {
          $teachers= Teachers::find($teachers);
        return view('/admin/teachers/edit',compact('teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teachers $teachers,$id)
    {
       $this->validate($request,[
            'teacher_id' => 'required',
            'name' => 'required',
            'designation' => 'required',
            'mobile' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            'father' => 'required',
            'mother' => 'required',
            'tnid' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'education' => 'required'
            
        ]);

         $teacher = Teachers::find($id);
         $teacher->teacher_id = $request->teacher_id;
         $teacher->name = $request->name;
         $teacher->mobile = $request->mobile;
         $teacher->email = $request->email;
        
         $teacher->designation = $request->designation;
         $teacher->nid = $request->tnid;
         $teacher->bloodgroup = $request->bloodgroup;
         $teacher->gender = $request->gender;
         $teacher->religion = $request->religion;
         $teacher->father = $request->father;
         $teacher->mother = $request->mother;
         $teacher->education = $request->education;
         $teacher->salary = $request->salary;
         $teacher->joindate = $request->joinging;
         $teacher->address = $request->address;
         $teacher->status = 1;

         $teacher->save();

         Session::flash('success','Teacher Update Successfully');
         return redirect('/admin/teachers');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function destroy( $teachers)
    {
        Teachers::find($teachers)->delete();
        Session::flash('success','Teachers delete are successfully');
        return redirect('/admin/teachers');
    }
}
