<?php

namespace App\Http\Controllers\exams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Classes;
use App\models\Groupes; 
use App\models\Sessions;
use App\models\Student;
use App\models\Subject;
use App\models\Exam;
use App\models\MarkEntry;
use App\models\Student_Subject;
use Session;

class MarkEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request)
    {
       
        $classes = Classes::all();
        $sessions = Sessions::all();
        $examterm  = Exam::all();
        $subject  = Subject::all();

      $classes_id   =  $request->classes_id;
      $sessions_id  =  $request->sessions_id;
      $exam_id      =  $request->exam_id;
      $subject_id   =  $request->subject_id;

      $data['classes_id'] =  $classes_id;
      $data['sessions_id'] =  $sessions_id;
      $data['exam_id'] =  $exam_id;
      $data['subject_id'] =  $subject_id;


    if($classes_id != null && $exam_id != null && $sessions_id != null && $subject_id != null){
         $alreadyExitThisSubject =     MarkEntry::select('subject_id')->where('classes_id',$classes_id)->where('sessions_id',$sessions_id)->where('exam_id',$exam_id)->where('subject_id',$subject_id)->get();
        $count = $alreadyExitThisSubject->count();



        if($count > 0 )
        {
          $data['allStudent'] = []; 
           $data['noDataFound'] = 0;
         Session::flash('error','No Data Found! Allready Exist');
        }
        else{
                $data['allStudent'] =   Student_Subject::where('classes_id',$classes_id)->where('session_id',$sessions_id)->where('subject_id',$subject_id)->get();
             $data['noDataFound'] = $data['allStudent']->count();
        }
    }
    else{
         $data['allStudent'] = []; 
          $data['noDataFound'] = 0 ;
    }

        return view('/admin/markentry/add',compact('classes','sessions','examterm','subject'),$data);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $classes = Classes::all();
        $sessions = Sessions::all();
        $examterm  = Exam::all();
        $subject  = Subject::all();

          $classes_id   =  $request->classes_id;
          $sessions_id  =  $request->sessions_id;
          $exam_id      =  $request->exam_id;
          $subject_id   =  $request->subject_id;

          $data['classes_id'] =  $classes_id;
          $data['sessions_id'] =  $sessions_id;
          $data['exam_id'] =  $exam_id;
          $data['subject_id'] =  $subject_id;

             $alreadyExitThisSubject =     MarkEntry::select('subject_id')->where('classes_id',$classes_id)->where('sessions_id',$sessions_id)->where('subject_id',$subject_id)->get();
            $count = $alreadyExitThisSubject->count();

            if($count > 0 )
            {
              $data['allStudent'] = []; 
             Session::flash('error','No Data Found! Allready Exist');
            }
            else{
                    $data['noDataFound'] = 1;
                  $data['allStudent'] =   Student_Subject::where('classes_id',$classes_id)->where('session_id',$sessions_id)->where('subject_id',$subject_id)->get();
            }

            return view('/admin/markentry/add',compact('classes','sessions','examterm','subject'),$data);
           
    }

    public function markview(Request $request)
    {

        $classes = Classes::all();
        $sessions = Sessions::all();
        $examterm  = Exam::all();
        $subject  = Subject::all();

        $classes_id   =  $request->classes_id;
        $sessions_id  =  $request->sessions_id;
        $exam_id      =  $request->exam_id;
        $subject_id   =  $request->subject_id;

        $data['classes_id'] =  $classes_id;
        $data['sessions_id'] =  $sessions_id;
        $data['exam_id'] =  $exam_id;
        $data['subject_id'] =  $subject_id;

        $data['allStudent'] = MarkEntry::where('classes_id',$classes_id)->where('sessions_id',$sessions_id)->where('exam_id',$exam_id)->where('subject_id',$subject_id)->get();

        return view('admin/markentry/view',compact('classes','sessions','examterm'),$data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            /*  if(isset($request->id) && empty($request->id))
            {*/
             $id = $request->id;

            foreach($id as $ids)
            {

                $mark =   new MarkEntry();
              $mark->classes_id     = $request->input('classes_id');          
              $mark->sessions_id    = $request->input('sessions_id');          
              $mark->subject_id     = $request->input('subject_id');      
              $mark->student_id     = $request->input('student_id_'.$ids);      
              $mark->exam_id        = $request->input('exam_id');  
              $mark->category       = $request->input('category');      
              $mark->written        = $request->input('written_'.$ids);      
              $mark->mcq            = $request->input('mcq_'.$ids);      
              $mark->practical      = $request->input('practical_'.$ids);  
              $mark->teacher_id     = 1;  
              $mark->is_delete      = 0;  
              $mark->is_active      = 1;  
              $mark->save();  
            } 
            Session::flash('success','Mark is inserted Successfully');
              return redirect()->back();

            /*}

    else{
               Session::flash('error','Please Select Required Filed');
            return redirect()->back();
          }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAll(Request $request)
    {
       
        $data['sessions'] = Sessions::all();
        $data['examterm'] = Exam::all();

    
          $sessions_id  =  $request->sessions_id;
          $exam_id      =  $request->exam_id;

          $data['sessions_id'] =  $sessions_id;
          $data['exam_id']     =  $exam_id;

          if($request->sessions_id > 0 &&  $request->exam_id > 0)
          {
              $data['markEntry'] =   MarkEntry::where('sessions_id',$sessions_id)
                                                ->where('exam_id',$exam_id)
                                                ->groupBy('classes_id')
                                                ->get();
          }
          else{
            $data['markEntry'] = [];
            /*
                $year = date('Y');
                $session =   Sessions::where('name','like',"$year%")->first()->id;

                //$session =   Sessions::where('name','like',"$year%")->first()->id;
               $examId =   MarkEntry::select('exam_id')->where('sessions_id',$session)->get();
                $date = date('Y');
                $data['markEntry'] =    MarkEntry::where('created_at','like',"$date%")
                                                ->orderBy('exam_id',"desc")
                                                ->groupBy('classes_id')
                                                ->get();
            */
            }
        return view('/admin/markentry/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $mark =   MarkEntry::findOrFail($id);

              $mark->classes_id     = $request->input('classes_id');          
              $mark->sessions_id    = $request->input('sessions_id');          
              $mark->subject_id     = $request->input('subject_id');      
              $mark->student_id     = $request->input('student_id');      
              $mark->exam_id        = $request->input('exam_id');  
              $mark->category       = $request->input('category');      
              $mark->written        = $request->input('written');      
              $mark->mcq            = $request->input('mcq');      
              $mark->practical      = $request->input('practical');  
              $mark->teacher_id     = 0;  
              $mark->is_delete      = 0;  
              $mark->is_active      = 1;  
              $mark->save();  

            Session::flash('success','Mark is Updated Successfully');
              return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
