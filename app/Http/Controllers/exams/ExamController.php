<?php

namespace App\Http\Controllers\exams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Exam;
use Session;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $examterms = Exam::all();

        return view('/admin/examterms/view',compact('examterms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
          'name' => 'required'
        ]);

        $examterms = New Exam();
      
        $examterms->name = $request->name;
       
        $examterms->save();

        Session::flash('success','New exam term are add successfully');
        return redirect('admin/examterms');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Examterm  $examterm
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $examterm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Examterm  $examterm
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $examterm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Examterm  $examterm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $examterm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Examterm  $examterm
     * @return \Illuminate\Http\Response
     */
    public function destroy( $examterm)
    {
        Exam::find($examterm)->delete();
        Session::flash('success','Exam term are delete successfully');
        return redirect('/admin/examterms');
    }
}
