<?php

namespace App\Http\Controllers\contents;

use App\models\Book;
use App\models\Category;
use App\models\Writter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['book'] = Book::all();
        return view('/admin/contents/book/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['writter'] = Writter::where('status',1)->get();
        $data['category'] = Category::where('status',1)->get();
        return view('/admin/contents/book/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name' => 'required',
            'book_id' => 'required|unique:books,book_id',
            'category_id' => 'required',
            'writter_id' => 'required',
            'qty' => 'required',
            'edition' => 'nullable',
            'prokashoni' => 'nullable',
            'file' => 'nullable|mimes:jpeg,png,jpg',
        ]);


        $book = New Book();
        $book->name = $request->name;

        if ($request->hasFile('file')) 
            {
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/book/',$filename);
                
                $book->image = $filename;
            }
            $book->book_id = $request->book_id;
            $book->category_id = $request->category_id;
            $book->writter_id = $request->writter_id;
            $book->edition = $request->edition;
            $book->prokashoni = $request->prokashoni;
            $book->qty = $request->qty;
            $book->status = 1;
             $book->save();

        Session::flash('success','New Book is added successfully');
        return redirect('admin/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book, $id)
    {
         $data['book'] = Book::findOrFail($id);
        return view('/admin/contents/book/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book , $id)
    {
        $data['writter'] = Writter::where('status',1)->get();
        $data['category'] = Category::where('status',1)->get(); 
          $data['book'] = Book::findOrFail($id);
        return view('/admin/contents/book/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book , $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'book_id' => 'required|unique:books,book_id,'.$id,
            'category_id' => 'required',
            'writter_id' => 'required',
            'qty' => 'required',
            'edition' => 'nullable',
            'prokashoni' => 'nullable',
            'file' => 'nullable|mimes:jpeg,png,jpg',
        ]);


        $book = Book::findOrFail($id);
        $book->name = $request->name;


        if ($request->hasFile('file')) 
            {
                 if(file_exists("backend/book.{$book->image}")) {
                        unlink("backend/book.{$book->image}");
                    }

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/book/',$filename);
                
                $book->image = $filename;
            }
            else{
               $book->image =  $book->image;  
            }
            $book->book_id = $request->book_id;
            $book->category_id = $request->category_id;
            $book->writter_id = $request->writter_id;
            $book->edition = $request->edition;
            $book->prokashoni = $request->prokashoni;
            $book->qty = $request->qty;
            $book->status = 1;
             $book->save();

        Session::flash('success',' Book is updated successfully');
        return redirect('admin/book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book,$id)
    {
            $book =  Book::find($id);

                if(file_exists("backend/book.{$book->image}")) {
                        unlink("backend/book.{$book->image}");
                    }
            $book->delete();
        Session::flash('success','Book is Deleted successfully');
        return redirect('/admin/book');
    }
}
