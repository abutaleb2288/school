<?php

namespace App\Http\Controllers\contents;

use App\models\Notice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::OrderBy('id','DESC')->get();
        return view('/admin/contents/notices/view',compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/notices/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'    => 'required',
            'file'  => 'required|mimes:jpeg,png,jpg',
            'notice_by'  => 'required',
            'status'  => 'required'
         ]);

         $notices = New Notice;
        $notices->title  = $request->title;



        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('public/backend/notices/',$filename);

            $notices->file = $filename;
        }

          
         $notices->notice_by =$request->notice_by;
         $notices->status =$request->status;

         $notices->save();

         Session::flash('success','Notices add successfully');
         return redirect('/admin/notices');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show($notice)
    {
       $notices = Notice::find($notice);
        return view('/admin/contents/notices/show',compact('notices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit( $notice)
    {
        $notices = Notice::find($notice);
        return view('/admin/contents/notices/edit',compact('notices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $notice)
    {
      $this->validate($request,[
            'title'    => 'required',
            'file'  => 'required|mimes:jpeg,png,jpg',
            'notice_by'  => 'required',
            'status'  => 'required'
         ]);

         $notices = Notice::find($notice);

         $notices->title  = $request->title;

 
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('public/backend/notices/',$filename);

            $notices->file = $filename;
        }

          
         $notices->notice_by =$request->notice_by;
         $notices->status =$request->status;

         $notices->save();

         Session::flash('success','Notices add successfully');
         return redirect('/admin/notices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy( $notice)
    {
        Notice::find($notice)->delete();
        Session::flash('success','Notice are Delete successfully');
        return redirect('/admin/notices');
    }
}
