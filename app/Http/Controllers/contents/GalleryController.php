<?php

namespace App\Http\Controllers\contents;

use App\models\Gallery;
use App\models\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;


class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::all();
         return view('/admin/contents/gallery/view',compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $album = Album::all();
        return view('/admin/contents/gallery/add',compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(request $request) {

            $input=$request->all();

            $images=array();

            if($files=$request->file('image')){
                foreach($files as $file){
                    $name=$file->getClientOriginalName();
                    $file->move('public/fontend/gallery/',$name);
                    $images[]=$name;
                }
            }
            /*Insert your data*/

            Gallery::insert( [
                'album_id' =>$input['album_id'],
                'image'=>  implode("|",$images)
                //you can put other insertion here
            ]);


            return redirect('admin/gallery');
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery,$id)
    {
       
         $gallery = Gallery::find($id);
         return view('/admin/contents/gallery/show',compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery,$id)
    {
        
         $gallery = Gallery::find($id);
         return view('/admin/contents/gallery/edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {

        $input = $request->all();

       $this->validate($request,[
            'album_id' => 'required'
        ]);

         $gallery =  Gallery::find($id);
        
         if ($request->image != "") {
               unlink(public_path()."/fontend/gallery/".$gallery->image);



         if($input['compulary_group_subject2'] != ''){
            foreach ($input['compulary_group_subject2'] as $key => $value) {
                $StudentSub_dist = new StudentSub_distribution;
                $StudentSub_dist->student_id      = $student_id;
                $StudentSub_dist->student_sid     = $student_sid;
                $StudentSub_dist->classes_id      = $classes_id;
                $StudentSub_dist->sessiones_id    = $session_id;
                $StudentSub_dist->groupes_id      = $groupes_id;
                $StudentSub_dist->subject_id      = $input['compulary_group_subject2'][$key];
                $StudentSub_dist->type            = 1;
                $StudentSub_dist->status          = 1;
                $StudentSub_dist->save();
            }
        }  






           if ($request->hasFile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/fontend/gallery/',$filename); 
                $gallery->image = $filename;
            }




         }

        
         $gallery->save();

         Session::flash('success','Gallery Update successfully');
         return redirect('/admin/gallerys');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery,$id)
    {
         $result = Gallery::findOrFail($id);

        unlink(public_path()."/fontend/gallery/".$result->image);
    
        $result->delete();

        Session::flash('success','Gallery Delete successfully');
        return redirect('/admin/gallery');
 
    }
}
