<?php

namespace App\Http\Controllers\contents;

use App\models\Writter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
class WritterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
          $data['writter'] = Writter::all();
        return view('/admin/contents/writter/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/writter/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'name' => 'required',
        ]);

        $classes = New Writter();
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success','New Writter is added successfully');
        return redirect('admin/writter');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Writter  $writter
     * @return \Illuminate\Http\Response
     */
    public function show(Writter $writter,$id)
    {
         $data['writter'] = Writter::findOrFail($id);
        return view('/admin/contents/writter/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Writter  $writter
     * @return \Illuminate\Http\Response
     */
    public function edit(Writter $writter,$id)
    {
       $data['writter'] = Writter::findOrFail($id);
        return view('/admin/contents/writter/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Writter  $writter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Writter $writter,$id)
    {
         $this->validate($request,[
            'name' => 'required|',
        ]);

        $classes = Writter::findOrFail($id);
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success',' Category is updated successfully');
        return redirect('admin/writter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Writter  $writter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Writter $writter,$id)
    {
        Writter::find($id)->delete();
        Session::flash('success','Writter is Deleted successfully');
        return redirect('/admin/writter');
    }
}
