<?php

namespace App\Http\Controllers\contents;

use App\models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $contents = Content::all();
         return view('/admin/contents/contents/view',compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/contents/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'title'    => 'required',
            'description'  => 'required',
            'position'  => 'required' 
         ]);

         $content = New Content;

         $content->title=$request->title;
         $content->description=$request->description;
         $content->position=$request->position;
         $content->status=1;

         $content->save();

         Session::flash('success','Content add successfully');
         return redirect('/admin/contents');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Content  $content
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
       $contents= Content::find($id);
        return view('/admin/contents/contents/show',compact('contents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $contents=Content::find($id);
        return view('/admin/contents/contents/edit',compact('contents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'    => 'required',
            'description'  => 'required',
            'position'  => 'required' 
         ]);

         $content = Content::find($id);

         $content->title=$request->title;
         $content->description=$request->description;
         $content->position=$request->position;
         $content->status=1;

         $content->save();

         Session::flash('success','Content Update successfully');
         return redirect('/admin/contents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Content::find($id)->delete();
       Session::flash('success','Content Delete successfully');
       return redirect('/admin/contents');
    }
}
