<?php

namespace App\Http\Controllers\contents;

use App\models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $data['event'] = Event::all();
        return view('/admin/contents/event/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/event/add'); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'end_date' => 'required',
            'description' => 'required',
        ]);


        $event = New Event();
        $event->title = $request->title;

       
            $event->end_date = $request->end_date;
            $event->description = $request->description;
            $event->process = 1;
            $event->status = 1;
             $event->save();

        Session::flash('success','New Event is added successfully');
        return redirect('/admin/event'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event,$id)
    {
         $data['event'] = Event::findOrFail($id);

        return view('/admin/contents/event/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event,$id)
    {
         $data['event'] = Event::findOrFail($id);

        return view('/admin/contents/event/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'end_date' => 'required',
            'description' => 'required',
        ]);


        $event =  Event::findOrFail($id);
        $event->title = $request->title;

       
            $event->end_date = $request->end_date;
            $event->description = $request->description;
            $event->process = 1;
            $event->status = 1;
             $event->save();

        Session::flash('success',' Event is updated successfully');
        return redirect('/admin/event'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event ,$id)
    {
        $event =  Event::find($id);
        $event->delete();
        Session::flash('success','Event is Deleted successfully');
        return redirect('/admin/event');
    }
}
