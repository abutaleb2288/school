<?php

namespace App\Http\Controllers\contents;

use App\models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['category'] = Category::all();
        return view('/admin/contents/category/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('/admin/contents/category/add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:categories,name',
        ]);

        $classes = New Category();
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success','New Category is added successfully');
        return redirect('admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category,$id)
    {
        $data['category'] = Category::findOrFail($id);
        return view('/admin/contents/category/show',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category,$id)
    {
        $data['category'] = Category::findOrFail($id);
        return view('/admin/contents/category/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category,$id)
    {
         $this->validate($request,[
            'name' => 'required|unique:categories,name,'.$id,
        ]);

        $classes = Category::findOrFail($id);
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success',' Category is updated successfully');
        return redirect('admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category,$id)
    {
         Category::find($id)->delete();
        Session::flash('success','Category is Deleted successfully');
        return redirect('/admin/category');
    }
}
