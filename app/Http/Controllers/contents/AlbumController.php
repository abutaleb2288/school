<?php

namespace App\Http\Controllers\contents;

use App\models\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate \Http\Response
     */
     public function index()
    {
         $album = Album::all();
         return view('/admin/contents/albums/view',compact('album'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('/admin/contents/albums/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'name'    => 'required',
            'image'  => 'required|mimes:jpeg,png,jpg',
            'status'  => 'required'   
        ]);

         $album = New Album;
         $album->name  = $request->name;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('public/fontend/albums/',$filename);       
            $album->a_image = $filename;
        }
      
         $album->status =$request->status;

         $album->save();

         Session::flash('success','Album add successfully');
         return redirect('/admin/albums');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album,$id)
    {
         $album = Album::find($id);
         return view('/admin/contents/albums/show',compact('album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album,$id)
    {
         $album = Album::find($id);
         return view('/admin/contents/albums/edit',compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album,$id)
    {
        $this->validate($request,[
            'name'    => 'required',
            
            'status'  => 'required'   
        ]);

         $album =  Album::find($id);
         $album->name  = $request->name;

         if ($request->image != "") {
               unlink(public_path()."/fontend/albums/".$album->a_image);
           if ($request->hasFile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/fontend/albums/',$filename); 
                $album->a_image = $filename;
            }

         }

      
         $album->status =$request->status;

         $album->save();

         Session::flash('success','Album Update successfully');
         return redirect('/admin/albums');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album,$id)
    {


        $result = Album::findOrFail($id);

        unlink(public_path()."/fontend/albums/".$result->a_image);
    
        $result->delete();

        Session::flash('success','Album Delete successfully');
        return redirect('/admin/albums');

    }
}
