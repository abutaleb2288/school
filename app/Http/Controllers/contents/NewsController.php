<?php

namespace App\Http\Controllers\contents;

use App\models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::OrderBy('id','DESC')->get();
        return view('/admin/contents/news/view',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/news/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'title'    => 'required',
            'description'  => 'required',
            'status'  => 'required'
         ]);

         $news = New News;

         $news->title=$request->title;
         $news->description=$request->description;
         $news->status=$request->status;

         $news->save();

         Session::flash('success','News add successfully');
         return redirect('/admin/news');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show( $news)
    {
       $news= News::find($news);
        return view('/admin/contents/news/show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit( $news)
    {
        $news= News::find($news);
        return view('/admin/contents/news/edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $news)
    {
        $this->validate($request,[
            'title'    => 'required',
            'description'  => 'required',
            'status'  => 'required'
         ]);

         $news = News::find($news);

         $news->title=$request->title;
         $news->description=$request->description;
         $news->status=$request->status;

         $news->save();

         Session::flash('success','News Update successfully');
         return redirect('/admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($news)
    {
       News::find($news)->delete();
       Session::flash('success','News Delete successfully');
       return redirect('/admin/news');
    }
}