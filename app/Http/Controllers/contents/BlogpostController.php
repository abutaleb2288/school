<?php

namespace App\Http\Controllers\contents;

use App\models\Blogpost;
use App\models\Blogcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class BlogpostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['post'] = Blogpost::all();
        return view('/admin/contents/blogpost/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category'] = Blogcategory::where('status',1)->get();
        return view('/admin/contents/blogpost/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'Thumbnail' => 'nullable|mimes:jpeg,png,jpg',
        ]);


        $post = New Blogpost();
        $post->title = $request->title;
        $post->user_id = Auth::user()->id;

        if ($request->hasFile('thumnail')) 
            {
                $file = $request->file('thumnail');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/blogpost/',$filename);
                
                $post->thumnail = $filename;
            }
            $post->category_id = $request->category_id;
            $post->description = $request->description;
            $post->status = 1;
             $post->save();

        Session::flash('success','New Post is added successfully');
        return redirect('/admin/blog/post');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function show(Blogpost $blogpost,$id)
    {
         $data['post'] = Blogpost::findOrFail($id);

        return view('/admin/contents/blogpost/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function edit(Blogpost $blogpost,$id)
    {
        $data['category'] = Blogcategory::where('status',1)->get(); 
        $data['post'] = Blogpost::findOrFail($id);
        return view('/admin/contents/blogpost/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blogpost $blogpost ,$id)
    {
         $this->validate($request,[
            'title' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'Thumbnail' => 'nullable|mimes:jpeg,png,jpg',
        ]);


        $post =  Blogpost::findOrFail($id);
        $post->title = $request->title;
         $post->user_id = Auth::user()->id;


        if ($request->hasFile('thumnail')) 
            {
                if(file_exists("backend/blogpost.{$post->thumnail}")) {
                        unlink("backend/blogpost.{$post->thumnail}");
                    }

                $file = $request->file('thumnail');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/blogpost/',$filename);
                
                $post->thumnail = $filename;
            }
             else{
               $post->thumnail =  $post->thumnail;  
            }

            $post->category_id = $request->category_id;
            $post->description = $request->description;
            $post->status = 1;
             $post->save();

        Session::flash('success',' Post is Updated successfully');
        return redirect('/admin/blog/post');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blogpost $blogpost,$id)
    {
          $post =  Blogpost::find($id);

                 if(file_exists("backend/blogpost.{$post->thumnail}")) {
                        unlink("backend/blogpost.{$post->thumnail}");
                    }

         $post->delete();
        Session::flash('success','Post is Deleted successfully');
        return redirect('/admin/blog/post');
    }
}
