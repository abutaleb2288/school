<?php

namespace App\Http\Controllers\contents;
use App\models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['slider'] = Slider::all();
        return view('/admin/contents/slider/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/contents/slider/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->validate($request,[
            'file' => 'required|mimes:jpeg,png,jpg',
        ]);


        $slider = New Slider();

        if ($request->hasFile('file')) 
            {
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/slider/',$filename);
                
                $slider->image = $filename;
            }
            $slider->status = 1;
             $slider->save();

        Session::flash('success','New Slider is added successfully');
        return redirect('admin/slider');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider ,$id)
    {
         $data['slider'] = Slider::findOrFail($id);
        return view('/admin/contents/slider/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,$id)
    {
          $data['slider'] = Slider::findOrFail($id);
        return view('/admin/contents/slider/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider,$id)
    {
        $this->validate($request,[
            'file' => 'nullable|mimes:jpeg,png,jpg',
        ]);


        $slider =  Slider::findOrFail($id);

        if ($request->hasFile('file')) 
            {

                 if(file_exists("backend/slider.{$slider->image}")) {
                        unlink("backend/slider.{$slider->image}");
                    }


                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('public/backend/slider/',$filename);
                
                $slider->image = $filename;
            }
             else{
               $slider->image =  $slider->image;  
            }
            $slider->status = 1;
             $slider->save();

        Session::flash('success',' Slider is Updated successfully');
        return redirect('admin/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider,$id)
    {
        $slider =  Slider::find($id);

                if(file_exists("backend/slider.{$slider->image}")) {
                        unlink("backend/slider.{$slider->image}");
                    }
            $slider->delete();
        Session::flash('success','Slider is Deleted successfully');
        return redirect('/admin/slider');
    } 
}
