<?php

namespace App\Http\Controllers\contents;

use App\models\Blogcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class BlogcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['category'] = Blogcategory::all();
        return view('/admin/contents/blogcategory/view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('/admin/contents/blogcategory/add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:blogcategories,name',
        ]);

        $classes = New Blogcategory();
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success','New  Blog Category is added successfully');
        return redirect('admin/blog/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['category'] = Blogcategory::findOrFail($id);
        return view('/admin/contents/blogcategory/show',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = Blogcategory::findOrFail($id);
        return view('/admin/contents/blogcategory/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'name' => 'required|unique:blogcategories,name,'.$id,
        ]);

        $classes = Blogcategory::findOrFail($id);
        $classes->name = $request->name;
        $classes->status = 1;
        $classes->save();

        Session::flash('success','Blog Category is updated successfully');
        return redirect('admin/blog/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Blogcategory::findOrFail($id)->delete();
        Session::flash('success','Blog Category is Deleted successfully');
        return redirect('/admin/blog/category');
    }
}
