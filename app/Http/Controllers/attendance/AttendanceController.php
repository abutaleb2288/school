<?php

namespace App\Http\Controllers\attendance;

use App\models\Classes;
use App\models\Groupes;
use App\models\Sections;
use App\models\Sessions;
use App\models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AttendanceController extends Controller
{
     
    public function studentattendance(Request $request , $allDataGet = null)
    {

        $classes = Classes::all();
        $groupes = Groupes::all();
        $sessions = Sessions::all();
        $sections = Sections::all();

       
        if($allDataGet == 'all'){

            $student = Student::all();
            return view('/admin/attendance/student_attendance',compact('student','classes','groupes','sessions','sections'));
        }
        else{

                if($request->classes_id != null){
                    $classes_id = $request->classes_id;
                }else{
                    $classes_id = null; 
                }

                if($request->sessions_id != null){
                    $sessions_id = $request->sessions_id;
                }else{
                    $sessions_id = null; 
                }

                if($request->groupes_id != null){
                    $groupes_id = $request->groupes_id;
                }else{
                    $groupes_id = null; 
                }

                if($request->sections_id != null){
                    $sections_id = $request->sections_id;
                }else{
                    $sections_id = null; 
                }


                        $data['classes_search'] = $classes_id;
                        $data['sessions_search'] = $sessions_id;
                  $student = Student::where(['classes_id' => $classes_id, 'sessions_id' => $sessions_id])->get();


            if($groupes_id != null && $sections_id != null ){
                  $student  =  $student->where('groupes_id' ,'=', $groupes_id)->where('sections_id', '=', $sections_id);
            }
            else{
                    $student ;
            }

            if($groupes_id != null){

                $data['groupes_search'] = $groupes_id;
                $student =  $student->where('groupes_id', '=' ,$groupes_id );
            }
            if($sections_id != null){
                $data['sections_search'] = $sections_id;
                $student =  $student->where('sections_id', '=', $sections_id );
            }

            if ($classes_id !=null && $sessions_id !=null) {
            $client = new Client();
            //$response = $client->request('GET', 'http://api-inovace360.com/api/v1/people/?api_token=2815-a4d3-f590-5a5a-5efd-50fd-3b78-0b2c-43f9-0ed4-7dc2-f0f9-82f7-0284-5a7e-8c12');
            $response = $client->request('GET', 'http://api-inovace360.com/api/v1/logs/?start=2019-06-23 17:29:58&end=2019-07-21
16:10:00&api_token=2815-a4d3-f590-5a5a-5efd-50fd-3b78-0b2c-43f9-0ed4-7dc2-f0f9-82f7-0284-5a7e-8c12&per_page=100');
            $get_data = json_decode($response->getBody()->getContents());
            //dd($response);
            }else{
                $get_data = "";
            }


                return view('/admin/attendance/student_attendance',compact('student','classes','groupes','sessions','sections','get_data'),$data);
            
        
    }



}














}
