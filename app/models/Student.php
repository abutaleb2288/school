<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function groups(){
    	return $this->belongsTo(Groupes::class,'groupes_id');
    }

    public function classs(){
    	return $this->belongsTo(Classes::class,'classes_id');
    }

    public function sections(){
    	return $this->belongsTo(Sections::class,'sections_id');
    }

    public function sessions(){
    	return $this->belongsTo(Sessions::class,'sessions_id');
    }
}