<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function classes()
    {
    	return $this->belongsTo(Classes::class);
    }
}
