<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Student_Subject extends Model
{

	protected $table = "student_subjects";

    public function studentTable(){
        return $this->belongsTo(Student::class,'student_id');
    }

    public function groups(){
        return $this->belongsTo(Groupes::class,'groupes_id');
    }

    public function classs(){
        return $this->belongsTo(Classes::class,'classes_id');
    }

    public function sections(){
        return $this->belongsTo(Sections::class,'sections_id');
    }

    public function sessions(){
        return $this->belongsTo(Sessions::class,'session_id');
    } 

    public function subject(){
        return $this->belongsTo(Subject::class,'subject_id');
    }



}
