<?php

namespace App\models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\models\Teachers;
class Routine extends Model
{
    
  	public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }

    public function time(){
        return $this->belongsTo(Time::class,'time_id');
    }

 	public function endtime(){
    	return $this->belongsTo(Time::class,'endtime_id');
    }

 	public function day(){
    	return $this->belongsTo(Day::class,'day_id');
    }

  	public function class(){
    	return $this->belongsTo(Classes::class,'class_id');
    }


    public function section(){
    	return $this->belongsTo(Sections::class,'section_id');
    }

    public function session(){
    	return $this->belongsTo(Sessions::class,'session_id');
    }

    public function teachers(){
    	return $this->belongsTo(Teachers::class,'teacher_id');
    }

    public function subject(){
    	return $this->belongsTo(Subject::class,'subject_id');
    }
}
