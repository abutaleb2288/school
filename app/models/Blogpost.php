<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Blogpost extends Model
{
    public function category(){
    	return $this->belongsTo(Blogcategory::class,'category_id');
    }

}
