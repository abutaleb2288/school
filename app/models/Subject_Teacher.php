<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Subject_Teacher extends Model
{
  public function class(){
    	return $this->belongsTo(Classes::class,'class_id');
    }


    public function section(){
    	return $this->belongsTo(Sections::class,'section_id');
    }

    public function session(){
    	return $this->belongsTo(Sessions::class,'session_id');
    }

    public function teachers(){
    	return $this->belongsTo(Teachers::class,'teacher_id');
    }

    public function subject(){
    	return $this->belongsTo(Subject::class,'subject_id');
    }

   }