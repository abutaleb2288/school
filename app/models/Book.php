<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }

    public function writter(){
    	return $this->belongsTo(Writter::class,'writter_id');
    } 
}
