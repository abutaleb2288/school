<?php

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('welcome');
});
  
Auth::routes();





Route::group(['middleware' => 'auth'], function() {

Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'prefix' => '/admin/'], function() {


/* ================= student setting  ========================== */
	Route::group(['namespace' => 'students'], function() {		
			Route::get('classes','StudentSetting@classes');
			Route::get('sessions','StudentSetting@sessions');
			Route::get('sections','StudentSetting@sections');
			Route::get('groupes','StudentSetting@groupes');			  
	});

		/* ================= student setting  ========================== */
	Route::group(['namespace' => 'students'], function() {		
		  Route::get('students/{all?}','StudentController@index');

	});


	/* ================= Subject setting  ========================== */
	Route::group(['namespace' => 'subjects'], function() {		
			Route::get('subjects','SubjectController@index');
			Route::get('subjects/create','SubjectController@create');
			Route::post('subjects/store','SubjectController@store');
			Route::get('subjects/edit/{id}','SubjectController@edit');
			Route::post('subjects/update/{id}','SubjectController@update');

			
			Route::get('subjects/students','StudentSubjectController@index');
			Route::get('subjects/students/add/{id}','StudentSubjectController@create');
			Route::post('/subjects/students/store','StudentSubjectController@store');
			Route::get('/subjects/student/show/{id}','StudentSubjectController@show');

			// Teacher Subject distributed route...
			Route::get('subjects/teachers/view','SubjectTeacherController@index')->name("teacherdistributedsubject");
			Route::get('subject/teacher/create','SubjectTeacherController@create');
			Route::get('subjects/teachers/edit/{id}','SubjectTeacherController@edit');
			Route::post('subjects/teachers','SubjectTeacherController@store');
			Route::post('subjects/teachers/update/{id}','SubjectTeacherController@update');
			

			Route::get('subjects/search/by/class/id','SubjectTeacherController@searchSubjectByClassId');	 	  
	});

	Route::group(['namespace' => 'teachers'], function() {

			Route::get('/teachers','TeachersController@index');
			Route::get('/teachers/create','TeachersController@create');
			Route::post('/teachers/store','TeachersController@store');
			Route::get('/teachers/show/{id}','TeachersController@show');
			Route::get('/teachers/edit/{id}','TeachersController@edit');
			Route::post('/teachers/update/{id}','TeachersController@update');
			Route::get('/teachers/delete/{id}','TeachersController@destroy');


	});

	/* ================= Exam term setting  ========================== */
	Route::group(['namespace' => 'exams'], function() {		
			Route::get('examterms','ExamController@index');
			Route::post('examterms/store','ExamController@store');
			Route::get('examterms/delete/{id}','ExamController@destroy');
		 	 	  
	});

  


		/* ================= Exam term setting  & Mark Entry========================== */
	Route::group(['namespace' => 'exams'], function() {		
			Route::get('markentry','MarkEntryController@index');
			Route::get('markentry/view/mark','MarkEntryController@markview');
			Route::post('markentry/store','MarkEntryController@store');
			Route::get('markentry/show/all','MarkEntryController@showAll');
			//Route::get('markentry/show/{id}','MarkEntryController@show');
			//Route::get('markentry/edit/{id}','MarkEntryController@edit');
			Route::post('markentry/update/{id}','MarkEntryController@update');
			//Route::get('markentry/delete/{id}','MarkEntryController@destroy');
		 	 	  
	});	


	/* ================= Attendance  setting  ========================== */
	Route::group(['namespace' => 'attendance'], function() {
			
			Route::get('/attendances/students','AttendanceController@studentattendance');
			Route::get('/attendances/teachers','AttendanceController@studentattendance');
			 
		 	 	  
	});


 
	/* ================= Routine  setting  ========================== */
	Route::group(['namespace' => 'Routine'], function() {
			Route::get('routine/index','RoutineController@index');
			Route::get('/routine/create','RoutineController@create');
			Route::post('routine/store','RoutineController@store');
			Route::get('routine/show/{id}','RoutineController@show');
			Route::get('routine/edit/{id}','RoutineController@edit');
			Route::post('routine/update/{id}','RoutineController@update');
			Route::get('routine/delete/{id}','RoutineController@destroy');
		 	 	  
	});

	/* ================= news  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {
			Route::get('/news','NewsController@index');
			Route::get('/news/add','NewsController@create');
			Route::post('/news/store','NewsController@store');
			Route::get('/news/show/{id}','NewsController@show');
			Route::get('/news/edit/{id}','NewsController@edit');
			Route::post('/news/update/{id}','NewsController@update');
			Route::get('/news/delete/{id}','NewsController@destroy');


		 	 	  
	});

	/* ================= content  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {

			Route::get('/contents','ContentController@index');
			Route::get('/contents/add','ContentController@create');
			Route::post('/contents/store','ContentController@store');
			Route::get('/contents/show/{id}','ContentController@show');
			Route::get('/contents/edit/{id}','ContentController@edit');
			Route::post('/contents/update/{id}','ContentController@update');
			Route::get('/contents/delete/{id}','ContentController@destroy');
 	 	 	  
	});

	/* ================= content  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {
		
 			Route::get('/notices','NoticeController@index');
			Route::get('/notices/add','NoticeController@create');
			Route::post('/notices/store','NoticeController@store');
			Route::get('/notices/show/{id}','NoticeController@show');
			Route::get('/notices/edit/{id}','NoticeController@edit');
			Route::post('/notices/update/{id}','NoticeController@update');
			Route::get('/notices/delete/{id}','NoticeController@destroy'); 	 	  
	});


	/* ================= albums  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {
			Route::get('albums','AlbumController@index');
			Route::get('albums/add','AlbumController@create');
			Route::post('albums/store','AlbumController@store');
			Route::get('albums/show/{id}','AlbumController@show');
			Route::get('albums/edit/{id}','AlbumController@edit');
			Route::post('albums/update/{id}','AlbumController@update');
			Route::get('albums/delete/{id}','AlbumController@destroy'); 	 	  
	});

	/* ================= albums  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 			Route::get('/gallery','GalleryController@index');
			Route::get('/gallery/add','GalleryController@create');
			Route::post('/gallery/store','GalleryController@store');
			Route::get('/gallery/show/{id}','GalleryController@show');
			Route::get('/gallery/edit/{id}','GalleryController@edit');
			Route::post('/gallery/update/{id}','GalleryController@update');
			Route::get('/gallery/delete/{id}','GalleryController@destroy');
	 	  
	});
		/* ================= Slider  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	 
 			Route::get('/slider','SliderController@index');
			Route::get('/slider/add','SliderController@create');
			Route::post('/slider/store','SliderController@store');
			Route::get('/slider/show/{id}','SliderController@show');
			Route::get('/slider/edit/{id}','SliderController@edit');
			Route::post('/slider/update/{id}','SliderController@update');
			Route::get('/slider/delete/{id}','SliderController@destroy');
	 	  
	});

	/* ================= Event  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	 
 			Route::get('/event','EventController@index');
			Route::get('/event/add','EventController@create');
			Route::post('/event/store','EventController@store');
			Route::get('/event/show/{id}','EventController@show');
			Route::get('/event/edit/{id}','EventController@edit');
			Route::post('/event/update/{id}','EventController@update');
			Route::get('/event/delete/{id}','EventController@destroy');
	 	  
	});

	/* ================= category  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	  		Route::get('/blog/category','BlogcategoryController@index');
			Route::get('/blog/category/add','BlogcategoryController@create');
			Route::post('/blog/category/store','BlogcategoryController@store');
			Route::get('/blog/category/show/{id}','BlogcategoryController@show');
			Route::get('/blog/category/edit/{id}','BlogcategoryController@edit');
			Route::post('/blog/category/update/{id}','BlogcategoryController@update');
			Route::get('/blog/category/delete/{id}','BlogcategoryController@destroy');
	 	  
	});	

	/* ================= Blog post  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	  		Route::get('/blog/post','BlogpostController@index');
			Route::get('/blog/post/add','BlogpostController@create');
			Route::post('/blog/post/store','BlogpostController@store');
			Route::get('/blog/post/show/{id}','BlogpostController@show');
			Route::get('/blog/post/edit/{id}','BlogpostController@edit');
			Route::post('/blog/post/update/{id}','BlogpostController@update');
			Route::get('/blog/post/delete/{id}','BlogpostController@destroy');
	 	  
	});

		/* ================= Blog post  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	  		Route::get('/writter','WritterController@index');
			Route::get('/writter/add','WritterController@create');
			Route::post('/writter/store','WritterController@store');
			Route::get('/writter/show/{id}','WritterController@show');
			Route::get('/writter/edit/{id}','WritterController@edit');
			Route::post('/writter/update/{id}','WritterController@update');
			Route::get('/writter/delete/{id}','WritterController@destroy');
	 	  
	});

			/* ================= Book Category  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	  		Route::get('/category','CategoryController@index');
			Route::get('/category/add','CategoryController@create');
			Route::post('/category/store','CategoryController@store');
			Route::get('/category/show/{id}','CategoryController@show');
			Route::get('/category/edit/{id}','CategoryController@edit');
			Route::post('/category/update/{id}','CategoryController@update');
			Route::get('/category/delete/{id}','CategoryController@destroy');
	 	  
	});
			/* ================= Book  setting  ========================== */
	Route::group(['namespace' => 'contents'], function() {		
 	  		
 	  		Route::get('/book','BookController@index');
			Route::get('/book/add','BookController@create');
			Route::post('/book/store','BookController@store');
			Route::get('/book/show/{id}','BookController@show');
			Route::get('/book/edit/{id}','BookController@edit');
			Route::post('/book/update/{id}','BookController@update');
			Route::get('/book/delete/{id}','BookController@destroy');	 	  
	});	

		/* ================= Account Expense  setting  ========================== */
	Route::group(['namespace' => 'accounts'], function() {		
 	  		
 	  		Route::get('daily/expense/index','ExpenseController@index')->name('expenseindex');
			Route::get('daily/expense/create','ExpenseController@create');
			Route::post('daily/expense/store','ExpenseController@store');
			Route::get('daily/expense/show/{id}','ExpenseController@show');
			Route::get('daily/expense/edit/{id}','ExpenseController@edit');
			Route::post('daily/expense/update/{id}','ExpenseController@update');
			Route::get('daily/expense/delete/{id}','ExpenseController@destroy');

			//--report expense
			Route::get('expense/report','ExpenseController@expenseReport');	 	  
	});	

		/* ================= Account Expense purpose setting  ========================== */
	Route::group(['namespace' => 'accounts'], function() {		
 	  		
 	  	Route::get('expense/purpose/index','Expense_perposeController@index')->name('expensePurposeIndex');
		Route::get('expense/purpose/create','Expense_perposeController@create');
		Route::post('expense/purpose/store','Expense_perposeController@store');
		Route::get('expense/purpose/show/{id}','Expense_perposeController@show');
		Route::get('expense/purpose/edit/{id}','Expense_perposeController@edit');
		Route::post('expense/purpose/update/{id}','Expense_perposeController@update');
		Route::get('expense/purpose/delete/{id}','Expense_perposeController@destroy');	  
	});	

		/* ================= Account Salary setting  ========================== */
	Route::group(['namespace' => 'accounts'], function() {		
 	  		
 	        Route::get('salary/index','SalaryController@index')->name('salaryIndex');
			Route::get('salary/create','SalaryController@create');
			Route::post('salary/store','SalaryController@store');
			Route::get('salary/show/{id}','SalaryController@show');
			Route::get('salary/edit/{id}','SalaryController@edit');
			Route::post('salary/update/{id}','SalaryController@update');
			Route::get('salary/delete/{id}','SalaryController@destroy');

	});




 



});

});