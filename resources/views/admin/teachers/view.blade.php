@extends('layouts.app')
@section('title','All Teachers')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>All Teacher's List</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                <a href="{{ url('/admin/teachers/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New </a>
            </li>
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                            	 @if(Session::get('success'))
                                   <p class="alert alert-success">{{ Session::get('success') }}</p>
                              @elseif(Session::get('error'))
                               <p class="alert alert-danger">{{ Session::get('error') }}</p>
                              @endif

                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Teacher ID</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Subject</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@php
                                    	 $i=1;
                                    	@endphp
                                    	@foreach($teachers as $value)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $value->teacher_id }}</td>
                                            <td>{{ $value->name }}</td>
                                            
                                            <td>{{ $value->mobile }}</td>
                                            <td>{{ $value->email }}</td>
                                            <td>{{ $value->subject }}</td>
                                            <td>
                                                <a href="{{ url('/admin/teachers/show/'.$value->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a>
                                                <a href="{{ url('/admin/teachers/edit/'.$value->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                                                <a href="{{ url('/admin/teachers/delete/'.$value->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item')"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                         </tr>
                                         @endforeach
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
@endsection
