@extends('layouts.app');
@section('title','Edit Teacher')
@section('content')
       <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Edit Teacher</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
				                         @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                            
                             <form method="post" action="{{ url('/admin/teachers/update/'.$teachers->id) }}"> 
                             	@csrf
                              <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Teacher ID :</label>
                                      <input type="text" class="form-control" name="teacher_id" value="{{ $teachers->teacher_id }}"  placeholder="Teacher ID" required>
                                 </div> 


                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher name">Name :</label>
                                      <input type="text" class="form-control" name="name" value="{{ $teachers->name }}" placeholder="Teacher name" required>
                                 </div> 
                                 <div class="col-xs-12 col-md-3">
                                      <label for="designation name">Designation :</label>
                                      <input type="text" class="form-control" name="designation" value="{{ $teachers->designation }}"  placeholder="Teacher 	designation" required>
                                 </div>   
                                   
 								  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher mobile">Mobile :</label>
                                      <input type="text" class="form-control" name="mobile" value="{{ $teachers->mobile }}" placeholder="Teacher Mobile" required>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="email">Email:</label>
                                      <input type="email" class="form-control" name="email" value="{{ $teachers->email }}" placeholder="Teacher Email" required>
                                 </div>  
                                <div class="col-xs-12 col-md-3">
                                      <label for="Fatehr">Father Name :</label>
                                      <input type="text" class="form-control" name="father" value="{{ $teachers->father }}" placeholder="Father name" required>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Mother">Mother Name :</label>
                                      <input type="text" class="form-control" name="mother" value="{{ $teachers->mother }}" placeholder="Mother Name" required>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="TeNationalacher ID">National ID :</label>
                                      <input type="text" class="form-control" name="tnid" value="{{ $teachers->tnid }}" placeholder="National ID" required>
                                 </div>
                                <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Salary :</label>
                                      <input type="number" class="form-control" name="salary" value="{{ $teachers->salary }}" placeholder="Salary" required>
                                 </div> 
                               <div class="col-xs-12 col-md-3">
                                      <label for="Gender">Gender :</label>
                                      <select name="gender"  class="form-control">
                                           @if($teachers->gender=='Male')
                                           <option value="Male">Male</option>
                                           <option value="Female">Female</option>
                                           @elseif($teachers->gender=='Female')
                                           @endif
                                      </select>
                                 </div>
                                <div class="col-xs-12 col-md-3">
                                      <label for="Gender">Religion :</label>
                                      <select name="religion"  class="form-control">
                                           @if($teachers->religion=='Islam')
                                           <option value="Islam">Islam</option>
                                           <option value="Hindu">Hindu</option>
                                           <option value="Chistran">Chistran</option>
                                           <option value="boudho">Boudho</option>
                                           @elseif($teachers->religion=='Hindu')
                                           <option value="Hindu">Hindu</option>
                                           <option value="Islam">Islam</option>
                                           <option value="Chistran">Chistran</option>
                                           <option value="boudho">Boudho</option> 
                                          @elseif($teachers->religion=='Chistran')
                                           <option value="Chistran">Chistran</option>
                                           <option value="Islam">Islam</option>
                                           <option value="Hindu">Hindu</option>
                                           <option value="boudho">Boudho</option> 
                                          @elseif($teachers->religion=='Boudho')
                                           <option value="Boudho">Boudho</option>
                                           <option value="Islam">Islam</option>
                                           <option value="Hindu">Hindu</option>
                                           <option value="Chistran">Chistran</option>
                                          @endif



                                         </select>
                                 </div>
                                   
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Birthday :</label>
                                      <input type="text" name="birthday" class="form-control" value="{{ $teachers->birthday }}" id="single_cal1">
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Blood ">Blood Group :</label>
                                       <select name="bloodgroup" id="" class="form-control">
                                           <option value="">Choose Blood Group</option>
                                           <option value="A+">A+</option>
                                           <option value="A-">A-</option>
                                           <option value="B+">B+</option>
                                           <option value="B-">B-</option>
                                           <option value="AB+">AB+</option>
                                           <option value="AB-">AB-</option>
                                           <option value="O+">O+</option>
                                           <option value="O-">O-</option>
                                            
                                       </select>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Joining Date :</label>
                                      <input type="text" class="form-control" id="single_cal3" value="{{ $teachers->joining }}" name="joinging" placeholder="joining" required>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Education :</label>
                                       <textarea name="education" class="form-control" id="" value="" cols="" rows="">{{ $teachers->education }}</textarea>
                                 </div> 
                                 <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Address :</label>
                          <textarea name="address" class="form-control" id="" value="" cols="" rows="">{{ $teachers->address }}</textarea>
                                 </div>  
                               
                                 
                                 <div class="col-xs-12 col-md-12">
                                     <label for=""></label>
                                     <br>
                                     <input type="submit" class="btn btn-primary" value="Submit">
                                 </div>
                                 

                                  
                            </form>
                    <!-- end form for validations -->
                              


                            </div>
                        </div>
                    </div>



@endsection