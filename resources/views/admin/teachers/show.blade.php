@extends('layouts.app')
@section('title','Teacher View')
@section('content')

 <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Teacher Details View</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Information</th>
                                            <th>Item</th>
                                            <th>Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Teacher ID</th>
                                            <td>{{ $teachers->teacher_id }}</td>
                                            <th>Name</th>
                                            <td>{{ $teachers->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>RF_ID</th>
                                            <td>{{ $teachers->rf_id }}</td>
                                            <th>Father Name</th>
                                            <td>{{ $teachers->father }}</td>
                                        </tr>
                                        <tr>
                                            <th>Monther Name</th>
                                            <td>{{ $teachers->mother }}</td>
                                            <th>National ID</th>
                                            <td>{{ $teachers->nid }}</td>
                                        </tr>
                                        <tr>
                                            <th>Birthday</th>
                                            <td>{{ $teachers->birthday }}</td>
                                            <th>Blood Group</th>
                                            <td>{{ $teachers->bloodgroup }}</td>
                                        </tr>  
                                       <tr>
                                            <th>Designation</th>
                                            <td>{{ $teachers->designation }}</td>
                                            <th>Mobile</th>
                                            <td>{{ $teachers->mobile }}</td>
                                        </tr>
                                        <tr>
                                        	<th>Email</th>
                                        	<td>{{ $teachers->email }}</td>
                                        	<th>Stage</th>
                                        	<td>
                                        		@if($teachers->prj_id==1)
					                               Primary (1 to 8)
					                              @elseif($teachers->prj_id==2)
					                              Secondery ( 9 to 10)
					                              @elseif($teachers->prj_id==3)
					                              Higher Secondery (11 to 12)
					                              @else
					                              Hon's / Degree
					                              @endif
                                        	</td>
                                        </tr>

                                        <tr>
                                            <th>Gender</th>
                                            <td>{{ $teachers->gender }}</td>
                                            <th>Religion</th>
                                            <td>{{ $teachers->religion }}</td>
                                        </tr>   
                                        <tr>
                                            <th>Salary</th>
                                            <td>{{ $teachers->salary }}</td>
                                            <th>Education</th>
                                            <td>{{ $teachers->education }}</td>
                                        </tr>
                                        <tr>
                                            <th>Joining Date</th>
                                            <td>{{ $teachers->joining }}</td>
                                            <th>Address</th>
                                            <td>{{ $teachers->address }}</td>
                                        </tr>
                                        <tr>
                                        	<th>Status</th>
                                             <td colspan="3">
                                             	@if($teachers->status==1)
                                             	<p class="btn btn-primary btn-xs">Active</p>
                                             	@elseif($teachers->status==0)
                                             	<p class="btn btn-danger">Deactive</p>
                                             	@endif
                                             </td>
                                        </tr>
                                         
                                        
                                        <tr>
                                           <th>Action</th>
                                            <td colspan="3">
                                                <a href="" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> View</a>
                                                <a href="" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                                                <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                            
                                         </tr>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>


@endsection