@extends('layouts.app');
@section('title','Add New Teacher')
@section('content')
       <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Add New Teacher</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
				                         @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                            
                             <form method="post" action="{{ url('/admin/teachers/store') }}"> 
                             	@csrf
                              <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Teacher ID :</label>
                                      <input type="text" class="form-control" name="teacher_id" value="{{ old('teacher_id') }}"  placeholder="Teacher ID" >
                                 </div> 


                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher name">Name :</label>
                                      <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Teacher name" required>
                                 </div> 
                                 <div class="col-xs-12 col-md-3">
                                      <label for="designation name">Designation :</label>
                                      <input type="text" class="form-control" name="designation" value="{{ old('designation') }}"  placeholder="Teacher 	designation" >
                                 </div>   
                                   
 								  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher mobile">Mobile :</label>
                                      <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Teacher Mobile" >
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="email">Email:</label>
                                      <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Teacher Email" >
                                 </div>  
                                <div class="col-xs-12 col-md-3">
                                      <label for="Password">Password :</label>
                                      <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Password" >
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Fatehr">Father Name :</label>
                                      <input type="text" class="form-control" name="father" value="{{ old('father') }}" placeholder="Father name" >
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Mother">Mother Name :</label>
                                      <input type="text" class="form-control" name="mother" value="{{ old('mother') }}" placeholder="Mother Name" >
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="TeNationalacher ID">National ID :</label>
                                      <input type="text" class="form-control" name="tnid" value="{{ old('tnid') }}" placeholder="National ID" >
                                 </div>
                                <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Salary :</label>
                                      <input type="number" class="form-control" name="salary" value="{{ old('salary') }}" placeholder="Salary" >
                                 </div> 
                               <div class="col-xs-12 col-md-3">
                                      <label for="Gender">Gender :</label>
                                      <select name="gender"  class="form-control">
                                          
                                           <option value="Male">Male</option>
                                           <option value="Female">Female</option>
                                      </select>
                                 </div>
                                <div class="col-xs-12 col-md-3">
                                      <label for="Gender">Religion :</label>
                                      <select name="religion"  class="form-control">
                                          
                                           <option value="Islam">Islam</option>
                                           <option value="Hindu">Hindu</option>
                                           <option value="Chistran">Chistran</option>
                                           <option value="boudho">Boudho</option>
                                         </select>
                                 </div>
                                   
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Birthday :</label>
                                      <input type="text" name="birthday" class="form-control" value="{{ old('birthday') }}" id="single_cal1">
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Blood ">Blood Group :</label>
                                       <select name="bloodgroup" id="" class="form-control">
                                           <option value="">Choose Blood Group</option>
                                           <option value="A+">A+</option>
                                           <option value="A-">A-</option>
                                           <option value="B+">B+</option>
                                           <option value="B-">B-</option>
                                           <option value="AB+">AB+</option>
                                           <option value="AB-">AB-</option>
                                           <option value="O+">O+</option>
                                           <option value="O-">O-</option>
                                            
                                       </select>
                                 </div>  
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Joining Date :</label>
                                      <input type="text" class="form-control" id="single_cal3" value="{{ old('joining') }}" name="joinging" placeholder="joining" >
                                 </div> 
                                  <div class="col-xs-12 col-md-3">
                                      <label for="Gender">Role :</label>
                                      <select name="type"  class="form-control" required="">
                                           
                                           <option value="1">শিক্ষক</option>
                                           <option value="2">তৃতীয় শ্রেণির  </option>
                                           <option value="3">চতুর্থ শ্রেণির</option>
                                            
                                         </select>
                                 </div>


                                  <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Education :</label>
                                       <textarea name="education" class="form-control" id="" value="" cols="" rows="">{{ old('education') }}</textarea>
                                 </div> 
                                 <div class="col-xs-12 col-md-3">
                                      <label for="Teacher ID">Address :</label>
                          <textarea name="address" class="form-control" id="" value="" cols="" rows="">{{ old('address') }}</textarea>
                                 </div>  
                               
                                 
                                 <div class="col-xs-12 col-md-12">
                                     <label for=""></label>
                                     <br>
                                     <input type="submit" class="btn btn-primary" value="Submit">
                                 </div>
                                 

                                  
                            </form>
                    <!-- end form for validations -->
                              


                            </div>
                        </div>
                    </div>



@endsection