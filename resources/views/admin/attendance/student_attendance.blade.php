@extends('layouts.app')
@section('title','Student List')
@section('content') 
   
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>All Student's List
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif




        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/attendances/students') }}" method="GET">
                     <div class="col-xs-12 col-md-2">
                         <label for="classes_id">Class<span style="color:red;">*</span></label>
                         <select name="classes_id" class="form-control" required="required">
                                    <option value="">Choose Class</option>
                                    @foreach($classes as $allclass)
                                       <option value="{{ $allclass->id }}" @if(isset($classes_search)) {{ $classes_search == $allclass->id ?'selected' : '' }} @endif>        
                                              {{$allclass->name }}
                                       </option>
                                    @endforeach
                                   
                        </select>
                     </div> 
                        <div class="col-xs-12 col-md-2">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="sessions_id" class="form-control" required="required">
                                        <option value="">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($sessions_search)) {{ $sessions_search == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                          <div class="col-xs-12 col-md-2">
                             <label for="groupes_id">Group </label>
                              <select name="groupes_id" class="form-control">
                                        <option value="">Choose Group</option>
                                        @foreach($groupes as $allgroups)
                                          <option value="{{ $allgroups->id }}"@if(isset($groupes_search)) {{ $groupes_search == $allgroups->id ?'selected' : '' }} @endif>
                                                {{ $allgroups->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>  
                          <div class="col-xs-12 col-md-2">
                             <label for="sections_id">Section </label>
                              <select name="sections_id" class="form-control" >
                                        <option value="">Choose Section</option>
                                        @foreach($sections as $section)
                                          <option value="{{ $section->id }}"@if(isset($sections_search)) {{ $sections_search == $section->id ?'selected' : '' }} @endif>
                                                {{ $section->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>    
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>
                    
                </div>
              
            </div>
        </div>

<hr>

             <div class="card">
              @php 
              $begin = new DateTime('2010-05-01');
              $end = new DateTime('2010-05-07');

              $interval = DateInterval::createFromDateString('1 day');
              $period = new DatePeriod($begin, $interval, $end);

              @endphp

                    <div class="card-body">
                        <table id="attendances-table" class="table table-bordered table-responsive" style="width: 100%;display: flex;overflow-x: auto;">
                            <thead>
                              <tr>
                                <th rowspan="2">Identifier</th>
                                <th rowspan="2" style="min-width: 150px">Name</th>
                                <th rowspan="2">RFID</th>
                                @foreach($period as $dt)
                                <th colspan="3" class="no-sort text-center">{{ $dt->format("d-m-Y") }}</th>
                                @endforeach
                                <th rowspan="2" style="border-right: 1px solid #ddd !important">Total Hour</th>
                              </tr>
                              <tr>
                                @foreach($period as $dt)
                                <th class="no-sort ">Entry</th>
                                <th class="no-sort ">Exit</th>
                                <th class="no-sort ">Hour</th>
                                @endforeach
                              </tr>
                              @if($get_data)
                              @foreach($get_data as $row)
                                <tr>
                                  <td>{{ $row->identifier }}</td>
                                  <td>{{ $row->name }}</td>
                                  <td>{{ $row->rfid }}</td>
                                  <td>{{ $row->updated_at }}</td>
                                  <td>{{ $row->updated_at }}</td>
                                  <td>{{ $row->updated_at }}</td>
                                  <td>{{ $row->total_fingerprints }}</td>
                                </tr>
                              @endforeach
                              @endif
                            </thead>
                            
                        </table>

                    </div>
                </div>


        </div>
    </div>
</div>

@endsection

