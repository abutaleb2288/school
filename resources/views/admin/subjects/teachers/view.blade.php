@extends('layouts.app')
@section('content') 
     
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>All Subject List</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                    <a href="{{ url('admin/subject/teacher/create') }}" class="btn btn-primary"> Add New</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                            @if(Session::get('success'))
                                   <p class="alert alert-success">{{ Session::get('success') }}</p>
                              @elseif(Session::get('error'))
                               <p class="alert alert-danger">{{ Session::get('error') }}</p>
                              @endif

                            <!----
                             <form>
                                 <div class="col-xs-12 col-md-3">
                                     <label for="email">Select Class :</label>
                                     <select name="class_id" class="form-control" required>
                                                <option value="">Choose Class</option>
                                               
                                    </select>
                                 </div> 
                                 
                                 <div class="col-xs-12 col-md-1">
                                     <label for=""></label>
                                     <input type="submit" class="btn btn-primary form-control" value="Search">
                                 </div>
                            </form>  -->
                    <!-- end form for validations -->
                                <hr>


                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Class</th>
                                            <th>Subject</th>
                                            <th>Teacher</th>
                                            <th>Session</th>
                                            <th>Sectoin</th>
                                            <th>Created Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @php
                                                   $i=1;
                                              @endphp
                                              @foreach($subjects as $value)

                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $value->class->name }}</td>
                                            <td>{{ $value->subject->name }}</td>
                                            <td>{{ $value->teachers->name }} </td>
                                            <td>{{ $value->session->name }} </td>
                                            <td>{{ $value->section->name }} </td>
                                            <td>{{ $value->created_at }} </td>
                                            <td>
                                                <a href="{{ url('admin/subjects/teachers/edit/'.$value->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>

                                                
                                              </td>
                                                 
                                           </tr>
                                         
                                        @endforeach
                                         
                                        


                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>

     
@endsection