@extends('layouts.app')
@section('title','Student List')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>  Subject Distribution For Teacher
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

             @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif



<!-- end form for validations -->



            <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            
                            <div class="x_content">
                                <br />

                                 @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif




                     <form class="form-horizontal form-label-left" method="post" action="{{ url('/admin/subjects/teachers/update',$subjectsDistribute->id) }}">
                          @csrf
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Class :</label>
                              <div class="col-md-7 col-sm-9 col-xs-12">
                                  <select name="classes_id" id="classes_id" class="classes_id form-control" required="required">
                                      <option value="">Choose Class</option>
                                      @foreach($classes as $class)
                                      <option {{ $subjectsDistribute->class_id == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->name }}</option>
                                      @endforeach 
                                  </select>
                              </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Subject :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="subject_id" id="subject_id" class="form-control" required="required">
                                    <option value="">Choose Subject</option> 
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}" {{ $subject->id == $subjectsDistribute->subject_id ? 'selected' : '' }}>{{ $subject->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Teacher :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="teacher_id" class="form-control" required="required">
                                    <option value="">Choose Teacher</option>
                                    @foreach($teachers as $value)
                                    <option  {{ $subjectsDistribute->teacher_id == $value->id ? "selected" : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div>     
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Session  :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="sessions_id" class="form-control" required="required">
                                    <option value="">Choose Session </option>
                                    @foreach($sessions as $session)
                                    <option  {{ $subjectsDistribute->session_id == $session->id ? "selected" : '' }} value="{{ $session->id }}">{{ $session->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div>     
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Section :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="sections_id" class="form-control" required="required">
                                    <option value="">Choose Section</option>
                                    @foreach($sections as $section)
                                    <option value="{{ $section->id }}" {{ $subjectsDistribute->section_id == $section->id ? "selected" : '' }} >{{ $section->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div>     
                         
                                
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-3">
                                <a href="{{ url('admin/subjects/teachers/view') }}" class="btn btn-danger">Back</a>
                                <button type="reset" class="btn btn-primary">Reset</button>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>


      <div id="getUrl" data-url="{{ url('admin/subjects/search/by/class/id')}}"></div>


        </div>
    </div>
</div>




@section('customjs')

  <script>

    $(document).ready(function(){
        $('.classes_id').on('change',function(){
           var classId =  $(this).val();
           var url = $('#getUrl').data("url");
           $.ajax({
              url: url,
              data: {classId:classId},
              type: "GET",
              success: function(response){
                $('#subject_id').html(response);
              },  
           });
        });
    });

  </script>
@endsection

@endsection
