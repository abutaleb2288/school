@extends('layouts.app')
@section('title','Student Subject View')
@section('content')

 <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Student Subject View</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content" id="print">
                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Information</th>
                                            <th>Item</th>
                                            <th>Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <th class="text-center" colspan="4">Basic Information</th>
                                         </tr>
                                        <tr>
                                            <th>Student ID</th>
                                            <td>{{ $student->student_sid }}</td>
                                            <th>Name</th>
                                            <td>{{ $student->name }}</td>
                                        </tr>
                                        <tr>
                                       <th>Class</th>
                                       <td>{{ $student->classs->name }}</td>
                                         <input type="hidden" name="classes_id" value="{{ $student->classes_id }}"> 
                                      <th>Sesssion</th>
                                      <td>{{ $student->sessions->name }}</td>
                                       <input type="hidden" name="sessiones_id" value="{{ $student->sessions_id }}"> 
                                    </tr> 

                                    <tr>
                                        <td colspan="4">Subject</td>
                                    </tr>
                                        
                                 <tr>
                                        <th>Serial</th>
                                        <th colspan="2">Subject Name</th>
                                        <th>Subject Code</th>
                                      
                                    </tr>
                                    @php
                                        $i =1;
                                    @endphp

                                    @foreach($student_info as $subject)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        
                                        <td colspan="2">{{ $subject->subject->name }}</td>
                                        <td>{{ $subject->subject->code }}</td>
                                        </tr>
                                    @endforeach
                                         
                                     </tbody>
                                </table>
                                    <a href="" class="btn btn-primary btn-xs" onclick="window.print();"><i class="fa fa-print"></i> Print</a>
                            </div>
                        </div>
                    </div>


 

@endsection