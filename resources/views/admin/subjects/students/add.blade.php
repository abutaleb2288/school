@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Add Student Subject
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        <form action="{{ url('/admin/subjects/students/store') }}" method="post">
          @csrf
            <table class="table table-bordered">
               <tbody>
                <tr>
                  <th colspan="4"><p class="text-center">Academic Information</p></th class="btn btn-primary">
                </tr>
                 <tr>
                   <th>Name</th>
                   <td>{{ $student->name }}</td>
                   <th>Student SID</th>
                   <td>{{ $student->student_sid }}</td>
                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                 </tr>
                 <tr>
                   <th>Class</th>
                   <td>{{ $student->classs->name }}</td>
                     <input type="hidden" name="classes_id" value="{{ $student->classes_id }}"> 
                  <th>Sesssion</th>
                  <td>{{ $student->sessions->name }}</td>
                   <input type="hidden" name="sessiones_id" value="{{ $student->sessions_id }}"> 
                 </tr>  
                <tr>
                   <th>Group</th>
                   <td>@if($student->groupes_id == null || $student->groupes_id == 0 )
                            <span style="color:red;">No Group</span>
                                    @else
                                    {{ $student->groups->name }}
                              <input type="hidden" name="groupes_id" value="{{ $student->groupes_id }}">
                                @endif
                    </td>
                  <th>Section</th>
                  <td>
                    @if($student->sections_id == null || $student->sections_id == 0 )
                          <span style="color:red;">No Section</span>
                          @else
                                {{ $student->sections->name }}

                         @endif
                  </td>
                 </tr>
                 <tr>
                   <th colspan="4"><p class="text-center">Choose Subject</p></th class="btn btn-primary">
                 </tr>
                 
                  <tr>   <td colspan="4">Compulsary Subject :</td>   </tr>
     <!--   ==========================================================================================play====eight================= -->
                  @if($student->classes_id >=1 && $student->classes_id <=10)
                        
                        <!--     class play to eight subject list -->

                  <tr> 
                    <td>Common Subject</td> 
                    <td colspan="3">

                        @php   $i=1;
                         $result_sub = DB::table('subjects')
                                        ->where('classes_id',$student->classes_id)
                                        ->get();

                          foreach($result_sub as $getsubvalue){  
                            @endphp
                            {{ $i++ }}
                            {{ $getsubvalue->name  }} <br>
                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                                             
                           @php    }  @endphp
                       </td>
                   </tr>

     <!--   ==================================================================================end========play====eight================= -->
   
                  
      <!--  ==========================================================   class nine to ten subject list============ -->

                @elseif($student->classes_id ==11 || $student->classes_id == 12)


                        @if($student->groupes_id==1)           <!-- =====================  if group science  =================== -->

                        <tr>
                    <td>Common Subject</td>
                    <td colspan="3">

                        @php     $i=1;
                         $result_sub = DB::table('subjects')
                                         ->where('classes_id',$student->classes_id)
                                         ->whereIn('id',[91,92,93,94,95,96])
                                         ->get();

                          foreach($result_sub as $getsubvalue){  
                            @endphp

                                {{ $i++ }}  .
          
                           {{ $getsubvalue->name  }} <br>

                      <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                                             
                   @php    }  @endphp
                    </td>
                 </tr> 


                 <tr>
                    <td>Group Subject</td>
                    <td colspan="3">
                    @php     $i=1;
                         $result_sub = DB::table('subjects')
                                         ->where('classes_id',$student->classes_id)
                                         ->whereIn('id',[97,98])
                                         ->get();

                          foreach($result_sub as $getsubvalue){  
                            @endphp

                                {{ $i++ }}  .
          
                           {{ $getsubvalue->name  }} <br>

                      <input type="hidden" name="compulary_subjectgroup[]" value="{{ $getsubvalue->id }}">
                                                             
                     @php   }  @endphp
                    </td>
                 </tr>  

                 <tr>
                    <td>3rd Subject</td>
                    <td colspan="3">

                    <select name="subjectgroup3rd[]" id="" class="form-control col-md-4">
                    @php     $i=1;
                         $result_sub = DB::table('subjects')
                                         ->where('classes_id',$student->classes_id)
                                         ->whereIn('id',[99,100])
                                         ->get();

                          foreach($result_sub as $getsubvalue1){  
                            @endphp


                               
                          

                          <option value="{{ $getsubvalue1->id }}">{{ $getsubvalue1->name }}</option>
                      
                                                             
                     @php   }  @endphp

                     </select>
                    </td>
                 </tr>



                   <tr>
                    <td>4th Subject</td>
                    <td colspan="3">

                    <select name="optional4thsubject[]" id="" class="form-control col-md-4">
                    @php     $i=1;
                         $result_sub = DB::table('subjects')
                                         ->where('classes_id',$student->classes_id)
                                         ->whereIn('id',[99,100,101])
                                         ->get();

                          foreach($result_sub as $getsubvalue2){  
                            @endphp


                               
                          

                          <option value="{{ $getsubvalue2->id }}">{{ $getsubvalue2->name }}</option>
                      
                                                             
                     @php   }  @endphp

                     </select>
                    </td>
                 </tr>


                   @elseif($student->groupes_id==2)              <!-- =======================  if group huminitice  ==========-->

                           <tr>
                    <td>Common Subject</td>
                    <td colspan="3">

                        @php     $i=1;
                         $result_sub = DB::table('subjects')
                                         ->whereIn('id',[91,92,93,94,95,96])
                                         ->get();

                          foreach($result_sub as $getsubvalue){  
                            @endphp

                                {{ $i++ }}  .
          
                           {{ $getsubvalue->name  }} <br>

                      <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                                             
                   @php    }  @endphp
                    </td>
                 </tr> 


                 <tr>
                    <td>Group Subject</td>
                    <td colspan="3">
                    @php     $i=1;
                         $result_sub = DB::table('subjects')
                                        
                                         ->whereIn('id',[102,103,104])
                                         ->get();

                          foreach($result_sub as $getsubvalueh){  
                            @endphp

                                {{ $i++ }}  .
          
                           {{ $getsubvalueh->name  }} <br>

                      <input type="hidden" name="compulary_subjectgroup[]" value="{{ $getsubvalueh->id }}">
                                                             
                     @php   }  @endphp
                    </td>
                 </tr>  
  
                 <tr>
                    <td>4th Subject</td>
                    <td colspan="3">

                    <select name="optional4thsubject[]" id="" class="form-control col-md-4">
                    @php     $i=1;
                         $result_sub = DB::table('subjects')
                                       
                                         ->whereIn('id',[105])
                                         ->get();

                          foreach($result_sub as $getsubvalueh1){  
                            @endphp
                     <option value="{{ $getsubvalueh1->id }}">{{ $getsubvalueh1->name }}</option>
                      
                                                             
                     @php   }  @endphp

                     </select>
                    </td>
                 </tr>
                    @endif  <!-- group endif -->
        
        <!--  ==================================================================== nine and ten close ====================== -->

                  @elseif($student->classes_id==13)         <!--  =eleven class subject list -->
                     
                      @if($student->groupes_id== 1)<!--         ===========      SCIENC GROUP SUBJECT  ========================= -->

                                    <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                       
                                       @php 

                                         $i=1;

                                            $result_sub = DB::table('subjects')
                                                           ->whereIn('id',['121','122','123'])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp



                                     </td>

                                    </tr>
                                    <tr>
                                        <td >Group Subject :</td>
                                        <td colspan="3">
                                          @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                               ->whereIn('id',['124','125'])
                                                               ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                                            {{ $getsubvalue->name  }} <br>

                                                    <input type="hidden" name="compulary_group_subject2[]" value="{{ $getsubvalue->id }}">
                                                           
                                            @php    }  @endphp     
                                             </td>

                                             </tr>



                                        <tr>
                                        <td>3rd Subject Choose</td>
                                        <td>
                                            <select name="compulary_group_subject3[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 

                                                 $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[126,127])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                      <option value="{{ $getsubvalue->id  }}">{{ $getsubvalue->name  }}</option>       
                                                           

                                              @php    }  @endphp

                                               </select>

                                              </td>
                                           
                                    </tr>    


                                 <tr>       
                                    <td> Choose Optional </td>

                                        <td>
                                            <select name="optional_subject[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 

                                           $result_sub = DB::table('subjects')
                                                          
                                                          ->whereIn('id',[126,127])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                            <option value="{{ $getsubvalue->id  }}">{{ $getsubvalue->name  }}</option>       
                                                           

                                              @php    }  @endphp

                                               </select>

                                              </td>
                                           
                                              </tr>
                                    


                                            <!-- ================= science close ============================== -->

                    @elseif($getgroup->id==2)   <!-- ====================== HUMINITIES SUBJECT===============  -->
                                            



                                  <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                       
                                       @php 

                                         $i=1;

                                            $result_sub = DB::table('subjects')
                                                           ->whereIn('id',['121','122','123'])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp



                                     </td>

                                    </tr>
                                    <tr>
                                        <td >Choose Any 3 Subject :</td>
                                        <td colspan="3">

                                     <select name="compulary_group_subject2[]" id="" multiple class="form-control">
    
                                        @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[128,129,130,131,132,133])
                                                           ->get();
                                          foreach($result_sub as $getsubvalue){    @endphp

                                       <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                        
                                          @php    }  @endphp    

                                              </select> 
                                             </td>

                                             </tr>

                                    <tr>       
                                    <td> Choose Optional </td>

                                        <td>
                                            <select name="optional_subject[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                              ->whereIn('id',[128,129,130,131,132,133])
                                                             ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

 
                                                              <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                                           


                                              @php    }  @endphp    

                                               </select>

                                              </td>
                                           
                                              </tr>



                                               @elseif($getgroup->id==3)         <!-- =================== COMMERCE SUBJECT  -->
                                            



                                          <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                         @php      $i=1;
                                           $result_sub = DB::table('subjects')
                                                           ->whereIn('id',['121','122','123'])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp

                                         </td>
                                    </tr>


                                    <tr>
                                        <td >Group Subject :</td>
                                        <td colspan="3">
                                          @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                                ->whereIn('id',[135,136,137])
                                                                ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp


                                                            
                                                                   {{ $getsubvalue->name  }} <br>

                                                                     <input type="hidden" name="compulary_group_subject2[]" value="{{ $getsubvalue->id }}">
                                                           


                                              @php    }  @endphp     
                                             </td>

                                             </tr>
 
                                           <tr>       
                                              <td> Choose Optional </td>

                                                  <td>
                                                      <select name="optional_subject[]" id="" class="form-control">
                                                          <option value="">Choose One</option>
                                                      

                                              @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                          
                                                          ->whereIn('id',[134,135])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp
                                           <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                                           
                                                      @php    }  @endphp    

                                               </select>

                                              </td>
                                           
                                           </tr>
                                       @endif




                     @elseif($student->classes_id==14)         <!--  =twelve class subject list ========================= -->

                     
                        @if($student->groupes_id== 1)      <!--   ===========     SCIENC GROUP SUBJECT  ========================= -->

                                    <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                       
                                       @php 

                                         $i=1;

                                            $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[140,141,142])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp



                                     </td>

                                    </tr>
                                    <tr>
                                        <td >Group Subject :</td>
                                        <td colspan="3">
                                          @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                               ->whereIn('id',[143,144])
                                                               ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                                            {{ $getsubvalue->name  }} <br>

                                                    <input type="hidden" name="compulary_group_subject2[]" value="{{ $getsubvalue->id }}">
                                                           
                                            @php    }  @endphp     
                                             </td>

                                             </tr>



                                        <tr>
                                        <td>3rd Subject Choose</td>
                                        <td>
                                            <select name="compulary_group_subject3[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 

                                                 $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[145,146])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                      <option value="{{ $getsubvalue->id  }}">{{ $getsubvalue->name  }}</option>       
                                                           

                                              @php    }  @endphp

                                               </select>

                                              </td>
                                           
                                    </tr>    


                                 <tr>       
                                    <td> Choose Optional </td>

                                        <td>
                                            <select name="optional_subject[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 

                                           $result_sub = DB::table('subjects')
                                                          
                                                          ->whereIn('id',[145,146])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

                                            <option value="{{ $getsubvalue->id  }}">{{ $getsubvalue->name  }}</option>       
                                                           

                                              @php    }  @endphp

                                               </select>

                                              </td>
                                           
                                              </tr>
                                    


                                            <!-- ================= science close ============================== -->

                                            @elseif($student->groupes_id==2)   <!-- ====================== HUMINITIES SUBJECT  -->
                                            



                                  <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                   <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                       
                                       @php 

                                         $i=1;

                                            $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[140,141,142])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp



                                     </td>

                                    </tr>


                                    <tr>
                                        <td >Choose Any 3 Subject :</td>
                                        <td colspan="3">

                                     <select name="compulary_group_subject2[]" id="" multiple class="form-control">
    
                                        @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[148,149,150,151,152,153])
                                                           ->get();
                                          foreach($result_sub as $getsubvalue){    @endphp

                                       <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                        
                                          @php    }  @endphp    

                                              </select> 
                                             </td>

                                             </tr>

                                    <tr>       
                                    <td> Choose Optional </td>

                                        <td>
                                            <select name="optional_subject[]" id="" class="form-control">
                                                <option value="">Choose One</option>
                                            

                                              @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                              ->whereIn('id',[148,149,150,151,152,153])
                                                             ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp

 
                                                              <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                                           


                                              @php    }  @endphp    

                                               </select>

                                              </td>
                                           
                                              </tr>



                                               @elseif($student->groupes_id==3)          <!-- =================== COMMERCE SUBJECT  -->
                                            



                                          <tr>
                                        <td colspan="4">Compulsary Subject</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1">Commmon Subject</td>
                                        <td colspan="3">   
                                         @php 

                                         $i=1;

                                             $result_sub = DB::table('subjects')
                                                           ->whereIn('id',[140,141,142])
                                                          ->get();

                                          foreach($result_sub as $getsubvalue){    @endphp
                                              {{ $i++ }}
                                              {{ $getsubvalue->name  }} <br>
                                          <input type="hidden" name="compulary_subject[]" value="{{ $getsubvalue->id }}">
                                         
                                        @php   }  @endphp

                                         </td>
                                    </tr>


                                    <tr>
                                        <td >Group Subject :</td>
                                        <td colspan="3">
                                          @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                                ->whereIn('id',[154,155,156])
                                                                ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp


                                                            
                                                                   {{ $getsubvalue->name  }} <br>

                                                                     <input type="hidden" name="compulary_group_subject2[]" value="{{ $getsubvalue->id }}">
                                                           


                                              @php    }  @endphp     
                                             </td>

                                             </tr>
 
                                           <tr>       
                                              <td> Choose Optional </td>

                                                  <td>
                                                      <select name="optional_subject[]" id="" class="form-control">
                                                          <option value="">Choose One</option>
                                                      

                                              @php 
                                                  $i==1;
                                                  $result_sub = DB::table('subjects')
                                                          
                                                          ->whereIn('id',[148,153])
                                                          ->get();

                                                          foreach($result_sub as $getsubvalue){    @endphp
                                           <option value="{{ $getsubvalue->id }}"> {{ $getsubvalue->name  }}  </option>
                                                           
                                                      @php    }  @endphp    

                                               </select>

                                              </td>
                                           
                                           </tr>
                                       @endif
                    
 

                    @endif  <!-- class end if -->





                     <tr>
                         <td>
                            <input type="submit" class="btn btn-primary" value="Add Subject">
                         </td>
                    </tr>
               </tbody>
            </table>

            </form>

        </div>
    </div>
</div>
@endsection
