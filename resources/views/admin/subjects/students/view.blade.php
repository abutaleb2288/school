@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>All Student Subject View
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        	 @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif




        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/subjects/students') }}" method="GET">
                     <div class="col-xs-12 col-md-2">
                         <label for="classes_id">Class<span style="color:red;">*</span></label>
                         <select name="classes_id" class="form-control" required="required">
                                    <option value="">Choose Class</option>
                                    @foreach($classes as $allclass)
                                       <option value="{{ $allclass->id }}" @if(isset($classes_search)) {{ $classes_search == $allclass->id ?'selected' : '' }} @endif>        
                                              {{$allclass->name }}
                                       </option>
                                    @endforeach
                                   
                        </select>
                     </div> 
                        <div class="col-xs-12 col-md-2">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="sessions_id" class="form-control" required="required">
                                        <option value="">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($sessions_search)) {{ $sessions_search == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                          <div class="col-xs-12 col-md-2">
                             <label for="groupes_id">Group </label>
                              <select name="groupes_id" class="form-control">
                                        <option value="">Choose Group</option>
                                        @foreach($groupes as $allgroups)
                                          <option value="{{ $allgroups->id }}"@if(isset($groupes_search)) {{ $groupes_search == $allgroups->id ?'selected' : '' }} @endif>
                                                {{ $allgroups->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>  
                          <div class="col-xs-12 col-md-2">
                             <label for="sections_id">Section </label>
                              <select name="sections_id" class="form-control" >
                                        <option value="">Choose Section</option>
                                        @foreach($sections as $section)
                                          <option value="{{ $section->id }}"@if(isset($sections_search)) {{ $sections_search == $section->id ?'selected' : '' }} @endif>
                                                {{ $section->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>    
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>
                    
                </div>
               
            </div>
        </div>
    
<!-- end form for validations -->

<hr>

            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Student Id</th>
                        
                        <th>Name</th>
                        <th>Class</th>
                        <th>Group</th>
                        <th>Session</th>
                       
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	 $i=1;
                   $j=1;
                	@endphp
                 
                    @foreach($student as $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->student_sid }}</td>
                            
                            <td>{{ $value->name }}</td>
                            <td>
                                @if($value->classes_id == null || $value->classes_id == 0 )
                                    <span style="color:red;">No Class</span>
                                    @else
                                    {{ $value->classs->name }}
                                @endif
                            </td>
                            <td>
                                 @if($value->groupes_id == null || $value->groupes_id == 0 )
                                    <span style="color:red;">No Group</span>
                                    @else
                                    {{ $value->groups->name }}
                                @endif
                            </td>
                            <td>
                                 @if($value->sections_id == null || $value->sections_id == 0 )
                                    <span style="color:red;">No Session</span>
                                    @else
                                    {{ $value->sessions->name }}
                                @endif
                            </td>
                            <td>

                  @php 
                   $result = DB::table('student_subjects')
                              ->where('session_id',$value->sessions_id)
                              ->where('classes_id',$value->classes_id)
                              ->where('student_id',$value->id)
                              ->count();

               @endphp


                            @if($result>0)
<a href="{{ url('/admin/subjects/student/show/'.$value->id) }}" class="btn btn-primary btn-xs"> view</a>
                            
                            @else                  
 <a href="{{ url('/admin/subjects/students/add/'.$value->id) }}" class="btn btn-info btn-xs">Add Subject</a>
                           @endif


                              </td>
                             
                        </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>
</div>
@endsection
