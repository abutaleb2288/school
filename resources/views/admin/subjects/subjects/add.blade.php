@extends('layouts.app')
@section('content') 
      <div class="col-md-6 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Add New Subject </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />

                                 @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                               <form class="form-horizontal form-label-left" method="post" action="{{ url('/admin/subjects/store') }}">
                                  @csrf
                                   
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Select Classes :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <select name="classname" class="form-control">
                                                <option value="">Choose Class</option>
                                                @foreach($classes as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                @endforeach
                                             
                                            </select>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Name :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Subject Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Code :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="code" value="{{ old('code') }}" class="form-control" placeholder="Subject Code">
                                        </div>
                                    </div>   
                                 <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject category :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <select name="category" id="" class="form-control">
                                                <option value="1">Only Written</option>
                                                <option value="2">Written & MCQ</option>
                                                <option value="3">Written & MCQ & Practical</option>
                                            </select>
                                        </div>
                                    </div>
                                    
    






                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="full_mark" value="{{ old('full_mark') }}" class="form-control" placeholder="full Mark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="passmark" value="{{ old('passmark') }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div>   

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Written Full Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="wfm" value="{{ old('wfm') }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Written Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="wpm" value="{{ old('wpm') }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">MCQ Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="mcqfm" value="{{ old('mcqfm') }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">MCQ Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="mcqpm" value="{{ old('mcqpm') }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div> 

                                     <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Practical Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="pfm" value="{{ old('pfm') }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Practical Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="ppm" value="{{ old('ppm') }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Type :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12" >
                                             <select name="subject_type" id="" class="form-control">
                                                 <option value="1">Compulsary</option>
                                                 <option value="0">Optional</option>
                                             </select>
                                        </div>
                                    </div>


                                   

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button type="button" class="btn btn-primary">Cancel</button>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

             
@endsection