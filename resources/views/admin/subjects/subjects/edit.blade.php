@extends('layouts.app')
@section('content') 
      <div class="col-md-6 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Edit Subject </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />

                                 @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                               <form class="form-horizontal form-label-left" method="post" action="{{ url('/admin/subjects/update',$subject->id) }}">
                                  @csrf
                                   
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Select Classes :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <select name="classname" class="form-control">
                                                <option value="">Choose Class</option>
                                                @foreach($classes as $value)
                                                <option value="{{ $value->id }}" {{ $subject->classes_id == $value->id ? 'selected' : '' }}>
                                                  {{ $value->name }}</option>
                                                @endforeach
                                             
                                            </select>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Name :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="text" name="name" value="{{ $subject->name }}" class="form-control" placeholder="Subject Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Code :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="code" value="{{ $subject->code }}" class="form-control" placeholder="Subject Code">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="full_mark" value="{{ $subject->full_mark }}" class="form-control" placeholder="full Mark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="passmark" value="{{ $subject->passmark }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div>   

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Written Full Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="wfm" value="{{ $subject->wfm }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Written Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="wpm" value="{{ $subject->wpm }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">MCQ Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="mcqfm" value="{{ $subject->mcqfm }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">MCQ Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="mcqpm" value="{{ $subject->mcqpm }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div> 

                                     <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Practical Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="pfm" value="{{ $subject->pfm }}" class="form-control" placeholder="Subject written Mark">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Practical Pass Mark :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12">
                                            <input type="number" name="ppm" value="{{ $subject->ppm }}" class="form-control" placeholder="Subject Pass Mark">
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12">Subject Type :</label>
                                        <div class="col-md-7 col-sm-9 col-xs-12" >
                                             <select name="subject_type" id="" class="form-control">
                                                @if($subject->subject_type==1)
                                                 <option value="1">Compulsary</option>
                                                 <option value="0">Optional</option>
                                                @else
                                                 <option value="0">Optional</option>
                                                <option value="1">Compulsary</option>
                                                @endif
                                             </select>
                                        </div>
                                    </div>


                                   

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button type="button" class="btn btn-primary">Cancel</button>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

             
@endsection