
	@extends('layouts.app')
	@section('title','Classes')
	@section('content') 
	<div class="row">
	<div class="col-sm-12">
	<h4 class="pull-left page-title">All Salay 
	@if(isset($monthYearSearch))
	    <span style="margin-left: 2%;margin-right:1%;color:blue;">Of The Month Of : <strong style="color:brown;">{{ $monthDataFull }} - {{ $yearData }}</strong> &nbsp;</span>
	@endif
	<ol class="breadcrumb pull-right">
	<li><a href="#"> Total Amount @isset( $totalAmout) {{ $totalAmout }} @endisset</a></li>
	</ol>
	</h4>
	</div>
	</div>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">
	    <h3 class="panel-title">
	            <form action="{{ url('admin/salary/index') }}" method="GET" class="form-inline pull-left">
	                    <div class="form-group">
	                        <label for="ex3">Year</label>
	                        <select name="year" id="year" class="form-control year">
	                            <option value="0"> Select One</option>
	                            @php
	                            $year = date('Y');
	                            $year2 = 2017;
	                            @endphp
	                            @for ($i = 1; $year2 <= $year; $year--)
	                                @if(!isset($startDate))
	                                    <option value="{{ $year }}"
	                                        @if (isset($yearData)) {{ $yearData == $year ? 'selected' : '' }} 
	                                            @else {{ date('Y') == $year ? 'selected' : '' }} 
	                                        @endif> {{ $year }}
	                                    </option>
	                                    @else
	                                        <option value="{{ $year }}"> {{ $year }}
	                                        </option>
	                                @endif
	                            @endfor
	                        </select>
	                    </div>
	                    <div class="form-group">
	                        <label for="">Month</label>
	                        <select name="month" id="month" class="form-control month">
	                            <option value="0">Select One</option>
	                            @php
	                            $month = 12;
	                            @endphp
	                            @for ($i = 1; $i<=$month; $month--)
	                                @if(!isset($startDate))
	                                    <option value="{{ $month }}"
	                                    @if (isset($monthData)) {{ $monthData == $month ? 'selected' : '' }}
	                                        @else {{ date('m') == $month ? 'selected' : '' }} 
	                                    @endif> {{ $month }} </option>
	                                    @else
	                                        <option value="{{ $month }}"> {{ $month }}
	                                        </option>
	                                @endif
	                            @endfor
	                        </select>
	                    </div>
	                    <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> Search</button>
	                </form>

	              <span style="margin-left:5%;">Created At</span>



	        <form action="{{ url('admin/salary/index') }}" method="GET" class="form-inline pull-right" style="color:red;margin-bottom:0%;">
	                <input type="text" name="startDate" value="@if(isset($monthYearSearch)) @else {{ isset($startDate) ? $startDate : date('Y-m-d') }} @endif">
	                <input type="text" name="endDate" value="@if(isset($monthYearSearch))  @else {{ isset($EndDate) ? $EndDate : date('Y-m-d')  }} @endif">
	                <input type="submit" name="submit" id="" value="Search">
	        </form>
	    </h3>
	</div>
	<div class="panel-body">
	    <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <table id="datatable" class="table table-striped table-bordered">
	                <thead>
	                    <tr>
	                        <th>Teacher Name</th>
	                        <th>Amount</th>
	                        <th>Created At</th>
	                        <th>Month - Year</th>
	                        <th>Action</th>
	                    </tr>
	                </thead>
	                <tbody style="font-size:12px;color:black;">
	                    @foreach ($salary as $item)
	                        <tr>
	                            <td>{{ $item->teacher->name }}</td>                                            
	                            <td>{{ $item->amount }}</td>                                            
	                            <td>{{ date('d-m-Y  -  h:i:s a',strtotime($item->created_at)) }}</td>                                            
	                            <td>
	                                    {{ date('F-Y',strtotime($item->date)) }}
	                            </td>
	                            <td>
	                                <a href="" class="btn btn-xs btn-primary"> <i class="fa fa-edit"></i></a>
	                                <a href="" class="btn btn-xs btn-danger" id="delete"> <i class="fa fa-trash"></i></a>
	                                <a href="" class="btn btn-xs btn-info"> <i class="fa fa-eye"></i></a>
	                            </td>
	                        </tr>
	                    @endforeach
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
	</div>
	@if(isset($monthYearSearch) || isset($startDate))
	        <a href="{{ url('admin/salary/index') }}" style="margin-top:0%;" class="btn btn-danger pull-right"> Back </a> 
	@endif  
	</div>

	</div> <!-- End Row -->

	<!---------->
	@endsection