
	@extends('layouts.app')
	@section('title','Classes')
	@section('content') 


	        <!---------->
	          <!-- Form-validation -->
    <div class="row" style="margin-left:2%;">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Add  Salary  
                        @if (isset($monthData))<strong style="color:blue;margin-left:1%;"> Salary Of The Month Of 
                                 <strong style="color:brown;"> {{  $monthDataFull  }} - {{ $yearData }} </strong>
                            </strong>
                            @else
                            <strong style="color:blue;margin-left:1%;"> Pending Salary Of The Month Of 
                                <strong style="color:brown;">    {{  date('F')  }} - {{ date('Y') }}  </strong>
                            </strong>  
                        @endif
                     <!-------------------->
                        <form action="" method="GET" class="form-inline pull-right">
                                <div class="form-group">
                                    <label for="ex3">Year</label>
                                    <select name="year" id="year" class="form-control year">
                                        <option value="0"> Select One</option>
                                        @php
                                        $year = date('Y');
                                        $year2 = 2017;
                                        @endphp
                                        @for ($i = 1; $year2 <= $year; $year--)
                                        <option value="{{ $year }}" @if (isset($yearData)) {{ $yearData == $year ? 'selected' : '' }} 
                                        @else {{ date('Y') == $year ? 'selected' : '' }}  @endif> {{ $year }} </option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Month</label>
                                    <select name="month" id="month" class="form-control month">
                                        <option value="0">Select One</option>
                                        @php
                                        $month = 12;
                                        @endphp
                                        @for ($i = 1; $i<=$month; $month--)
                                        <option value="{{ $month }}" @if (isset($monthData)) {{ $monthData == $month ? 'selected' : '' }}
                                            @else {{ date('m') == $month ? 'selected' : '' }}  @endif> {{ $month }} </option>
                                        @endfor
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> Search</button>
                            </form>
                            <!--------------------->                       
                	</h3>
                </div>
                <div class="panel-body">            
                    <div class=" form">                                                                         <!--id="signupForm" novalidate="novalidate"--->          
                       <!-- {{-- --
                            <form method="POST" action="{{ route('admin.salary.store') }}"  class="cmxform form-horizontal tasi-form" >
                            @csrf
                            ----}} -->
                            <!-------------------------------------------->
                           
                            <table class="table table-striped projects">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Teacher Name</th>
                                        <th >Designation</th>
                                        <th>Salary</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                               
                                @php $sl = 1; @endphp
                                @foreach ($employee as $item)
                                <tbody id="tbody">
                                    <form method="POST" action="{{ url('admin/salary/store') }}"  class="cmxform form-horizontal tasi-form" >
                                        @csrf
                                        <tr>
                                            <td>{{ $sl ++ }}</td>
                                            	<!---{{-- --
                                                <td>
                                                    <input type="checkbox" name="status[]" value="{{  $item->user->id }}"  checked  />
                                                </td>
                                                --}}-->
                                            <td>
                                                 {{ $item->name }}
                                            </td>
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <input type="hidden" name="teacher_id" value="{{ $item->id }}"> 

                                            <input type="hidden" name="amount" value="{{ $item->salary }}">
                                            <td>
                                                {{ $item->designation }}
                                            </td>
                                            <td id="fixed_salary_{{ $item->id  }}">
                                                {{ $item->salary }} Tk 
                                            </td>

                                            
                                            <td >
                                                <input id="yearmonth" name="yearmonth" class="yearmonth" type="hidden" value="@if (Session::get('yearmonth')){{ Session::get('yearmonth')}} @else {{ date('Y-m') }} @endif">
                                                <input class="btn btn-success btn-xs" type="submit" value="Pay Now"/>
                                            </td>
                                        </tr>
                                    </form>
                                </tbody>
                                @endforeach
                                @isset($employee)  @endisset
                            </table>
                            <!-------------------------------------------->
                            <!-- --                                               
                             <div class="form-group" style="margin-right:0%;">
                                <div class="col-lg-offset-10 col-lg-10">
                                    <a  class="btn btn-default waves-effect" href="">Cancel</a>
                                    <input class="btn btn-success " type="submit" value="Submit"/>
                                </div>
                                </div>
                                </form>
                         -->
                    </div> <!-- .form -->

                </div> <!-- panel-body -->
            </div> <!-- panel -->
        </div> <!-- col -->
    </div> <!-- End row -->

        <!---------->



	@section('customjs')   

	<script>
	$(document).ready(function () {

	$(".sal").click(function () {
	var id = parseInt($(this).attr("id").substr(4));

	var fix_salary = parseFloat($("#fixed_salary_" + id).text());
	var advance = parseFloat($("#advance_" + id).text());
	var payable = fix_salary - advance;
	if(isNaN(advance)){
	$("#salary_" + id).text(fix_salary  + ' Tk.')
	$("#salary_value_" + id).val(fix_salary)
	$("#total_" + id).text(fix_salary);
	}else{
	$("#salary_" + id).text(payable + ' TK.') 
	$("#salary_value_" + id).val(payable) 
	$("#total_" + id).text(payable);
	}
	});
	//--------------------------------------------
	$("body").on("change", ".slaryStatement", function () {
	//$('.slaryStatement').change(function () {
	var id = parseInt($(this).attr("id").substr(4));

	var salary = parseFloat($("#salary_" + id).text());

	var bonus = parseFloat($("#bon_" + id).val());

	var penalty = parseFloat($("#pen_" + id).val());

	var total_salary = salary + bonus - penalty;

	$("#total_" + id).text(total_salary);

	});
	});

	</script>
	
	@endsection
	@endsection