@extends('layouts.app')
@section('content')

 <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="">
                                <h2>Student Details View</h2>
                                
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <th>Expense Purpose</th>
                                             <th>{{ $totalExpense->expense_purpose_id != 0 ? $totalExpense->expense_purpose->name : "No purpose" }}</th>
                                         </tr>
                                        
                                        <tr>
                                            <th>Title</th>
                                            <th>{{ $totalExpense->title }}</th>
                                        </tr>

                                        <tr>
                                            <th>Expense Date</th>
                                            <th>{{ $totalExpense->date }}</th>
                                        </tr>

 
                                        <tr>
                                            <th>Amount</th>
                                            <th>{{ $totalExpense->amount }}</th>
                                        </tr>

 
 
                                        <tr>
                                            <th>Description</th>
                                            <th>{{ $totalExpense->description }}</th>
                                        </tr>

 
                                        <tr>
                                            <th>Cteated By</th>
                                            <th>{{ $totalExpense->user->name }}</th>
                                        </tr>
                                            <tr>
                                            <th>Cteated Date</th>
                                            <th>{{ $totalExpense->created_at }}</th>
                                        </tr>

 
                                         <tr>
                                            <th>Status</th>
                                             <td colspan="3" >
                                                @if($totalExpense->status==1)
                                                <p class="btn btn-primary btn-xs">Active</p>
                                                @elseif($totalExpense->status==0)
                                                <p class="btn btn-danger btn-xs">Pending</p>
                                                @endif
                                             </td>
                                             

                                        </tr>
                                         
                                        
                                        <tr>
                                           <th>Action</th>
                                            <td colspan="3">
                                               <a href="{{ url('/admin/daily/expense/delete/'.$totalExpense->id) }}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                               <a href="{{ url('/admin/daily/expense/edit/'.$totalExpense->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> 

                                               <a href="{{ url('/admin/daily/expense/index') }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Back</a>
                                            </td>
                                            
                                         </tr>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>


@endsection