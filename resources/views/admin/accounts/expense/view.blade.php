@extends('layouts.app')
@section('title','Classes')
@section('content') 

<!---------->

		<div class="row">
		
		</div>


		<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title">Datatable</h3>
		</div>
		<div class="panel-body">
		<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		<table id="datatable" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>Expense Purpose</th>
			<th style="width:4%;text-align:center;"><small>Amount</small></th>
			<th>Title</th>
			<th>Created </th>
			<th>Status</th>
			<th style="width:15%;text-align:center;">Action</th>
		</tr>
		</thead>
		<tbody style="font-size:12px;color:black;">
			@foreach ($expenses as $item)
				<tr>
					<td>{{ $item->expense_purpose_id == null ? 'No' : str_limit($item->expense_purpose->name, 20) }}</td>                                            
					<td style="text-align:center;">{{ $item->amount }} Tk.</td>                                            
					<td>{{ str_limit($item->title, 20) }}</td>                                            
					<td>{{ date('d-m-Y  -  h:i:s',strtotime($item->created_at)) }}</td>                                            
					<td>
					{{ $item->status == 1?'Verified':'No Verified' }}
					</td>
					<td>
					<a href="{{ url('admin/daily/expense/edit',$item->id) }}" class="btn btn-xs btn-primary"> <i class="fa fa-edit"></i></a>
					<a href="{{ url('admin/daily/expense/delete',$item->id) }}" class="btn btn-xs btn-danger" id="delete"> <i class="fa fa-trash"></i></a>
					<a href="{{ url('admin/daily/expense/show',$item->id) }}" class="btn btn-xs btn-info"> <i class="fa fa-eye"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
		</table>

		</div>
		</div>
		</div>
		</div>
		</div>

		</div> <!-- End Row -->

<!---------->
@endsection
              