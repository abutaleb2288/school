
@extends('layouts.app')
@section('title','Classes')
@section('content') 


<!---------->
<!-- Form-validation -->
	<div class="row">

		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Add Daily Expense 
						<span class="pull-right">Total Today's Expense : <strong style="color:blue;"> @if(isset($todaysExpense)){{  $todaysExpense }} @endif</strong> Tk.</span>
					</h3>
				</div>
				<div class="panel-body">            
					<div class=" form">                                                                  <!--id="signupForm" novalidate="novalidate"--->          
						<form method="POST" action="{{ url('admin/daily/expense/store') }}"  class="cmxform form-horizontal tasi-form" novalidate="novalidate">
						@csrf
						<!---------------------------------------------------------------------------->   
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="expense_purpose_id">Expense Purpose <span
							class="required"> *</span>
							</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<select  style="font-size:11px;" id="expense_purpose_id" name="expense_purpose_id" class="form-control col-md-7 col-xs-12">
								@foreach ($expensePurposes as $expensePurpose)
								<option value="{{ $expensePurpose->id }}" style="font-size:12px;font-weight:700;"> {{ $expensePurpose->name }}</option>
								@endforeach
								</select>

								@if ($errors->has('expense_purpose_id'))
								<span class="invalid-feedback" role="alert">
								<strong class="red">{{ $errors->first('expense_purpose_id') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
							</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input style="font-size:11px;" value="{{ old('title') }}" type="text" id="title" name="title"  class="form-control col-md-7 col-xs-12">

								@if ($errors->has('title'))
								<span class="invalid-feedback" role="alert">
								<strong class="red">{{ $errors->first('title') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Expense Date <span class="required">*</span>
							</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input style="font-size:11px;" value="{{ old('date') }}" type="date" id="date" name="date"  class="form-control col-md-7 col-xs-12">

								@if ($errors->has('title'))
								<span class="invalid-feedback" role="alert">
								<strong class="red">{{ $errors->first('title') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
							</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input style="font-size:11px;" value="{{ old('amount') }}" type="text" id="amount" name="amount" required="required" class="form-control col-md-7 col-xs-12 amount">

								@if ($errors->has('amount'))
								<span class="invalid-feedback" role="alert">
								<strong class="red">{{ $errors->first('amount') }}</strong>
								</span>
								@endif

								<span class="text-danger" role="alert">
								<strong id="err_amount"></strong>
								</span>
							</div>
						</div>
						<div class="form-group ">
							<label for="description" class="control-label col-lg-3">Description <small>(optional)</small></label>
							<div class="col-lg-8">
								<input value="{{ old('description') }}"  class=" form-control" id="description" name="description" type="text" >

								@if ($errors->has('description'))
								<span class="invalid-feedback" role="alert">
								<strong class="red">{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
						</div>
						

						<div class="form-group">
							<div class="col-lg-offset-3 col-lg-10">
								<input class="btn btn-success " type="submit" value="Submit"/>
								<a  class="btn btn-default waves-effect" href="{{ url('admin/daily/expense/index') }}">Cancel</a>
							</div>
						</div>
						</form>
					</div> <!-- .form -->

				</div> <!-- panel-body -->
			</div> <!-- panel -->
		</div> <!-- col -->
		<div class="col-sm-1"></div>
	</div> <!-- End row -->






@endsection
              