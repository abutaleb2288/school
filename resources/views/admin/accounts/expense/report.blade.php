@extends('layouts.app')
@section('title','Classes')
@section('content') 

<!---------->

	<div class="row">
                <div class="col-md-12">
                   <form action="{{ url('admin/expense/report') }}" method="GET">
                     <div class="col-xs-12 col-md-4">
                         <label for="classes_id">Start Date / From<span style="color:red;">*</span></label>
                        <input value="{{ isset($startDate) ? $startDate : '' }}" name="startDate" type="date" class="form-control" required="required">
                     </div> 
                        <div class="col-xs-12 col-md-4">
                             <label for="sessions_id">End Date / To<span style="color:red;">*</span></label>
                                <input  value="{{ isset($endDate) ? $endDate : '' }}" name="endDate" type="date" class="form-control" required="required">
                         </div>       
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>
                </div>
            </div>

	


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Datatable  <strong class="pull-right" style="margin-right:5%;color:brown;">Total Amount : <span style="color:red; font-weight: 700;">{{ $total_amount }}</span> Tk</strong></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Expense Purpose</th>
									<th style="width:4%;text-align:center;"><small>Amount</small></th>
									<th>Title</th>
									<th>Expense Date</th>
									<th>Created </th>
									<th>Status</th>
									<th style="width:15%;text-align:center;">Action</th>
								</tr>
							</thead>
							<tbody style="font-size:12px;color:black;">
								@foreach ($expenses as $item)
									<tr>
										<td>{{ $item->expense_purpose_id == null ? 'No' : str_limit($item->expense_purpose->name, 20) }}</td>                                            
										<td style="text-align:center;">{{ $item->amount }} Tk.</td>                                            
										<td>{{ str_limit($item->title, 20) }}</td>                                            
										<td>{{ date('d-m-Y  -  h:i:s',strtotime($item->date)) }}
										</td>                                                    
										<td>{{ date('d-m-Y  -  h:i:s',strtotime($item->created_at)) }}
										</td>                                            
										<td>
										{{ $item->status == 1?'Verified':'No Verified' }}
										</td>
										<td>
										<a href="{{ url('admin/daily/expense/edit',$item->id) }}" class="btn btn-xs btn-primary"> <i class="fa fa-edit"></i></a>
										<a href="{{ url('admin/daily/expense/delete',$item->id) }}" class="btn btn-xs btn-danger" id="delete"> <i class="fa fa-trash"></i></a>
										<a href="{{ url('admin/daily/expense/show',$item->id) }}" class="btn btn-xs btn-info"> <i class="fa fa-eye"></i></a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div> <!-- End Row -->

<!---------->
@endsection
              