@extends('layouts.app')
@section('title','Expense Purpose')
@section('content')


                    <div class="col-md-6 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Add New Subject </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content"> 


                               
                            </div>
                        </div>
                    </div>






























<!-- Page-Title -->
<div class="row">
<div class="col-sm-12">
<h4 class="pull-left page-title">Add Expense Purpose</h4>
<ol class="breadcrumb pull-right">
    <li><a href="#">Moltran</a></li>
    <li class="active">Dashboard</li>
</ol>
</div>
</div>

<!---------->
<!-- Form-validation -->
<div class="row">

<div class="col-sm-1"></div>
<div class="col-sm-10">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title">Add Expense Purpose</h3></div>
<div class="panel-body">            
    <div class=" form">                                                                         <!--id="signupForm" novalidate="novalidate"--->          
        <form method="POST" action="{{ url('admin/expense/purpose/store') }}"  class="cmxform form-horizontal tasi-form" novalidate="novalidate">
            @csrf
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-3">Expense Purpose *</label>
                <div class="col-lg-8">
                    <input value="{{ old('name') }}"  class=" form-control" id="firstname" name="name" type="text" required>
                
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="red">{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group ">
                <label for="description" class="control-label col-lg-3">Description <small>(optional)</small></label>
                <div class="col-lg-8">
                    <input value="{{ old('description') }}"  class=" form-control" id="description" name="description" type="text" >
                
                    @if ($errors->has('description'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="red">{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
           

            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-10">
                    <input class="btn btn-success " type="submit" value="Submit"/>
                    <a  class="btn btn-default waves-effect" href="">Cancel</a>
                </div>
            </div>
        </form>
    </div> <!-- .form -->

</div> <!-- panel-body -->
</div> <!-- panel -->
</div> <!-- col -->
<div class="col-sm-1"></div>
</div> <!-- End row -->

@endsection