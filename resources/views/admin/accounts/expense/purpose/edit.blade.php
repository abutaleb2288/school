@extends('layouts.app')
@section('title','Classes')
@section('content') 
<div class="row">

<div class="col-sm-1"></div>
<div class="col-sm-10">
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title">Update Expense Purpose</h3></div>
    <div class="panel-body">            
        <div class=" form">                                                                         <!--id="signupForm" novalidate="novalidate"--->          
            <form method="POST" action="{{ url('admin/expense/purpose/update',$expensePurpose->id) }}"  class="cmxform form-horizontal tasi-form" novalidate="novalidate">
                @csrf
             
                <div class="form-group ">
                    <label for="firstname" class="control-label col-lg-3">Name *</label>
                    <div class="col-lg-8">
                        <input value="{{ $expensePurpose->name ?? old('name') }}"  class=" form-control" id="firstname" name="name" type="text" required>
                    
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong class="red">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group ">
                    <label for="description" class="control-label col-lg-3">Description <small>(optional)</small></label>
                    <div class="col-lg-8">
                        <input value="{{ $expensePurpose->description ??  old('description') }}"  class=" form-control" id="description" name="description" type="text" >
                    
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong class="red">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
               
                

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-10">
                        <input class="btn btn-success " type="submit" value="Updated"/>
                        <a  class="btn btn-default waves-effect" href="">Cancel</a>
                    </div>
                </div>
            </form>
        </div> <!-- .form -->

    </div> <!-- panel-body -->
</div> <!-- panel -->
</div> <!-- col -->
<div class="col-sm-1"></div>
</div> <!-- End row -->

@endsection