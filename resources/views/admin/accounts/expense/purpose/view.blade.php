@extends('layouts.app')
@section('title','Classes')
@section('content') 

<div class="row">
<div class="col-sm-12">
<h4 class="pull-left page-title">All Expense Purpose</h4>
<ol class="breadcrumb pull-right">
<li><a href="#">Moltran</a></li>
<li><a href="#">Tables</a></li>
<li class="active">Data Table</li>
</ol>
</div>
</div>


<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">Datatable</h3>
</div>
<div class="panel-body">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Created_at</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody style="font-size:12px;color:black;">
                @foreach ($expensePurpose as $item)
                    <tr>
                        <td>{{ $item->name }}</td>                                            
                        <td>{{ $item->description }}</td>                                            
                        <td>{{ date('d-m-Y  -  h:i:s',strtotime($item->created_at)) }}</td>                                            
                       
                        <td>
                            <a href="{{ url('admin/expense/purpose/edit',$item->id) }}" class="btn btn-xs btn-primary"> <i class="fa fa-edit"></i></a>
                            <a href="" class="btn btn-xs btn-danger" id="delete"> <i class="fa fa-trash"></i></a>
                            <a href="" class="btn btn-xs btn-info"> <i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
</div>
</div>
</div>

</div> <!-- End Row -->

<!---------->

@endsection