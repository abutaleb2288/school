@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Add Mark
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 <a href="{{ url('admin/markentry/view/mark') }}" class="btn btn-info"> View Mark</a>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        	 @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif


<!--djfkldjlfjkl-->

        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/markentry') }}" method="GET">
                     <div class="col-xs-12 col-md-2">
                         <label for="classes_id">Class<span style="color:red;">*</span></label>
                         <select name="classes_id" class="classes_id form-control" required="required">
                                    <option value="">Choose Class</option>
                                    @foreach($classes as $allclass)
                                       <option value="{{ $allclass->id }}" @if(isset($classes_id)) {{ $classes_id == $allclass->id ?'selected' : '' }} @endif>        
                                              {{$allclass->name }}
                                       </option>
                                    @endforeach
                                   
                        </select>
                     </div> 
                        <div class="col-xs-12 col-md-2">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="sessions_id" class="form-control" required="required">
                                        <option value="">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($sessions_id)) {{ $sessions_id == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                          <div class="col-xs-12 col-md-2">
                             <label for="groupes_id">Exam Term </label>
                              <select name="exam_id" class="form-control" required="required">
                                        <option value="">Choose Exam Term</option>
                                        @foreach($examterm as $allgroups)
                                          <option value="{{ $allgroups->id }}"@if(isset($exam_id)) {{ $exam_id == $allgroups->id ?'selected' : '' }} @endif>
                                                {{ $allgroups->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>  
                          <div class="col-xs-12 col-md-2">
                             <label for="sections_id">Subject </label>
                            <select name="subject_id" id="subject_id" class="form-control" >
                                  <option value="">Class Choose First</option>   
                            </select>
                         </div>    
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>

                </div>
                 
            </div>
        </div>
    
<!-- end form for validations -->

<hr>

            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Student Id</th>
                        
                        <th>Name</th>
                        <th>Class</th>
                        <th>Session</th>
                        <th  class="col">Mark Entry</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	 $i=1;
                   $j=1;
                	@endphp
              <form action="{{ url('admin/markentry/store') }}" method="POST">
                @csrf
                  @foreach($allStudent as $student)
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td>{{ $student->studentTable->student_sid }}</td>
                      <input  name="student_id_{{ $student->studentTable->id }}" type="hidden" value="{{ $student->studentTable->id }}">
                
       <input  name="id[]" type="hidden" value="{{ $student->studentTable->id  }}">

                    <td>{{ $student->studentTable->name }} </td>

                    <td>{{ $student->classs->name }}</td>
                     <input  name="classes_id" type="hidden" value="{{ $student->classs->id  }}">

                     <input  name="exam_id" type="hidden" value="{{ $exam_id }}">
                     <input  class="subjectId" name="subject_id" type="hidden" value="{{ $subject_id  }}">

                    <td>{{ $student->sessions->name }}</td>
                     <input  name="sessions_id" type="hidden" value="{{ $student->sessions->id }}">

                      <input type="hidden" name="category" id="category" value="{{ $student->subject->category }}">

                    <td><input name="written_{{ $student->studentTable->id }}" type="text" value=""></td>
                
                     @if($student->subject->category  > 1)
                    <td class="mcq" id="mcq" style="display: ;">
                      <input type="text" name="mcq_{{ $student->studentTable->id }}" value="0">
                    </td>
                    @endif
                 
                      @if($student->subject->category  > 2)
                    <td class="practical" id="practical" style="display: ;">
                      <input type="text" name="practical_{{ $student->studentTable->id }}" value="0">
                    </td>
                    @endif

                    <td></td>
                  </tr>

                 @endforeach
                   @if($noDataFound > 0) 
                 <tr>
                    <td rowspan="1">
                     <input type="submit" value="Submit" class="pull-right btn btn-primary">
                   </td>
                 </tr>
                      @endif
                </tbody>
            </table>
          </form> 
        </div>
    </div>
</div>
 <div id="getUrl" data-url="{{ url('admin/subjects/search/by/class/id')}}"></div>

@section('customjs')
<!-- jQuery -->
   
    
  <script>
    $(document).ready(function(){
     
      $('.alert-danger').fadeOut(5000);

       var category =  $('#category').val();
        if(category == 2){
          $('.col').attr('colspan',2);
        }
      
           else if(category == 3){
          $('.colspan').removeAttr('colspan');
            $('.col').attr('colspan',3);
        }
    

            var classId =   $('.classes_id').val();
        if(classId > 0)
        {
            var url = $('#getUrl').data("url");
             $.ajax({
                url: url,
                data: {classId:classId},
                type: "GET",
                success: function(res){
                  $('#subject_id').html(res);
              var subjectId =  $('.subjectId').val();
              $("#subject_id option[value='" + subjectId + "']").attr("selected","selected");
                },  
             });
           }


        $('.classes_id').on('change',function(){
           var classId =  $(this).val();
           var url = $('#getUrl').data("url");
           $.ajax({
              url: url,
              data: {classId:classId},
              type: "GET",
              success: function(response){
                $('#subject_id').html(response);
                   var subjectId =  $('.subjectId').val();
              $("#subject_id option[value='" + subjectId + "']").attr("selected","selected");
              },  
           });
        });
    });

  </script>
@endsection
@endsection
