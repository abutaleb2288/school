@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>View Mark
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 <a href="{{ url('admin/markentry') }}" class="btn btn-info"> Add Mark</a>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        	 @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif


<!--djfkldjlfjkl-->

        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/markentry/view/mark') }}" method="GET">
                     <div class="col-xs-12 col-md-2">
                         <label for="classes_id">Class<span style="color:red;">*</span></label>
                         <select name="classes_id" class="classes_id form-control" required="required">
                                    <option value="">Choose Class</option>
                                    @foreach($classes as $allclass)
                                       <option value="{{ $allclass->id }}" @if(isset($classes_id)) {{ $classes_id == $allclass->id ?'selected' : '' }} @endif>        
                                              {{$allclass->name }}
                                       </option>
                                    @endforeach
                                   
                        </select>
                     </div> 
                        <div class="col-xs-12 col-md-2">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="sessions_id" class="form-control" required="required">
                                        <option value="">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($sessions_id)) {{ $sessions_id == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                          <div class="col-xs-12 col-md-2">
                             <label for="groupes_id">Exam Term </label>
                              <select name="exam_id" class="form-control">
                                        <option value="">Choose Exam Term</option>
                                        @foreach($examterm as $allgroups)
                                          <option value="{{ $allgroups->id }}"@if(isset($exam_id)) {{ $exam_id == $allgroups->id ?'selected' : '' }} @endif>
                                                {{ $allgroups->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>  
                          <div class="col-xs-12 col-md-2">
                             <label for="sections_id">Subject </label>
                            <select name="subject_id" id="subject_id" class="form-control" >
                                  <option value="">Class Choose First</option>   
                            </select>
                         </div>    
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>

                </div>
                 
            </div>
        </div>
    
<!-- end form for validations -->

<hr>

            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Student Id</th>
                        
                        <th>Name</th>
                        <th>Class</th>
                        <th>Session</th>
                        <th  class="col">Mark Entry</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	 $i=1;
                   $j=1;
                	@endphp
             
                  @forelse($allStudent as $student)
                   <form action="{{ url('admin/markentry/update',$student->id) }}" method="POST">
                          @csrf
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td>{{ $student->studentTable->student_sid }}</td>
                      <input  name="student_id" type="hidden" value="{{ $student->studentTable->id }}">
                
               

                    <td>{{ $student->studentTable->name }} </td>

                    <td>{{ $student->classs->name }}</td>
                     <input  name="classes_id" type="hidden" value="{{ $student->classs->id  }}">

                     <input  name="exam_id" type="hidden" value="{{ $exam_id }}">
                     <input  class="subjectId" name="subject_id" type="hidden" value="{{ $subject_id  }}">

                    <td>{{ $student->sessions->name }}</td>
                     <input  name="sessions_id" type="hidden" value="{{ $student->sessions->id }}">

                      <input type="hidden" name="category" id="category" value="{{ $student->category }}">

                    <td>
                      <input value="{{ $student->written }}" name="written" type="text" value="">
                    </td>
                
                     @if($student->subject->category  > 1)
                    <td class="mcq" id="mcq" style="display: ;">
                      <input value="{{ $student->mcq }}" type="text" name="mcq" >
                    </td>
                    @endif
                 
                      @if($student->subject->category  > 2)
                    <td class="practical" id="practical" style="display: ;">
                      <input value="{{ $student->practical }}" type="text" name="practical" >
                    </td>
                    @endif

                    <td>
                            <input type="submit" value="Update" class="btn-xs btn btn-primary">
                    </td>
                  </tr>
              </form> 
                    @empty
                    <tr style="background-color:#eee;">
                       <td colspan="7" class="text-center" style="color:red;">
                          Data Not Found
                       </td>
                    </tr>
                 @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
 <div id="getUrl" data-url="{{ url('admin/subjects/search/by/class/id')}}"></div>




@section('customjs')
  <!-- jQuery -->
    <script src="{{ asset('public/backend/vendors/jquery/dist/jquery.min.js')}}"></script>
  <script>
    $(document).ready(function(){


       var category =  $('#category').val();
        if(category == 2){
          $('.col').attr('colspan',2);
        }
      
           else if(category == 3){
          $('.colspan').removeAttr('colspan');
            $('.col').attr('colspan',3);
        }
    

            var classId =   $('.classes_id').val();
        if(classId > 0)
        {
            var url = $('#getUrl').data("url");
             $.ajax({
                url: url,
                data: {classId:classId},
                type: "GET",
                success: function(res){
                  $('#subject_id').html(res);
              var subjectId =  $('.subjectId').val();
              $("#subject_id option[value='" + subjectId + "']").attr("selected","selected");
                },  
             });
           }


        $('.classes_id').on('change',function(){
           var classId =  $(this).val();
           var url = $('#getUrl').data("url");
           $.ajax({
              url: url,
              data: {classId:classId},
              type: "GET",
              success: function(response){
                $('#subject_id').html(response);
                   var subjectId =  $('.subjectId').val();
              $("#subject_id option[value='" + subjectId + "']").attr("selected","selected");
              },  
           });
        });
    });

  </script>
@endsection
@endsection
