@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>View All Subject Marked
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 <a href="{{ url('admin/markentry') }}" class="btn btn-info"> Add Mark</a>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        	 @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif


<!--djfkldjlfjkl-->

        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/markentry/show/all') }}" method="GET">
                    
                        <div class="col-xs-12 col-md-3">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="sessions_id" class="form-control" required="required">
                                        <option value="">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($sessions_id)) {{ $sessions_id == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                          <div class="col-xs-12 col-md-3">
                             <label for="groupes_id">Exam Term </label>
                              <select name="exam_id" class="form-control">
                                        <option value="">Choose Exam Term</option>
                                        @foreach($examterm as $allgroups)
                                          <option value="{{ $allgroups->id }}"@if(isset($exam_id)) {{ $exam_id == $allgroups->id ?'selected' : '' }} @endif>
                                                {{ $allgroups->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>  
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>

                </div>
                 
            </div>
        </div>
    
<!-- end form for validations -->

<hr>

            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Class</th>
                        <th>Session</th>
                        <th>Exam Type</th>
                        <th>Total Subject</th>
                        <th>Inserted Subject</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	 $i=1;
                   $j=1;
                	@endphp
             
                  @forelse($markEntry as $mark)
          
                  <tr>
                     <td> {{ $i++ }} </td>
                     <td>
                        <a href="" data-toggle="modal" data-target="#{{ $mark->id }}-myModal"> 
                          {{ $mark->classs->name }}
                        </a>
                    </td>
                     <td>{{ $mark->sessions->name }}</td>
                      <td>{{ $mark->examId->name }}</td>
                     <td>
                        @php
                          $totalSubject = \App\models\Subject::where('classes_id',$mark->subject_id)->count();
                          $totalInserted = \App\models\markEntry::where('classes_id',$mark->classes_id)->groupBy('subject_id')->where('sessions_id',$mark->sessions_id)->where('exam_id',$mark->exam_id)->get();
                          $totalInsertedSubject = $totalInserted->count();
                        @endphp
                      {{ $totalSubject }}
                     </td>
                     <td>
                      <a href="" data-toggle="modal" data-target="#{{ $mark->id }}-myModal"> {{ $totalInsertedSubject }}</a> 
                     </td>
                     <td> Already Inserted</td>
                     <td>
                      <a class="btn btn-primary btn-sm" href="" data-toggle="modal" data-target="#{{ $mark->id }}-myModal"> View </a>
                    </td>
                  </tr>
      
                    @empty
                    <tr style="background-color:#eee;">
                       <td colspan="8" class="text-center" style="color:red;">
                          Data Not Found , Please Select the Session and Exam Type field
                       </td>
                    </tr>
                 @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

  


      <!-- Modal -->
       @forelse($markEntry as $mark)
      <div id="{{ $mark->id }}-myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">This Subject is already inserted Mark</h4>
            </div>
            <div class="modal-body">
          <table class="table table-bordered table-striped">
            @php
             $totalInserted = \App\models\markEntry::where('classes_id',$mark->classes_id)->where('sessions_id',$mark->sessions_id)->where('exam_id',$mark->exam_id)->groupBy('subject_id')->get();
             @endphp
              <thead>
                <tr>
                  <td>Subject Name</td>
                  <td>Class Name</td>
                </tr>
              </thead>
              <tbody>
                 @foreach( $totalInserted as $totalSubj)
                  <tr>
                    <td>{{ $totalSubj->subject->name }}</td>
                    <td>{{ $totalSubj->classs->name }}</td>
                  </tr>
                   @endforeach
                </tbody>
           </table>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    @endforeach
@endsection
