@extends('layouts.app');
@section('title','Student Edit')
@section('content')
       <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Edit Application Student</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
				                            @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                            
                            <form action="{{ url('/admin/students/update/'.$student->id) }}" method="post">
          @csrf
          
           <p style="padding-left: 10px;font-weight:900;border-bottom: 1px solid  #000">Basic Information</p>

            <div class="col-xs-12 col-md-6">
                      <label for="">Applicant Name :</label>
                      <input type="text" class="form-control" name="name" value="{{ $student->name }}" required>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-6">
                      <label for="">Father Name :</label>
                      <input type="text" class="form-control" name="father" value="{{ $student->father }}"  required>
                      <br>
                    </div> 
                   <div class="col-xs-12 col-md-6">
                      <label for="">Mother Name :</label>
                      <input type="text" class="form-control" name="mother" value="{{ $student->mother }}"  required>
                      <br>
                    </div>
                    <div class="col-xs-12 col-md-6">
                      <label for="">Father NID :</label>
                      <input type="text" class="form-control" name="fathernid" value="{{ $student->fathernid }}" >
                      <br>
                    </div>  

                    <div class="col-xs-12 col-md-6">
                      <label for="">Gradian Mobile :</label>
                      <input type="text" class="form-control" name="gsm" value="{{ $student->gsm }}"  required>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-6">
                      <label for="">Personal Mobile :</label>
                      <input type="text" class="form-control" name="own_mobile" value="{{ $student->own_mobile }}"   required>
                      <br>
                    </div> 
                   <div class="col-xs-12 col-md-6">
                      <label for="">Birthday :</label>
                      <input type="text" class="form-control"  name="birthday" value="{{ $student->birthday }}" required>
                      <br>
                    </div>
                    <div class="col-xs-12 col-md-6">
                      <label for="">Gender :</label>
                       <select name="gender" id="" class="form-control">
                          @if($student->gender=='Male')
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          @elseif($student->gender=='Female')
                           <option value="Female">Female</option>
                           <option value="Male">Male</option>
                          @endif
                       </select>
                      <br>
                    </div>  
                    <div class="col-xs-12 col-md-6">
                      <label for="">Religion :</label>
                       <select name="religion" id="" class="form-control">
                         @if($student->religion=='Islam')
                          <option value="Islam">Islam</option>
                          <option value="Hindu">Hindu</option>
                          <option value="Chistran">Chistran</option>
                          <option value="Bondho">Bondho</option>
                         @elseif($student->religion=='Hindu')

                          <option value="Hindu">Hindu</option>
                          <option value="Islam">Islam</option>
                          <option value="Chistran">Chistran</option>
                          <option value="Bondho">Bondho</option> 
                        @elseif($student->religion=='Chistran')

                          <option value="Chistran">Chistran</option>
                          <option value="Islam">Islam</option>
                          <option value="Hindu">Hindu</option>
                          <option value="Bondho">Bondho</option>
                         @elseif($student->religion=='Bondho')

                          <option value="Bondho">Bondho</option>
                          <option value="Islam">Islam</option>
                          <option value="Hindu">Hindu</option>
                          <option value="Chistran">Chistran</option>
                          @endif


                       </select>
                      <br>
                    </div> 
                     <div class="col-xs-12 col-md-6">
                     <label for="">Blood Group :</label>
                       <select name="bloodgroup" id="" class="form-control">
                    
                            <option value="{{ $student->bloodgroup }}">{{ $student->bloodgroup }}</option>
                            <option value="N/A">N/A</option>
                            <option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                            <option value="O+">O+</option>
                            <option value="O-">O-</option>
                       </select>
                      <br>
                    </div> 
                   <div class="col-xs-12 col-md-6">
                      <label for="">Care Of :</label>
                       <textarea name="care_of" id="" class="form-control" >
                         {{ $student->care_of }}
                       </textarea>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-6">
                      <label for="">Address :</label>
                     <textarea name="address" id="" class="form-control" >
                       {{ $student->address }}
                     </textarea>
                      <br>
                   </div> 
        <p style="padding-left: 10px;font-weight:900;border-bottom: 1px solid  #000">Admission Information</p>
           
          <div class="col-xs-12 col-md-6">
                      <label for="">Class Name :</label>
                       <select name="classes_id" id="" class="form-control">
                          @foreach($class_edit as $lig)
                                <option value="  {{ $lig->id }}">  {{ $lig->name }}</option>
                              @endforeach 

                          @foreach($classes as $value)
                               <option value="{{ $value->id }}">{{ $value->name }}</option>
                          @endforeach
                       </select>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-6">
                      <label for="">Session :</label>
                       <select name="sessiones_id" id="" class="form-control">
                             @foreach($session_edit as $lig)
                                  <option value="{{ $lig->id }}">{{ $lig->name }}</option>
                             @endforeach</option>


                             @foreach($sessiones as $value)
                                  <option value="{{ $value->id }}">{{ $value->name }}</option>
                             @endforeach 

                       </select>
                      <br>
                    </div> 
                   <div class="col-xs-12 col-md-12">
                      <label for="">Select Group :</label>
                      <select name="groupes_id" id="" class="form-control">
                            @foreach($group_edit as $lig)
                                <option value="{{ $lig->id }}">{{ $lig->name }}</option>
                             @endforeach</option>
                            @foreach($groupes as $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                       </select>
                      <br>
                    </div>  
                   
         
          <p style="padding-left: 10px;font-weight:900;border-bottom: 1px solid  #000">SSC Information</p>
          
             <div class="col-xs-12 col-md-6">
                                <label for="Class name">Examination :</label>
                                <select name="examination" class="form-control">
                                    @if($data['student_edu']->student_info_type==3)
                                      <option value="3">SSC/Dakhil/Equivalent</option>
                                      <option value="4">SSC Vocational</option>
                                    @elseif($data['student_edu']->student_info_type==4)
                                      <option value="4">SSC Vocational</option>
                                      <option value="3">SSC/Dakhil/Equivalent</option>
                                    @endif
                                 </select>
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <label for="">Institute Name :</label>
                                <input type="text" class="form-control" name="institutename" value="{{ $data['student_edu']->institute_name }}"  required>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <label for="Class name">Passsing Year :</label>
                                <select name="passing_year" class="form-control">
                                    <option value="{{ $data['student_edu']->year }}"> {{ $data['student_edu']->year }}</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                 </select>
                            </div> 
                            <div class="col-xs-12 col-md-6">
                                <label for="Class name">Select Group :</label>
                                <select name="pre_groupes_id" class="form-control">
                                       @if($data['student_edu']->groupes_id=='Science')

                                            <option value="Science">Science</option>
                                            <option value="Humanitarian">Humanitarian</option>
                                            <option value="Business_Study">Business Study</option>
                                       @elseif($data['student_edu']->groupes_id=='Humanitarian')

                                            <option value="Humanitarian">Humanitarian</option>
                                            <option value="Science">Science</option>
                                            <option value="Business_Study">Business Study</option>

                                       @elseif($data['student_edu']->groupes_id=='Business_Study')
                                           <option value="Business_Study">Business Study</option>
                                           <option value="Humanitarian">Humanitarian</option>
                                           <option value="Science">Science</option>
                                        @endif



                                 </select>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <label for="Board">Board :</label>
                                <select name="board" class="form-control" required>
                                    <option value="{{ $data['student_edu']->board }}" selected>{{ $data['student_edu']->board }}</option>
                                    <option value="barisal">Barisal</option>
                                    <option value="chittagong">Chittagong</option>
                                    <option value="comilla">Comilla</option>
                                    <option value="dhaka">Dhaka</option>
                                    <option value="dinajpur">Dinajpur</option>
                                    <option value="jessore">Jessore</option>
                                    <option value="rajshahi">Rajshahi</option>
                                    <option value="sylhet">Sylhet</option>
                                    <option value="madrasah">Madrasah</option>
                                    <option value="Technical">Technical</option>
                                    <option value="dibs">DIBS(Dhaka)</option>
                                </select>
                            </div>
                           <div class="col-xs-12 col-md-6" >
                                <label for="Roll">Roll :</label>
                                <input type="text" class="form-control" name="pre_roll" value="{{ $data['student_edu']->pre_roll }}"  required>
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <label for="Roll">Registration :</label>
                                <input type="text" class="form-control" name="pre_reg" value="{{ $data['student_edu']->pre_reg }}"  required>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <label for="Result">Result's :</label>
                                <input type="text" class="form-control" name="pre_result" value="{{ $data['student_edu']->result }}"   required>
                            </div>
                     
                           <div class="col-xs-12 col-md-12">
                                        <label for=""></label>
                                        <br>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
  
                          </form>
                    <!-- end form for validations -->
                              


                            </div>
                        </div>
                    </div>



@endsection