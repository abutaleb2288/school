@extends('layouts.app')
@section('content')

 <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Student Details View</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Information</th>
                                            <th>Item</th>
                                            <th>Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <th class="text-center" colspan="4">Basic Information</th>
                                         </tr>
                                        <tr>
                                            <th>Student ID</th>
                                            <td>{{ $student->sid }}</td>
                                            <th>Name</th>
                                            <td>{{ $student->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>RF ID</th>
                                            <td>{{ $student->rf_id }}</td>
                                            <th>Father Name</th>
                                            <td>{{ $student->father }}</td>
                                        </tr>
                                        <tr>
                                            <th>Monther Name</th>
                                            <td>{{ $student->mother }}</td>
                                            <th>Father NID</th>
                                            <td>{{ $student->fathernid }}</td>
                                        </tr>
                                        <tr>
                                            <th>Birthday</th>
                                            <td>{{ $student->birthday }}</td>
                                            <th>Blood Group</th>
                                            <td>{{ $student->bloodgroup }}</td>
                                        </tr>  
                                        <tr>
                                            <th>Gradian Mobile</th>
                                            <td>{{ $student->gsm }}</td>
                                            <th>Student Mobile Number</th>
                                            <td>{{ $student->own_mobile }}</td>
                                        </tr>
                                        
                                         <tr>
                                            <th>Gender</th>
                                            <td>{{ $student->gender }}</td>
                                            <th>Religion</th>
                                            <td>{{ $student->religion }}</td>
                                        </tr>  
                                        <tr>
                                            <th>Address</th>
                                            <td>{{ $student->address }}</td>
                                            <th>Care Of</th>
                                            <td>{{ $student->care_of }}</td>
                                        </tr>   
                                         <tr>
                                             <th class="text-center" colspan="4">Academic Information</th>
                                         </tr>
                                      <tr>
                                            <th>Class</th>
                                            <td>

                                                <?php

                                                $result = DB::table('student_informations')
                                                          ->where('student_id',$student->id)
                                                          ->get();

                                                          foreach($result as $getvalue){

                                                             $idshow =  $getvalue->classes_id;
                                                              
                                                               $results = DB::table('classes')
                                                                      ->where('id',$idshow)
                                                                      ->get();
                                                                
                                                                foreach($results as $getclass){
                                                                    echo $getclass->name;
                                                                }   

                                                            }
                                                        

                                             ?>
                                                
                                             </td>

                                            <th>Group</th>   
                                            <td>

                                                <?php

                                                $result = DB::table('student_informations')
                                                          ->where('student_id',$student->id)
                                                          ->get();

                                                          foreach($result as $getvalue){

                                                             $idshow =  $getvalue->groupes_id;
                                                              
                                                               $results = DB::table('groupes')
                                                                      ->where('id',$idshow)
                                                                      ->get();
                                                                
                                                                foreach($results as $getgroup){
                                                                    echo $getgroup->name;
                                                                }   

                                                            }
                                                        

                                             ?>
                                                 
                                             </td> 

                                            </tr>
                                            <tr>
                                                <th >Session</th>
                                                 <td colspan="3">
                                            <?php

                                                $result = DB::table('student_informations')
                                                          ->where('student_id',$student->id)
                                                          ->get();

                                                          foreach($result as $getvalue){

                                                             $idshow =  $getvalue->sessiones_id;
                                                              

                                                               $results = DB::table('sessiones')
                                                                      ->where('id',$idshow)
                                                                      ->get();
                                                                
                                                                foreach($results as $getsessions){
                                                                    echo $getsessions->name;
                                                                }   

                                                            }
                                                        

                                             ?></td> 
                                            </tr>   
                                         
                                        <tr>
                                             <th class="text-center" colspan="4">Previous Academic Information</th>
                                         </tr>
                                        <?php

                                                $result = DB::table('student_educations')
                                                            ->where('student_id',$student->id)
                                                            ->get();

                                                        foreach($result as $get_info){?> 
                                         <tr>
                                         <th>Examination</th>
                                           <td> @if($get_info->student_info_type ==3)
                                                    SSC/Dhakhil
                                                    @elseif($get_info->student_info_type ==4)
                                                    Vocational
                                                    @endif
                                              
                                </td>
                                         <th>Institute Name</th>
                                         <td> {{ $get_info->institute_name }} </td>
                                     </tr>
                                     <tr>
                                        
                                         <th>Roll </th>
                                         <td>{{ $get_info->pre_roll }}</td>
                                   
                                         <th>Registration</th>
                                         <td>{{ $get_info->pre_reg }}</td>
                                   </tr> 
                                    <tr>
                                         <th>Result </th>
                                         <td>{{ $get_info->result }}</td>
                                     
                                         <th>Board</th>
                                         <td>{{ $get_info->board }}</td>
                                    </tr>
                                     <tr>
                                         <th>Group </th>
                                         <td colspan="3">{{ $get_info->groupes_id }}</td>
                                     </tr>
                                          
                                    

                                       <?php  }  ?>


 
                                         <tr>
                                            <th>Status</th>
                                             <td colspan="3" >
                                                @if($student->status==1)
                                                <p class="btn btn-primary btn-xs">Active</p>
                                                @elseif($student->status==0)
                                                <p class="btn btn-danger btn-xs">Pending</p>
                                                @endif
                                             </td>
                                             

                                        </tr>
                                         
                                        
                                        <tr>
                                           <th>Action</th>
                                            <td colspan="3">
                                               <a href="{{ url('/admin/students/delete/'.$student->id) }}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                               <a href="{{ url('/admin/students/edit/'.$student->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                            
                                         </tr>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>


@endsection