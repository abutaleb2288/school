@extends('layouts.app')
@section('title','Notices Add')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Book</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


                <form action="{{ url('/admin/book/update',$book->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Book Name </label>
                      <input type="text" class="form-control" name="name" value="{{ $book->name ?? old('name') }}" placeholder="Book Name" required>
                      <br>
                    </div> 

                    <div class="col-xs-12 col-md-12">
                      <label for="">Book ID </label>
                      <input type="text" class="form-control" name="book_id" value="{{ $book->book_id ?? old('book_id') }}" placeholder="Book ID , tpye an Unique value" required>
                      <br>
                    </div> 
       
                    
                    <div class="col-xs-12 col-md-12">
                      <label for="">Category :</label>
                      <select name="category_id" id="category_id" class="form-control" required>
                       @foreach($category as $categry)
                          <option {{ $book->category_id == $categry->id ? 'selected' : '' }} value="{{ $categry->id }}">{{ $categry->name }}
                        </option>
                        @endforeach
                      </select>
                      <br>
                   </div> 
                  <div class="col-xs-12 col-md-12">
                      <label for="">Writter :</label>
                      <select name="writter_id" id="writter_id" class="form-control" required>
                        @foreach($writter as $writer)
                        <option value="{{ $writer->id }}" {{ $book->writter_id == $writer->id ? 'selected' : '' }}>{{ $writer->name }}</option>
                        @endforeach
                      </select>
                      <br>
                   </div> 

                   <div class="col-xs-12 col-md-12">
                      <label for="">Edition </label>
                      <input type="text" class="form-control" name="edition" value="{{ $book->edition ?? old('edition') }}" placeholder="Edition" required>
                      <br>
                    </div>

                    <div class="col-xs-12 col-md-12">
                      <label for="">Publisher (Prokashoni) </label>
                      <input type="text" class="form-control" name="prokashoni" value="{{ $book->prokashoni ?? old('prokashoni') }}" placeholder="Publisher (Prokashoni)" required>
                      <br>
                    </div>

                   <div class="col-xs-12 col-md-12">
                      <label for="">Quantity </label>
                      <input type="text" class="form-control" name="qty" value="{{ $book->qty ?? old('qty') }}" placeholder="Quantity" required>
                      <br>
                    </div>
                   <div class="col-xs-12 col-md-12">
                      <label for="">Book Image <small>(optional)</small>:</label>
                       <input type="file" class="form-control" name="file">
                      <br>
                    </div>
                       
                  <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                          <a href="{{ url('admin/book') }}" class="btn btn-danger"> Cancel</a>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
