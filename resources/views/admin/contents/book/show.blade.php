@extends('layouts.app')
@section('title','News Show')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>View  Book</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
                              <th>Menu</th>
                              <th>INformation</th>
                           </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Book Serial</td>
                          <td>{{ $book->id }}</td>
                        </tr>
                        <tr>
                          <th>Book Name</th>
                          <td>{{ $book->name }}</td>
                        </tr>
                      
                          <tr>
                          <th>Book ID</th>
                          <td>{{ $book->book_id }}</td>
                        </tr>
                   
                          <tr>
                          <th>Category Name</th>
                          <td>{{ $book->category->name }}</td>
                        </tr>
                     
                          <tr>
                          <th>Writter Name</th>
                          <td>{{ $book->writter->name }}</td>
                        </tr>
                      
                          <tr>
                          <th>Quantity</th>
                          <td>{{ $book->qty }}</td>
                        </tr>

                        <tr>
                          <th>Edition</th>
                          <td>{{ $book->edition }}</td>
                        </tr>

                        <tr>
                          <th>Publisher <small>(Prokashoni)</small></th>
                          <td>{{ $book->prokashoni }}</td>
                        </tr>

                        <tr>
                          <th>Image</th>
                          <td>
                            <img src="{{ asset('public/backend/book/'.$book->image) }}" alt="" width="100%" height="70%">
                          </td>
                        </tr>
                      </tbody>                       
                        <tr>
                          <td>Status</td>
                          <td>@if($book->status==1)
                                <p class="btn btn-primary btn-xs">Active</p> 
                               @else
                                <p class="btn btn-danger btn-xs">Deactive</p> 
                               @endif
                              </td>
                        </tr>
                        <tr>
                          <td>Date</td>
                          <td>{{ $book->created_at->format('Y-m-d H:i A') }}</td>
                        </tr>
                        <tr>
                          <td>Action</td>
                          <td>
                                <a href="{{ url('admin/book') }}" class="btn btn-xs btn-primary"> Back</a>
                                <a href="{{ url('/admin/book/edit/'.$book->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                               <a href="{{ url('/admin/book/delete/'.$book->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>

                      </tbody>
      

                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
