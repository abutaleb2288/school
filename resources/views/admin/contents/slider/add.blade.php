@extends('layouts.app')
@section('title','Notices Add')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add New Slider</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


                <form action="{{ url('/admin/slider/store') }}" method="post" enctype="multipart/form-data">
                @csrf
                  

                  
                   <div class="col-xs-12 col-md-12">
                      <label for="">Slider Image <small></small>:</label>
                       <input type="file" class="form-control" name="file">
                      <br>
                    </div>


                  <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                        <a href="{{ url('admin/slider') }}" class="btn btn-danger"> Cancel</a>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
