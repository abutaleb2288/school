@extends('layouts.app')
@section('title','News Show')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>View  Contents </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                    <table id="" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
                              <th>Menu</th>
                              <th>INformation</th>
                           </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Content ID</td>
                          <td>{{ $contents->id }}</td>
                        </tr>
                        <tr>
                          <th>Title</th>
                          <td>{{ $contents->title }}</td>
                        </tr>
                        <tr>
                          <th>Description</th>
                          <td>{!! $contents->description !!}</td>
                        </tr>
                        <tr>
                          <td>Position</td>
                          <td> 
                              @if($contents->position==1)
                                 কলেজ সংক্রান্ত তথ্য
                                @elseif($contents->position==2) 
                                ভৌত অবকাঠামো
                                @elseif($contents->position==3) 
                                যোগাযোগ
                                @elseif($contents->position==4) 
                                কলেজ গর্ভনিং বডি
                                @elseif($value->position==5) 
                                ভর্তি সংক্রান্ত তথ্য
                                @elseif($contents->position==6) 
                                নিয়মাবলী
                                @elseif($contents->position==7) 
                                একাডেমিক ক্যালেন্ডার
                                @elseif($contents->position==8) 
                                সাংস্কৃতিক কর্মকান্ড
                                @elseif($contents->position==9) 
                                খেলাধুলা 
                                @elseif($contents->position==10) 
                                 অধ্যক্ষের নামের তালিকা
                                @endif

 
                           </td>
                        </tr>
                        <tr>
                          <td>Date</td>
                          <td>{{ $contents->created_at->format('Y-m-d H:i A') }}</td>
                        </tr>
                        <tr>
                          <td>Action</td>
                          <td>
                               
                                <a href="{{ url('/admin/contents/edit/'.$contents->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                               <a href="{{ url('/admin/contents/delete/'.$contents->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>

                      </tbody>
      

                      
                  
         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
