@extends('layouts.app')
@section('title','News Add')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add New News</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


             <form action="{{ url('/admin/contents/store') }}" method="post">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Title </label>
                      <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Title" required>
                      <br>
                    </div> 
       
                   <div class="col-xs-12 col-md-12">
                      <label for="">Description :</label>
                       <textarea name="description"  id="summernote" class="form-control" value="{{ old('description') }}"  placeholder="Description"></textarea>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-12">
                      <label for="">Position :</label>
                      <select name="position" id="" class="form-control">
                        <option value="">Choose Position</option>
                        <option value="1">কলেজ সংক্রান্ত তথ্য</option>
                        <option value="2">ভৌত অবকাঠামো</option>
                        <option value="3">যোগাযোগ</option>
                        <option value="4">কলেজ গর্ভনিং বডি</option>
                        <option value="5">ভর্তি সংক্রান্ত তথ্য</option>
                        <option value="6">নিয়মাবলী</option>
                        <option value="7">একাডেমিক ক্যালেন্ডার</option>
                        <option value="8">সাংস্কৃতিক কর্মকান্ড</option>
                        <option value="9">খেলাধুলা</option>
                        <option value="10">অধ্যক্ষের নামের তালিকা </option>
                      </select>
                      <br>
                   </div> 

                  <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
