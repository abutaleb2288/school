@extends('layouts.app')
@section('title','Contents List')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Contents List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                                          <li>
                <a href="{{ url('/admin/contents/add') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New </a>
            </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                           @if(Session::get('success'))
                                   <p class="alert alert-success">{{ Session::get('success') }}</p>
                              @elseif(Session::get('error'))
                               <p class="alert alert-danger">{{ Session::get('error') }}</p>
                              @endif

                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
                              <th>Serial</th>
                              <th>Title</th>
                              <th>Position</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                            @php
                           $i=1;
                        @endphp
                        @foreach($contents as $value)
                           <tr>
                              <td>{{ $i++ }}</td>
                              <td>{{ $value->title }} </td>
                              <td>
                                @if($value->position==1)
                                 কলেজ সংক্রান্ত তথ্য
                                @elseif($value->position==2) 
                                ভৌত অবকাঠামো
                                @elseif($value->position==3) 
                                যোগাযোগ
                                @elseif($value->position==4) 
                                কলেজ গর্ভনিং বডি
                                @elseif($value->position==5) 
                                ভর্তি সংক্রান্ত তথ্য
                                @elseif($value->position==6) 
                                নিয়মাবলী
                                @elseif($value->position==7) 
                                একাডেমিক ক্যালেন্ডার
                                @elseif($value->position==8) 
                                সাংস্কৃতিক কর্মকান্ড
                                @elseif($value->position==9) 
                                খেলাধুলা  
                               @elseif($value->position==10) 
                                অধ্যক্ষের নামের তালিকা
                                @endif

 

                              </td> 
                              
                              <td>
                                  <a href="{{ url('/admin/contents/show/'.$value->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a>
                                  <a href="{{ url('/admin/contents/edit/'.$value->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                                  <a href="{{ url('/admin/contents/delete/'.$value->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('are you sure? Want to delete this data')"><i class="fa fa-trash"></i> Delete</a>
                              </td>
                           </tr>
                         @endforeach


                      </tbody>
      

                      
                  
         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
