@extends('layouts.app')
@section('title','News Edit')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit News</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                    @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


             <form action="{{ url('/admin/news/update/'.$news->id) }}" method="post">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Title </label>
                      <input type="text" class="form-control" name="title" value="{{ $news->title }}" required>
                      <br>
                    </div> 
       
                   <div class="col-xs-12 col-md-12">
                      <label for="">Description :</label>
                       <textarea name="description" id="summernote" class="form-control">
                         {{ $news->description }}
                       </textarea>
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-12">
                      <label for="">Status :</label>
                      <select name="status" id="" class="form-control">
                        @if($news->status==1)
                          <option value="1">Publish</option>
                          <option value="0">Daft</option>
                        @else
                          <option value="0">Daft</option>
                          <option value="1">Publish</option>
                        @endif
                      </select>
                      <br>
                   </div> 
                 <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>
 
  
        </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
