@extends('layouts.app')
@section('title','Notices List')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Blog Post List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                                          <li>
                <a href="{{ url('/admin/blog/post/add') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New </a>
            </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                           @if(Session::get('success'))
                                   <p class="alert alert-success">{{ Session::get('success') }}</p>
                              @elseif(Session::get('error'))
                               <p class="alert alert-danger">{{ Session::get('error') }}</p>
                              @endif

                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
                              <th>Serial</th>
                              <th>Post Title</th>
                              <th>Category</th>
                              <th>Status</th>
                              <th>Date</th>
                               <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                            @php
                           $i=1;
                        @endphp
                        @foreach($post as $value)
                           <tr>
                              <td>{{ $i++ }}</td>
                              <td>{{ str_limit($value->title,20) }} </td>
                              <td>{{ $value->category->name }}  </td>

                              <td>@if($value->status==1)
                                <p class="btn btn-primary btn-xs">Active</p> 
                               @else
                                <p class="btn btn-danger btn-xs">Deactive</p> 
                               @endif
                              </td>
                              <td>{{ $value->created_at->format('D - M - Y H:i A') }} </td>
                              <td>
                                  <a href="{{ url('/admin/blog/post/show/'.$value->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a>
                                  <a href="{{ url('/admin/blog/post/edit/'.$value->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                                  <a href="{{ url('/admin/blog/post/delete/'.$value->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('are you sure? Want to delete this data')"><i class="fa fa-trash"></i> Delete</a>
                              </td>
                           </tr>
                         @endforeach


                      </tbody>
      

                      
                  
         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
