@extends('layouts.app')
@section('title','Notices Add')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Blog Post</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


                <form action="{{ url('/admin/blog/post/update',$post->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Title </label>
                      <input type="text" class="form-control" name="title" value="{{ $post->title ?? old('title') }}" placeholder="Post Title" required>
                      <br>
                    </div> 

                    
       
                    
                    <div class="col-xs-12 col-md-12">
                      <label for="">Category :</label>
                      <select name="category_id" id="category_id" class="form-control" required>
                       @foreach($category as $categry)
                          <option {{ $post->category_id == $categry->id ? 'selected' : '' }} value="{{ $categry->id }}">{{ $categry->name }}
                        </option>
                        @endforeach
                      </select>
                      <br>
                   </div> 
                
                

                   <div class="col-xs-12 col-md-12">
                      <label for="">Thumbnail  <small></small></label>
                       <input type="file" class="form-control" name="thumnail">
                      <br>
                    </div>  

                    <div class="col-xs-12 col-md-12">
                      <label for="">Description </label>
                      <textarea id="summernote" name="description" class="form-control">
                          {{ $post->description }}
                      </textarea>
                      <br>
                    </div>

                  <div class="col-xs-12 col-md-12">
                        <label for=""></label>
                         <br>
                          <a href="{{ url('admin/blog/post') }}" class="btn btn-danger"> Cancel</a>
                        <input type="submit" class="btn btn-primary" value="Update">
                    </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
