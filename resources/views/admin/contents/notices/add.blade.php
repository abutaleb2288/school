@extends('layouts.app')
@section('title','Notices Add')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add New Notice</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


                <form action="{{ url('/admin/notices/store') }}" method="post" enctype="multipart/form-data">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Title </label>
                      <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Title" required>
                      <br>
                    </div> 
       
                   <div class="col-xs-12 col-md-12">
                      <label for="">Notice File :</label>
                       <input type="file" class="form-control" name="file">
                      <br>
                    </div> 
                    <div class="col-xs-12 col-md-12">
                      <label for="">Notice By :</label>
                      <select name="notice_by" id="" class="form-control" required>
                        <option value="1">General</option>
                        <option value="2">HSC 1st Year</option>
                        <option value="3">HSC 2st Year</option>
                        <option value="4">Hon's</option>
                        <option value="5">Degree</option>
                      </select>
                      <br>
                   </div> 

                  <div class="col-xs-12 col-md-12">
                      <label for="">Status :</label>
                      <select name="status" id="" class="form-control" required>
                        <option value="1">Publish</option>
                        <option value="0">Daft</option>
                      </select>
                      <br>
                   </div> 

                  <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
