@extends('layouts.app')
@section('title','News Show')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>View  Category</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
                              <th>Menu</th>
                              <th>INformation</th>
                           </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Category ID</td>
                          <td>{{ $category->id }}</td>
                        </tr>
                        <tr>
                          <th>Title</th>
                          <td>{{ $category->name }}</td>
                        </tr>
                       
                        <tr>
                          <td>Status</td>
                          <td>@if($category->status==1)
                                <p class="btn btn-primary btn-xs">Active</p> 
                               @else
                                <p class="btn btn-danger btn-xs">Deactive</p> 
                               @endif
                              </td>
                        </tr>
                        <tr>
                          <td>Date</td>
                          <td>{{ $category->created_at->format('Y-m-d H:i A') }}</td>
                        </tr>
                        <tr>
                          <td>Action</td>
                          <td>
                               
                                <a href="{{ url('/admin/category/edit/'.$category->id) }}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
                               <a href="{{ url('/admin/category/delete/'.$category->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>

                      </tbody>
      

                      
                  
         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
