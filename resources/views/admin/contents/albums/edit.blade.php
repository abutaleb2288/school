@extends('layouts.app')
@section('title','Albums Edit')
@section('content') 
   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Album</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                        

                      @if ($errors->any())
                       <div class="alert alert-danger">
                         <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                  @endforeach
                          </ul>
                       </div>
                       @endif


                <form action="{{ url('/admin/albums/update/'.$album->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                   <div class="col-xs-12 col-md-12">
                      <label for="">Title </label>
                      <input type="text" class="form-control" name="name" value="{{ $album->name }}" placeholder="Title" required>
                      <br>
                    </div> 
       
                   <div class="col-xs-12 col-md-12">
                      <label for="">Album image :</label>
                       <img src="{{ asset('public/fontend/albums/'.$album->a_image) }}" alt="" width="100px">   
                       <input type="file" class="form-control" name="image">
                      <br>
                    </div> 
               
                      <div class="col-xs-12 col-md-12">
                      <label for="">Status :</label>
                      <select name="status" id="" class="form-control" required>
                        @if($album->status==1)
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                        @else
                        <option value="0">Deactive</option>
                        <option value="1">Active</option>
                        @endif
                      </select>
                      <br>
                   </div> 

                  <div class="col-xs-12 col-md-12">
                         <label for=""></label>
                         <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                  </form>

                   
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
