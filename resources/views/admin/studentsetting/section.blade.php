@extends('layouts.app')
@section('title','Classes')
@section('content') 

    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Section List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>Section name</th>
                          <th>Action</th>
                         </tr>
                      </thead>
                      <tbody>
                        @php
                           $i=1;
                        @endphp
                        @foreach($sections as $value)
                        <tr>
                          <td>{{ $i++ }}</td>
                          
                          <td>{{ $value->name }}</td>
                          
                          <td><p class="btn btn-primary btn-xs"></p></td>
                           
                        </tr>
                        @endforeach
         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
           
 
     
@endsection
              
              
