@extends('layouts.app')
@section('title','Student Subject View')
@section('content') 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>All Class Routine View
               
            </h2>

            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        	 @if(Session::get('success'))
               <p class="alert alert-success">{{ Session::get('success') }}</p>
          @elseif(Session::get('error'))
           <p class="alert alert-danger">{{ Session::get('error') }}</p>
          @endif


<!--djfkldjlfjkl-->

        <div class="row">
            <div class="container-flude">
                <div class="col-md-11">
                   <form action="{{ url('admin/routine/index') }}" method="GET">
                     <div class="col-xs-12 col-md-3">
                         <label for="classes_id">Class<span style="color:red;">*</span></label>
                         <select name="class_id" class="form-control" required="required">
                                    <option value="0">Choose Class</option>
                                    @foreach($classes as $allclass)
                                       <option value="{{ $allclass->id }}" @if(isset($IdClass)) {{ $IdClass == $allclass->id ?'selected' : '' }} @endif>        
                                              {{$allclass->name }}
                                       </option>
                                    @endforeach
                                   
                        </select>
                     </div> 
                        <div class="col-xs-12 col-md-3">
                             <label for="sessions_id">Session <span style="color:red;">*</span></label>
                                <select name="session_id" class="form-control" required="required">
                                        <option value="0">Choose Session</option>
                                        @foreach($sessions as $session)
                                          <option value="{{ $session->id }}"@if(isset($IdSession)) {{ $IdSession == $session->id ?'selected' : '' }} @endif>
                                                {{ $session->name }}
                                            </option>
                                        @endforeach
                                </select>
                         </div>   
                         
                          <div class="col-xs-12 col-md-3">
                             <label for="section_id">Section </label>
                              <select name="section_id" class="form-control" >
                                        <option value="0">Choose Section</option>
                                        @foreach($sections as $section)
                                          <option value="{{ $section->id }}"@if(isset($IdSection)) {{ $IdSection == $section->id ?'selected' : '' }} @endif>
                                                {{ $section->name }}
                                            </option>
                                        @endforeach
                            </select>
                         </div>    
                         <div class="col-xs-12 col-md-2">
                             <label for="">&nbsp;</label>
                             <input type="submit"  class="btn btn-primary form-control" value="Search">
                         </div>
                    </form>
                    
                </div>
                 
            </div>
        </div>
	<hr>

	<div class="row">
		<div class="col-md-12">
			<div class="table">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="background-color:brown;color:yellow;">Class : @isset($className) {{ $className }} @endisset</th>
							<th style="background-color:brown;color:yellow;"> Session : @isset($sessionName) {{ $sessionName }} @endisset</th>
							<th style="background-color:brown;color:yellow;"> Section : @isset($SectionName)  {{ $SectionName }} @endisset
							</th>
							@isset($SectionName)
							<th>
								<a href="" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-xs"> Add New</a>
							</th>
							 @endisset
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	
<!-- end form for validations -->
  
         <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width:4%;">Sl.</th>
                        <th style="width:10%;">Day - Time</th>
                        @foreach($classTime as $time)
                          <th>{{ $time->time_id }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                        
                </tbody>
            </table>


            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial</th>
                         <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Subject Name</th>
                        <th>Teacher</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	 $i=1;
                	@endphp
                  @foreach($routine as $routines)
	                  <tr>
                      <td>{{ $i++ }}</td>
	                  		<td> {{ $routines->day->name }}</td>
                        <td> {{ $routines->time->name }}</td>
                        <td> {{ $routines->endtime_id == null ? "No time" : $routines->endtime->name }}</td>
	                   		<td> {{ $routines->subject->name }}</td>
	                   		<td>
                          {{  $routines->teachers->name}}
                       </td>
	                   		<td>Add New</td>
	                  </tr>
                 @endforeach
                </tbody>
            </table>


        </div>
    </div>
</div>



				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Add Time , Teacher and Day for this class of this section</h4>
				      </div>
				      <div class="modal-body">
				       
				       <form class="form-horizontal form-label-left" method="post" action="{{ url('/admin/routine/store') }}">
                          @csrf
                         
						        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Time  :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="time_id" class="form-control" required="required">
                                    <option value="">Choose Time </option>
                                    @foreach($times as $time)
                                    <option value="{{ $time->id }}">{{ $time->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div> 

                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">End Time  :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="endtime_id" class="form-control" required="required">
                                    <option value="">Choose Time </option>
                                    @foreach($times as $time)
                                    <option value="{{ $time->id }}">{{ $time->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div> 

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Day :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="day_id" class="form-control" required="required">
                                    <option value="">Choose Day</option>
                                    @foreach($days as $day)
                                    <option value="{{ $day->id }}">{{ $day->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div>     
                         
                         @isset($subject)
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Subject :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="subject_id" id="subject_id" class="form-control" required="required">
                                    <option value="0">Choose Subject</option> 
                                    @foreach($subject as $sub)
										                  <option value="{{ $sub->id }}"> {{ $sub->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endisset
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Teacher :</label>
                            <div class="col-md-7 col-sm-9 col-xs-12">
                                <select name="teacher_id" class="form-control" required="required">
                                    <option value="">Choose Teacher</option>
                                    @foreach($teachers as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                 
                                </select>
                            </div>
                        </div>     
                          
              <input type="hidden" name="section_id" value="{{ isset($IdSection) ? $IdSection : null  }}">
              <input type="hidden" name="session_id" value="{{ isset($IdSession) ? $IdSession : null  }}">
             <input type="hidden" name="class_id" value="{{ isset($IdClass) ? $IdClass  : null }}">
                               
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                   
                </div>

				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
 						</form>
				  </div>
				</div>


@endsection
