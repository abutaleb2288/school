
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title','Softech || Digital College')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

 
 
    <!-- Bootstrap -->
    <link href="{{ asset('public/backend/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('public/backend/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <!-- Datatables -->
    <link href="{{ asset('public/backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('public/backend/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    
    <!-- text area summer note -->

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('public/backend/build/css/custom.min.css')}}" rel="stylesheet">

 
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Digital College</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('public/fontend/images/principal.jpg') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-edit"></i>Admission Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/application/pending') }}"> Online Application's </a></li>
                    </ul>
                  </li> 
                      
                  <li><a><i class="fa fa-edit"></i>Student's Setting <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/classes') }}"> Class </a></li>
                      <li><a href="{{ url('/admin/sessions') }}"> Session </a></li>
                      <li><a href="{{ url('/admin/groupes') }}"> Group </a></li>
                      <li><a href="{{ url('/admin/sections') }}"> Section </a></li>
                   </ul>
                  </li>
                  
                  
                  <li><a><i class="fa fa-desktop"></i>Student's Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/students') }}"> Student's List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i>Subject's Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/subjects') }}"> Subject's List</a></li>
                      <li><a href="{{ url('/admin/subjects/students') }}">Student's Subject's Distribution</a></li>
                     <li><a href="{{ url('/admin/subjects/teachers/view') }}">Teachers Subject's Distribution</a></li>
                    </ul>
                  </li>
                    <li>
                      <a><i class="fa fa-bar-chart-o"></i> Teacher's Management <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                           <li><a href="{{ url('/admin/teachers') }}">Teacher's </a></li>
                       </ul>
                  </li> 

                  <li>
                      <a><i class="fa fa-bar-chart-o"></i>Routine Management <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                           <li><a href="{{ url('/admin/routine/index') }}"> View Routine </a></li>
                       </ul>
                  </li> 
              

                  <li><a><i class="fa fa-desktop"></i>Exam Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/examterms') }}"> Exam Type</a></li>
                      <li><a href="{{ url('/admin/markentry') }}">Mark Entry</a></li>
                      <li><a href="{{ url('/admin/markentry/view/mark') }}">View Mark </a></li>
                      <li><a href="{{ url('/admin/markentry/show/all') }}">Summery Mark </a></li>
                      
                    </ul>
                  </li> 
                  <li><a><i class="fa fa-desktop"></i>Attendance Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/attendances/students') }}"> Students Attendance</a></li>
                      <li><a href="{{ url('/admin/attendances/teachers') }}"> Teachers Attendance </a></li>
                      
                    </ul>
                  </li>
                 

                  
                </ul>

                
              </div>

              <div class="menu_section">
                <h3>Accounts</h3>
                  <ul class="nav side-menu">
                      <li><a><i class="fa fa-money"></i>Expense<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/daily/expense/index') }}">View  Expense</a>
                      </li>
                      <li>
                        <a href="{{ url('/admin/daily/expense/create') }}">Add  Expense
                        </a>
                      </li>
                      <li>
                        <a href="{{ url('/admin/expense/report') }}">Expense Report
                        </a>
                      </li>
                    </ul>
                  </li>
              
                  <li><a><i class="fa fa-money"></i>Expense Purpose<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/expense/purpose/index') }}">View  Purpose</a>
                      </li>
                      <li><a href="{{ url('/admin/expense/purpose/create') }}">Add  Purpose</a>
                      </li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-money"></i>Salary Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/salary/create') }}"> Add Salary</a></li>
                      <li><a href="{{ url('/admin/salary/index') }}"> View Salary</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-money"></i>Student Payments <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/salary/create') }}"> Fee Category</a></li>
                      <li><a href="{{ url('/admin/salary/index') }}">  Fee Sub Category</a></li>
                      <li><a href="{{ url('/admin/salary/index') }}">  Fee Collection</a></li>
                    </ul>
                  </li> 
                  </ul>
              </div>






              <div class="menu_section">
                <h3>Web Site</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i>Content Management<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/notices') }}">Notice's</a></li>
                      <li><a href="{{ url('/admin/gallery') }}">Gallery</a></li>
                      <li><a href="{{ url('/admin/albums') }}">Albums</a></li>
                      <li><a href="{{ url('/admin/contents') }}">Content's</a></li>
                      <li><a href="{{ url('/admin/news') }}">News</a></li>
                      <li><a href="{{ url('/admin/slider') }}">Slider Setting</a></li>
                      <li><a href="{{ url('/admin/event') }}">Event Setting</a></li>
                      <li><a href="{{ url('/admin/blog/category') }}">Blog Category</a></li>
                      <li><a href="{{ url('/admin/blog/post') }}">Post's Setting</a></li>
                      <li><a href="{{ url('/admin/users') }}">User's</a></li>

                    </ul>
                  </li>
                </ul>
                 <ul class="nav side-menu">
                   <li><a><i class="fa fa-bug"></i>Library Management<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('/admin/category') }}">Category Setting</a></li>
                       <li><a href="{{ url('/admin/writter') }}">Writter's Setting</a></li>
                      <li><a href="{{ url('/admin/book') }}">Book's</a></li>
                      
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
  
          </div>
        </div>

       
       
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
               
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">{{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                          <span>Settings</span>
                      </a>
                    </li>
                    
                    <li><a class="" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                         </form>
                      </li>
                  </ul>
                </li>

                
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
  
       
       
       
       
       
       
       
        <!-- page content -->
        <div class="right_col" role="main"><!-- =======================================================================-->
           <div class="row">
             <main class="py-4">



                   @yield('content')







             </main>
               
 
 
            </div>
        </div>
        <!-- /page content -->

       
       
       
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Digital College <a href="https://softech.com.bd">Softech BD</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('public/backend/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('public/backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{ asset('public/backend/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('public/public/backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{ asset('public/public/public/backend/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{ asset('public/backend/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ asset('public/public/backend/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ asset('public/public/public/backend/vendors/pdfmake/build/vfs_fonts.js')}}"></script>


 
    <!-- text area summer note -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('public/backend/build/js/custom.min.js')}}"></script>


    <script>
      $(document).ready(function() {
          $('#summernote').summernote();
     });
    </script>
    
    @yield('customjs')
  </body>
</html>



