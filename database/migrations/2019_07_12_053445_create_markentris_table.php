<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkentrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markentries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('classes_id');
            $table->integer('sessions_id');
            $table->integer('subject_id');
            $table->integer('exam_id');
            $table->double('written');
            $table->double('mcq');
            $table->double('practical');
            $table->integer('teacher_id');
            $table->integer('is_active');
            $table->integer('is_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markentris');
    }
}
