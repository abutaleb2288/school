<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_expenses', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('expense_purpose_id')->unsigned();
            $table->string('title',50);
            $table->string('date',25)->nullable();
            $table->integer('amount');
            $table->string('status',5)->nullable();
            $table->string('description',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_expenses');
    }
}
