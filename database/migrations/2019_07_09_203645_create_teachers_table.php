<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('teacher_id')->nullable();
            $table->string('name');
            $table->string('rf_id')->nullable();
            $table->string('mobile')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('designation');
            $table->integer('nid')->nullable();
            $table->string('bloodgroup')->nullable();
            $table->string('gender');
            $table->string('father');
            $table->string('mother')->nullable();
            $table->text('education');
            $table->float('salary')->nullable();
            $table->string('joindate');
            $table->integer('position')->nullable();
            $table->longtext('photo')->nullable();
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
