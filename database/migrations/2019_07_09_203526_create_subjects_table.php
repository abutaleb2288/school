<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('classes_id');
            $table->string('name');
            $table->integer('code')->nullable();
            $table->integer('category')->nullable();
            $table->integer('full_mark')->nullable();
            $table->integer('passmark')->nullable();
            $table->integer('wfm')->nullable();
            $table->integer('wpm')->nullable();
            $table->integer('mcqfm')->nullable();
            $table->integer('mcqpm')->nullable();
            $table->integer('pfm')->nullable();
            $table->integer('ppm')->nullable();
            $table->integer('subject_type')->nullable();
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
