<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('teacher_id')->nullable()->unsigned();
            $table->integer('subject_id')->nullable()->unsigned();
            $table->integer('session_id')->nullable()->unsigned();
            $table->integer('section_id')->nullable()->unsigned();
            $table->integer('class_id')->nullable()->unsigned();
            $table->integer('day_id')->nullable()->unsigned();
            $table->integer('time_id')->nullable()->unsigned();
            $table->integer('endtime_id')->nullable()->unsigned();
            $table->string('status',5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routines');
    }
}
